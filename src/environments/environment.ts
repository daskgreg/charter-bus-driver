// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBWYWq14esejLNJ4Ls77sbwOHVIuTsqvh4",
    authDomain: "ionic-auth-example-862b1.firebaseapp.com",
    projectId: "ionic-auth-example-862b1",
    storageBucket: "ionic-auth-example-862b1.appspot.com",
    messagingSenderId: "714991366381",
    appId: "1:714991366381:web:d19a236679a16f98bfdce4",
    measurementId: "G-69TE695X6K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
