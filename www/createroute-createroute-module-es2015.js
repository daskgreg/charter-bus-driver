(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["createroute-createroute-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/createroute/createroute.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/createroute/createroute.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>createroute</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/createroute/createroute-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/createroute/createroute-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CreateroutePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateroutePageRoutingModule", function() { return CreateroutePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _createroute_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./createroute.page */ "./src/app/createroute/createroute.page.ts");




const routes = [
    {
        path: '',
        component: _createroute_page__WEBPACK_IMPORTED_MODULE_3__["CreateroutePage"]
    }
];
let CreateroutePageRoutingModule = class CreateroutePageRoutingModule {
};
CreateroutePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreateroutePageRoutingModule);



/***/ }),

/***/ "./src/app/createroute/createroute.module.ts":
/*!***************************************************!*\
  !*** ./src/app/createroute/createroute.module.ts ***!
  \***************************************************/
/*! exports provided: CreateroutePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateroutePageModule", function() { return CreateroutePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _createroute_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./createroute-routing.module */ "./src/app/createroute/createroute-routing.module.ts");
/* harmony import */ var _createroute_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./createroute.page */ "./src/app/createroute/createroute.page.ts");







let CreateroutePageModule = class CreateroutePageModule {
};
CreateroutePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _createroute_routing_module__WEBPACK_IMPORTED_MODULE_5__["CreateroutePageRoutingModule"]
        ],
        declarations: [_createroute_page__WEBPACK_IMPORTED_MODULE_6__["CreateroutePage"]]
    })
], CreateroutePageModule);



/***/ }),

/***/ "./src/app/createroute/createroute.page.scss":
/*!***************************************************!*\
  !*** ./src/app/createroute/createroute.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NyZWF0ZXJvdXRlL2NyZWF0ZXJvdXRlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/createroute/createroute.page.ts":
/*!*************************************************!*\
  !*** ./src/app/createroute/createroute.page.ts ***!
  \*************************************************/
/*! exports provided: CreateroutePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateroutePage", function() { return CreateroutePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CreateroutePage = class CreateroutePage {
    constructor() { }
    ngOnInit() {
    }
};
CreateroutePage.ctorParameters = () => [];
CreateroutePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-createroute',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./createroute.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/createroute/createroute.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./createroute.page.scss */ "./src/app/createroute/createroute.page.scss")).default]
    })
], CreateroutePage);



/***/ })

}]);
//# sourceMappingURL=createroute-createroute-module-es2015.js.map