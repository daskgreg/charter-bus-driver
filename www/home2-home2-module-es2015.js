(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home2-home2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home2/home2.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home2/home2.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n  \n<!-- \n  <ion-fab vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button>\n      <ion-icon (click)=\"navigateToLoginPage()\" name=\"add\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n   -->");

/***/ }),

/***/ "./src/app/home2/home2-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/home2/home2-routing.module.ts ***!
  \***********************************************/
/*! exports provided: Home2PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2PageRoutingModule", function() { return Home2PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home2.page */ "./src/app/home2/home2.page.ts");




const routes = [
    {
        path: '',
        component: _home2_page__WEBPACK_IMPORTED_MODULE_3__["Home2Page"],
        children: [
            {
                path: 'routelist',
                loadChildren: () => Promise.all(/*! import() | routelist-routelist-module */[__webpack_require__.e("common"), __webpack_require__.e("routelist-routelist-module")]).then(__webpack_require__.bind(null, /*! ../routelist/routelist.module */ "./src/app/routelist/routelist.module.ts")).then(m => m.RoutelistPageModule)
            },
            {
                path: 'wallet',
                loadChildren: () => __webpack_require__.e(/*! import() | wallet-wallet-module */ "wallet-wallet-module").then(__webpack_require__.bind(null, /*! ../wallet/wallet.module */ "./src/app/wallet/wallet.module.ts")).then(m => m.WalletPageModule)
            },
            {
                path: 'routehistory',
                loadChildren: () => Promise.all(/*! import() | routehistory-routehistory-module */[__webpack_require__.e("common"), __webpack_require__.e("routehistory-routehistory-module")]).then(__webpack_require__.bind(null, /*! ../routehistory/routehistory.module */ "./src/app/routehistory/routehistory.module.ts")).then(m => m.RoutehistoryPageModule)
            },
            {
                path: 'settings',
                loadChildren: () => Promise.all(/*! import() | settings-settings-module */[__webpack_require__.e("common"), __webpack_require__.e("settings-settings-module")]).then(__webpack_require__.bind(null, /*! ../settings/settings.module */ "./src/app/settings/settings.module.ts")).then(m => m.SettingsPageModule)
            },
        ]
    },
    {
        path: '',
        redirectTo: '/home2/routelist'
    }
];
let Home2PageRoutingModule = class Home2PageRoutingModule {
};
Home2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Home2PageRoutingModule);



/***/ }),

/***/ "./src/app/home2/home2.module.ts":
/*!***************************************!*\
  !*** ./src/app/home2/home2.module.ts ***!
  \***************************************/
/*! exports provided: Home2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2PageModule", function() { return Home2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home2-routing.module */ "./src/app/home2/home2-routing.module.ts");
/* harmony import */ var _home2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home2.page */ "./src/app/home2/home2.page.ts");







let Home2PageModule = class Home2PageModule {
};
Home2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Home2PageRoutingModule"]
        ],
        declarations: [_home2_page__WEBPACK_IMPORTED_MODULE_6__["Home2Page"]]
    })
], Home2PageModule);



/***/ }),

/***/ "./src/app/home2/home2.page.scss":
/*!***************************************!*\
  !*** ./src/app/home2/home2.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-tabs ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\nion-tabs ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\nion-tabs ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\nion-tabs ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\nion-tabs ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\nion-tabs ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\nion-tabs ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\nion-tabs ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\nion-tabs ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZTIvaG9tZTIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNDO0VBQ0MsMENBQUE7RUFBNEMsaUJBQUE7QUFDOUM7QUFBRTtFQUNDLGtCQUFBO0VBQ0EsVUFBQTtBQUVIO0FBQ0M7RUFDQyxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQVEsUUFBQTtFQUNSLFdBQUE7QUFFRjtBQURFO0VBQ0MsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBR0g7QUFERTtFQUNDLG9DQUFBO0FBR0g7QUFERTtFQUNDLGlCQUFBO0VBQ0EsNkJBQUE7QUFHSDtBQURFO0VBQ0MsZ0JBQUE7RUFDQSw0QkFBQTtBQUdIO0FBREU7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7QUFHSDtBQUZHO0VBQ0MsNEJBQUE7QUFJSjtBQUVBO0VBQ0MscUNBQUE7QUFDRCIsImZpbGUiOiJzcmMvYXBwL2hvbWUyL2hvbWUyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10YWJze1xyXG5cdGlvbi1mYWIge1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG5cdFx0XHR6LWluZGV4OiAxO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRpb24tdGFiLWJhciB7XHJcblx0XHQtLWJvcmRlcjogMDtcclxuXHRcdC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHRsZWZ0OjA7IHJpZ2h0OiAwO1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHQmOmFmdGVye1xyXG5cdFx0XHRjb250ZW50OiBcIiBcIjtcclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdGJvdHRvbTogMDtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdFx0aGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uIHtcclxuXHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24uY29tbWVudHMge1xyXG5cdFx0XHRtYXJnaW4tcmlnaHQ6IDBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5ub3RpZnMge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xyXG5cdFx0fVxyXG5cdFx0c3ZnIHsgICAgXHJcblx0XHRcdHdpZHRoOiA3MnB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxOXB4O1xyXG5cdFx0XHRwYXRoe1xyXG5cdFx0XHRcdGZpbGw6ICB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHR9XHRcdFxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmljb25TdHlsaW5ne1xyXG5cdGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/home2/home2.page.ts":
/*!*************************************!*\
  !*** ./src/app/home2/home2.page.ts ***!
  \*************************************/
/*! exports provided: Home2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2Page", function() { return Home2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let Home2Page = class Home2Page {
    constructor(router, menu) {
        this.router = router;
        this.menu = menu;
        this.pages = [
            {
                title: 'routelist',
                url: '/home2/routelist'
            },
            {
                title: 'wallet',
                url: '/home2/wallet',
            },
            {
                title: 'routehistory',
                url: '/home2/routehistory',
            },
            {
                title: 'settings',
                url: '/home2/settings'
            },
        ];
        this.selectedPath = '';
        this.router.events.subscribe((event) => {
            this.selectedPath = event.url;
        });
    }
    ngOnInit() {
    }
};
Home2Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
Home2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home2/home2.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home2.page.scss */ "./src/app/home2/home2.page.scss")).default]
    })
], Home2Page);



/***/ })

}]);
//# sourceMappingURL=home2-home2-module-es2015.js.map