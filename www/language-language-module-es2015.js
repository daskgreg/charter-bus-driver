(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["language-language-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/language/language.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/language/language.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding class=\"background\">\n  <ion-title>Select Language</ion-title>\n  <div class=\"style-center\">\n    <button (click)=\"selectgr()\" class=\"btnStyling\">\n      <img src=\"assets/images/flags/greek_flag.svg\" alt=\"eng\">\n    </button>\n    <button (click)=\"selecten()\" class=\"btnStyling\">\n      <img src=\"assets/images/flags/english_flag.svg\" alt=\"ελ\">\n    </button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/language/language-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/language/language-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LanguagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguagePageRoutingModule", function() { return LanguagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _language_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./language.page */ "./src/app/language/language.page.ts");




const routes = [
    {
        path: '',
        component: _language_page__WEBPACK_IMPORTED_MODULE_3__["LanguagePage"]
    }
];
let LanguagePageRoutingModule = class LanguagePageRoutingModule {
};
LanguagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LanguagePageRoutingModule);



/***/ }),

/***/ "./src/app/language/language.module.ts":
/*!*********************************************!*\
  !*** ./src/app/language/language.module.ts ***!
  \*********************************************/
/*! exports provided: LanguagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguagePageModule", function() { return LanguagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _language_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./language-routing.module */ "./src/app/language/language-routing.module.ts");
/* harmony import */ var _language_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./language.page */ "./src/app/language/language.page.ts");







let LanguagePageModule = class LanguagePageModule {
};
LanguagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _language_routing_module__WEBPACK_IMPORTED_MODULE_5__["LanguagePageRoutingModule"]
        ],
        declarations: [_language_page__WEBPACK_IMPORTED_MODULE_6__["LanguagePage"]]
    })
], LanguagePageModule);



/***/ }),

/***/ "./src/app/language/language.page.scss":
/*!*********************************************!*\
  !*** ./src/app/language/language.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content.background {\n  --ion-background-color: var( --ion-color-ibus-theblue);\n}\n\n.style-center {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n}\n\n.ios .style-center {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n}\n\n.btnStyling {\n  background-color: transparent;\n}\n\n.ios ion-title {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n  color: white;\n  font-size: 22px;\n  position: absolute;\n  font-family: monteSBold;\n  margin-top: -50%;\n  margin-left: 0%;\n}\n\nion-title {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n  color: white;\n  font-size: 20px;\n  position: absolute;\n  font-family: monteSBold;\n  margin-left: 20%;\n  margin-top: 38%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGFuZ3VhZ2UvbGFuZ3VhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksc0RBQUE7QUFBSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBRUo7O0FBQUE7RUFDSSw2QkFBQTtBQUdKOztBQURBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBSUo7O0FBREE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFJSiIsImZpbGUiOiJzcmMvYXBwL2xhbmd1YWdlL2xhbmd1YWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pb24tY29udGVudC5iYWNrZ3JvdW5ke1xyXG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogdmFyKCAtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG59XHJcbi5zdHlsZS1jZW50ZXJ7IFxyXG4gICAgZGlzcGxheTogZmxleDsgXHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyBcclxuICAgIGhlaWdodDogMTAwJTsgXHJcbn0gXHJcbi5pb3MgLnN0eWxlLWNlbnRlcnsgXHJcbiAgICBkaXNwbGF5OiBmbGV4OyBcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IFxyXG4gICAgaGVpZ2h0OiAxMDAlOyBcclxufSBcclxuLmJ0blN0eWxpbmd7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O1xyXG59XHJcbi5pb3MgaW9uLXRpdGxle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XHJcbiAgICBtYXJnaW4tdG9wOi01MCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMCU7XHJcbn1cclxuXHJcbmlvbi10aXRsZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwJTtcclxuICAgIG1hcmdpbi10b3A6IDM4JTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/language/language.page.ts":
/*!*******************************************!*\
  !*** ./src/app/language/language.page.ts ***!
  \*******************************************/
/*! exports provided: LanguagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguagePage", function() { return LanguagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/language.service */ "./src/app/services/language.service.ts");




let LanguagePage = class LanguagePage {
    constructor(languageService, router) {
        this.languageService = languageService;
        this.router = router;
    }
    ngOnInit() {
    }
    selecten() {
        this.lang = 'en';
        localStorage.setItem('lang', this.lang);
        this.languageService.setLanguage(this.lang);
        this.router.navigate(['login']);
    }
    selectgr() {
        this.lang = 'gr';
        localStorage.setItem('lang', this.lang);
        this.languageService.setLanguage(this.lang);
        this.router.navigate(['login']);
    }
};
LanguagePage.ctorParameters = () => [
    { type: _services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LanguagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-language',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./language.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/language/language.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./language.page.scss */ "./src/app/language/language.page.scss")).default]
    })
], LanguagePage);



/***/ })

}]);
//# sourceMappingURL=language-language-module-es2015.js.map