(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routestarted-routestarted-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/routestarted/routestarted.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routestarted/routestarted.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item (click)=\"navigateToStartFeedPage()\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"goToWalletPageWithDriverId()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n<ion-router-outlet id=\"main-content\">\n\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'routeStarted-myRoute.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content scroll=\"true\">\n    <ion-label  style=\"padding: 1px;\" class=\"ion-text-center\">\n      <ion-item (click)=\"navigateToRouteDetails()\" class=\"destItem\" lines=\"none\">\n        <button  class=\"destBtn\">{{startingPoint}}</button>\n        <ion-icon class=\"dmpIconStyle destIcon\" name=\"map\" side=\"end\" ></ion-icon>\n      </ion-item>\n      <ion-item class=\"timeItem\" lines=\"none\">\n        <ion-icon class=\"dmpIconStyle timeIcon\" name=\"hourglass\" side=\"start\" ></ion-icon>\n        <button class=\"timeBtn\"><a class=\"numberStyle\">34&nbsp;&nbsp;</a>hours</button>\n      </ion-item>\n      <ion-item (click)=\"getPassengersInformationFromRouteListSelection()\" class=\"passItem\" lines=\"none\">\n        <button class=\"passBtn\">\n          <a class=\"numberStyle\">2&nbsp;&nbsp;</a>Passengers\n        </button>\n        <ion-icon class=\"dmpIconStyle passIcon\" name=\"walk\" side=\"start\" ></ion-icon>\n      </ion-item>\n    </ion-label>\n\n    <ion-list class=\"ionListClass\">\n      <ion-button  (click)=\"startRoute()\" class=\"btnStyle2\">{{'routeStarted-startRoute.title' | translate}}</ion-button>\n    </ion-list>\n    <!-- <ion-list class=\"ionListClass\">\n      <ion-button (click)=\"beforeRouteStartedGoTechInspectPage()\"  class=\"btnStyle\">{{'routeStarted-map.title' | translate}}</ion-button>\n    </ion-list> -->\n    <ion-list class=\"ionListClass\">\n      <ion-button (click)=\"getPassengersInformationFromRouteListSelection()\"class=\"btnStyle\">{{'routeStarted-passengers.title' | translate}}</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button  (click)=\"goToRouteDetails()\" class=\"btnStyle\">{{'routeStarted-routeDetails.title' | translate}}</ion-button>\n    </ion-list>\n  </ion-content>\n  <div class=\"myiontab\">\n\n    <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n      <ion-buttons class=\"addBtn\" (click)=\"goBackToRouteListPage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n        <button (click)=\"navigateToStartFeedPage()\" class=\"adBtn2\">\n          <ion-icon  name=\"arrow-back\"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-tab-button tab=\"routelist\" (click)=\"navigateToStartFeedPage()\">\n        <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"goToWalletPageWithDriverId()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n      </ion-tab-button>\n  \n      <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n  \n      <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"takePicture4()\">\n        <ion-icon class=\"iconStyling\" name=\"camera\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n      </ion-tab-button>\n    </ion-tab-bar>\n  \n  </div>\n  \n</div>\n</ion-router-outlet>\n\n<!-- <ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home/feed\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n<ion-router-outlet id=\"main-content\">\n\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'routeStarted-myRoute.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n  <ion-content scroll=\"true\">\n\n    <ion-label  style=\"padding: 1px;\" class=\"ion-text-center\">\n    \n      <ion-item (click)=\"navigateToRouteDetails()\" class=\"destItem\" lines=\"none\">\n        <button  class=\"destBtn\">{{startPlace}}</button>\n        <ion-icon class=\"dmpIconStyle destIcon\" name=\"map\" side=\"end\" ></ion-icon>\n      </ion-item>\n      <ion-item class=\"timeItem\" lines=\"none\">\n        <ion-icon class=\"dmpIconStyle timeIcon\" name=\"hourglass\" side=\"start\" ></ion-icon>\n        <button class=\"timeBtn\"><a class=\"numberStyle\">{{Time}}</a>{{total}}</button>\n      </ion-item>\n      <ion-item (click)=\"navigateToPassengersPage()\" class=\"passItem\" lines=\"none\">\n        <button class=\"passBtn\"><a class=\"numberStyle\">{{passCounter}}</a>{{pTotal}}</button>\n        <ion-icon class=\"dmpIconStyle passIcon\" name=\"walk\" side=\"start\" ></ion-icon>\n      </ion-item>\n    </ion-label>\n\n    <ion-list class=\"ionListClass\">\n      <ion-button  (click)=\"startRoute()\" class=\"btnStyle2\">{{'routeStarted-startRoute.title' | translate}}</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button (click)=\"navigateToMapPage()\"  class=\"btnStyle\">{{'routeStarted-map.title' | translate}}</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button (click)=\"navigateToPassengersPage()\"class=\"btnStyle\">{{'routeStarted-passengers.title' | translate}}</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button  (click)=\"navigateToRouteDetails()\" class=\"btnStyle\">{{'routeStarted-routeDetails.title' | translate}}</ion-button>\n    </ion-list>\n  </ion-content>\n  <div class=\"myiontab\">\n\n    <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n      <ion-buttons class=\"addBtn\" (click)=\"navigateToVehiclePage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n        <button (click)=\"navigateToVehiclePage()\" class=\"adBtn2\">\n          <ion-icon  name=\"arrow-back\"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-tab-button tab=\"routelist\" (click)=\"navigateToRouteListPage()\">\n        <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n      </ion-tab-button>\n  \n      <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n  \n      <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n      </ion-tab-button>\n    </ion-tab-bar>\n  \n  </div>\n  \n</div>\n</ion-router-outlet> -->");

/***/ }),

/***/ "./src/app/routestarted/routestarted-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/routestarted/routestarted-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: RoutestartedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutestartedPageRoutingModule", function() { return RoutestartedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _routestarted_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routestarted.page */ "./src/app/routestarted/routestarted.page.ts");




const routes = [
    {
        path: '',
        component: _routestarted_page__WEBPACK_IMPORTED_MODULE_3__["RoutestartedPage"]
    }
];
let RoutestartedPageRoutingModule = class RoutestartedPageRoutingModule {
};
RoutestartedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RoutestartedPageRoutingModule);



/***/ }),

/***/ "./src/app/routestarted/routestarted.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/routestarted/routestarted.module.ts ***!
  \*****************************************************/
/*! exports provided: RoutestartedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutestartedPageModule", function() { return RoutestartedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _routestarted_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routestarted-routing.module */ "./src/app/routestarted/routestarted-routing.module.ts");
/* harmony import */ var _routestarted_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./routestarted.page */ "./src/app/routestarted/routestarted.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let RoutestartedPageModule = class RoutestartedPageModule {
};
RoutestartedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _routestarted_routing_module__WEBPACK_IMPORTED_MODULE_5__["RoutestartedPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_routestarted_page__WEBPACK_IMPORTED_MODULE_6__["RoutestartedPage"]]
    })
], RoutestartedPageModule);



/***/ }),

/***/ "./src/app/routestarted/routestarted.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/routestarted/routestarted.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.titleStyle {\n  font-size: 25px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-darkblue);\n  padding: 37px;\n}\n\n.ionListClass {\n  justify-content: center;\n  display: flex !important;\n  align-items: center !important;\n}\n\n.ionListClass .btnStyle {\n  width: 88%;\n  height: 50px;\n  border-radius: 12px;\n  --background:var(--ion-color-ibus-theblue);\n  font-family: monteBold;\n  font-size: 16px;\n}\n\n.ionListClass .btnStyle2 {\n  width: 88%;\n  height: 50px;\n  border-radius: 12px;\n  --background:var(--ion-color-ibus-lightgreen);\n  font-family: monteBold;\n  font-size: 16px;\n}\n\n.ionListClass .btnStyleChanged {\n  --background:var(--ion-color-ibus-lightgreen);\n}\n\n.ionListClass .txtArea {\n  width: 88%;\n  height: 150px;\n  border-radius: 12px;\n  border: none;\n  background-color: var(--ion-color-ibus-theblue);\n  color: white;\n  font-family: monteSBold;\n  font-size: 16px;\n  padding: 25px 2px 2px 20px;\n}\n\n.myiontab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\n\n.myiontab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 70px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 51px;\n  height: 51px;\n  position: absolute;\n  left: 181px;\n  top: -32px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.myiontab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.myiontab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.myiontab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.myiontab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.myiontab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.dmpIconStyle {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.destBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: white;\n  border: 2px solid var(--ion-color-ibus-darkblue);\n  font-size: 15px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.timeBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: white;\n  border: 2px solid var(--ion-color-ibus-darkblue);\n  font-size: 15px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.passBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: white;\n  border: 2px solid var(--ion-color-ibus-darkblue);\n  font-size: 15px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.numberStyle {\n  color: var(--ion-color-ibus-lightgreen);\n  font-size: 17;\n}\n\n.destItem {\n  padding: 9px 8px 12px 6px;\n}\n\n.destIcon {\n  width: 160px;\n  height: 50px;\n}\n\n.timeItem {\n  padding: 9px 8px 12px 6px;\n}\n\n.timeIcon {\n  width: 160px;\n  height: 50px;\n}\n\n.passItem {\n  padding: 9px 8px 12px 6px;\n}\n\n.passIcon {\n  width: 160px;\n  height: 50px;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm91dGVzdGFydGVkL3JvdXRlc3RhcnRlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw0Q0FBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0FBRUo7O0FBQUE7RUFDSSxxQkFBQTtFQUNBLFlBQUE7QUFHSjs7QUFEQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUlKOztBQUZBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUtKOztBQUhBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0VBQ0EscUNBQUE7QUFNSjs7QUFKQTtFQUNJLHFDQUFBO0FBT0o7O0FBTEE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQVFKOztBQUxBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBUUo7O0FBTkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFTSjs7QUFQQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtBQVVKOztBQVJBO0VBQ0ksbUJBQUE7QUFXSjs7QUFUQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQVlKOztBQVZBO0VBQ0ksb0JBQUE7QUFhSjs7QUFWQTtFQUNJLGVBQUE7RUFDQSx1QkFBQTtFQUNBLHFDQUFBO0VBQ0EsYUFBQTtBQWFKOztBQVhBO0VBQ1MsdUJBQUE7RUFDQSx3QkFBQTtFQUNBLDhCQUFBO0FBY1Q7O0FBYkk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsMENBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7QUFlUjs7QUFiSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSw2Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBQWVSOztBQWJJO0VBQ0ksNkNBQUE7QUFlUjs7QUFiSTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsK0NBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7QUFlUjs7QUFYQztFQUNDLDBDQUFBO0VBQTRDLGlCQUFBO0FBZTlDOztBQWRFO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0FBZ0JIOztBQWJDO0VBNkJDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFBUSxRQUFBO0VBQ0YsV0FBQTtFQUNBLFlBQUE7QUFaUjs7QUF0QlE7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLDBDQUFBO0VBQTRDLGlCQUFBO0FBeUJ4RDs7QUF4Qlk7RUFDSSxrQkFBQTtFQUNBLCtDQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBMEJoQjs7QUF4Qlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBMEJoQjs7QUF4Qlk7RUFDQSx5QkFBQTtBQTBCWjs7QUFoQkU7RUFDQyxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esa0JBQUE7QUFrQkg7O0FBaEJFO0VBQ0Msb0NBQUE7QUFrQkg7O0FBaEJFO0VBQ0MsaUJBQUE7RUFDQSw2QkFBQTtBQWtCSDs7QUFoQkU7RUFDQyxnQkFBQTtFQUNBLDRCQUFBO0FBa0JIOztBQWhCRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQWtCSDs7QUFqQkc7RUFDQyw0QkFBQTtBQW1CSjs7QUFiQTtFQUNJLHFDQUFBO0FBZ0JKOztBQWRBO0VBQ0MscUNBQUE7QUFpQkQ7O0FBZkE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnREFBQTtFQUVBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBaUJKOztBQWZBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0RBQUE7RUFFQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQWlCSjs7QUFmQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdEQUFBO0VBRUEsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFpQko7O0FBZEE7RUFDSSx1Q0FBQTtFQUNBLGFBQUE7QUFpQko7O0FBZkE7RUFDSSx5QkFBQTtBQWtCSjs7QUFmQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0FBa0JKOztBQWhCQTtFQUNJLHlCQUFBO0FBbUJKOztBQWhCQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0FBbUJKOztBQWpCQTtFQUNJLHlCQUFBO0FBb0JKOztBQWpCQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0FBb0JKOztBQWxCQTtFQUNJO0lBQ0kscUJBQUE7SUFDQSxhQUFBO0VBcUJOO0FBQ0Y7O0FBbkJFO0VBQ0U7SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUFxQk47QUFDRjs7QUFsQkU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQW9CTjs7RUFsQkU7SUFDSSxrQkFBQTtJQUNBLFVBQUE7SUFDQSxXQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7RUFxQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JvdXRlc3RhcnRlZC9yb3V0ZXN0YXJ0ZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbn1cclxuLmlvcyBpb24tdG9vbGJhcntcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogMTI3cHg7XHJcbn1cclxuLm1kIGlvbi10b29sYmFye1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgaGVpZ2h0OiA4N3B4O1xyXG59XHJcbi5ub3RCdG57XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogNDdweDtcclxuICAgIGhlaWdodDogNjJweDtcclxufVxyXG4ubWVudUJ0bntcclxuICAgIGZvbnQtc2l6ZTogMTAwcHg7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ubWVudVR4dHtcclxuICAgIGZvbnQtc2l6ZToxOHB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVSZWd1bGFyO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5pY29uU3R5bGluZ3tcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4ubWVudVRvb2xiYXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA4MHB4O1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG59XHJcblxyXG4uaW9zICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5tZCAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxufVxyXG5cclxuLnRpdGxlU3R5bGV7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcGFkZGluZzogMzdweDtcclxufVxyXG4uaW9uTGlzdENsYXNze1xyXG4gICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XHJcbiAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xyXG4gICAgLmJ0blN0eWxle1xyXG4gICAgICAgIHdpZHRoOiA4OCU7XHJcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOnZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgICAgICBmb250LXNpemU6MTZweDtcclxuICAgIH1cclxuICAgIC5idG5TdHlsZTJ7XHJcbiAgICAgICAgd2lkdGg6IDg4JTtcclxuICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICAtLWJhY2tncm91bmQ6dmFyKC0taW9uLWNvbG9yLWlidXMtbGlnaHRncmVlbik7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZToxNnB4O1xyXG4gICAgfVxyXG4gICAgLmJ0blN0eWxlQ2hhbmdlZHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6dmFyKC0taW9uLWNvbG9yLWlidXMtbGlnaHRncmVlbik7XHJcbiAgICB9XHJcbiAgICAudHh0QXJlYXtcclxuICAgICAgICB3aWR0aDogODglO1xyXG4gICAgICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZToxNnB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDI1cHggMnB4IDJweCAyMHB4O1xyXG4gICAgfVxyXG59XHJcbi5teWlvbnRhYntcclxuXHRpb24tZmFiIHtcclxuXHRcdG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTsgLyogZml4IG5vdGNoIGlvcyovXHJcblx0XHRpb24tZmFiLWJ1dHRvbiB7XHJcblx0XHRcdC0tYm94LXNoYWRvdzogbm9uZTtcclxuXHRcdFx0ei1pbmRleDogMTtcclxuXHRcdH1cclxuXHR9XHJcblx0aW9uLXRhYi1iYXIge1xyXG4gICAgICAgIC5hZGRCdG4ge1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG4gICAgICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgICAgICAgLS1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDUxcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAxODFweDtcclxuICAgICAgICAgICAgICAgIHRvcDogLTMycHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5Om1vbnRlU0JvbGQ7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6MTFweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpb24taWNvbntcclxuICAgICAgICAgICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6MzBweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDozMHB4O1xyXG4gICAgICAgICAgICAgICAgc3Ryb2tlOiA1cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaW9uLWJhZGdle1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHRcdC0tYm9yZGVyOiAwO1xyXG5cdFx0LS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdGJvdHRvbTogMDtcclxuXHRcdGxlZnQ6MDsgcmlnaHQ6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OjcwcHg7XHJcblx0XHQmOmFmdGVye1xyXG5cdFx0XHRjb250ZW50OiBcIiBcIjtcclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdGJvdHRvbTogMDtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdFx0aGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uIHtcclxuXHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24uY29tbWVudHMge1xyXG5cdFx0XHRtYXJnaW4tcmlnaHQ6IDBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5ub3RpZnMge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xyXG5cdFx0fVxyXG5cdFx0c3ZnIHsgICAgXHJcblx0XHRcdHdpZHRoOiA3MnB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxOXB4O1xyXG5cdFx0XHRwYXRoe1xyXG5cdFx0XHRcdGZpbGw6ICB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHR9XHRcdFxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmRtcEljb25TdHlsZXtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7ICBcclxufVxyXG4uaWNvblN0eWxpbmd7XHJcblx0Y29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5kZXN0QnRue1xyXG4gICAgd2lkdGg6IDIxMHB4O1xyXG4gICAgaGVpZ2h0OiA1N3B4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjdweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG5cclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi50aW1lQnRue1xyXG4gICAgd2lkdGg6MjEwcHg7XHJcbiAgICBoZWlnaHQ6NTdweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI3cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuXHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4ucGFzc0J0bntcclxuICAgIHdpZHRoOjIxMHB4O1xyXG4gICAgaGVpZ2h0OjU3cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyN3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcblxyXG4gICAgZm9udC1zaXplOjE1cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbi5udW1iZXJTdHlsZXtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWxpZ2h0Z3JlZW4pO1xyXG4gICAgZm9udC1zaXplOjE3O1xyXG59XHJcbi5kZXN0SXRlbXtcclxuICAgIHBhZGRpbmc6IDlweCA4cHggMTJweCA2cHg7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogOCU7XHJcbn1cclxuLmRlc3RJY29ue1xyXG4gICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbi50aW1lSXRlbXtcclxuICAgIHBhZGRpbmc6IDlweCA4cHggMTJweCA2cHg7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogOCU7XHJcbn1cclxuLnRpbWVJY29ue1xyXG4gICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbi5wYXNzSXRlbXtcclxuICAgIHBhZGRpbmc6IDlweCA4cHggMTJweCA2cHg7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogOCU7XHJcbn1cclxuLnBhc3NJY29ue1xyXG4gICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuICAgIGlvbi10b29sYmFye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDEyN3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgICBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLmlvcyBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgfVxyXG4gICAgLmlvcyBpb24tdGl0bGV7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogLTE1cHg7XHJcbiAgICAgICAgbGVmdDogLTE1cHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIH1cclxuICB9XHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gICBcclxuXHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/routestarted/routestarted.page.ts":
/*!***************************************************!*\
  !*** ./src/app/routestarted/routestarted.page.ts ***!
  \***************************************************/
/*! exports provided: RoutestartedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutestartedPage", function() { return RoutestartedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");









const { Camera } = _capacitor_core__WEBPACK_IMPORTED_MODULE_4__["Plugins"];
let RoutestartedPage = class RoutestartedPage {
    constructor(http, activatedRoute, router, nativeHttp, loading, platform) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.nativeHttp = nativeHttp;
        this.loading = loading;
        this.platform = platform;
        this.chrbus_passengers_json = [{ "vehicle_map_id": "2245", "pass_id": "1", "first_name": "Christos", "last_name": "Sotidis", "mobile": "123456", "email": "christos@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2245", "pass_id": "2", "first_name": "Christos2", "last_name": "Sotidis2", "mobile": "123456", "email": "christos2@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2246", "pass_id": "3", "first_name": "Christos3", "last_name": "Sotidis3", "mobile": "123456", "email": "christos3@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2246", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2250", "pass_id": "1", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2250", "pass_id": "2", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2251", "pass_id": "1", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2251", "pass_id": "2", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2247", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2248", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2249", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" }];
        this.custom = localStorage.getItem('custom');
        this.ekkinisiRoute = localStorage.getItem('ekkinisiRoute');
        this.ekkinisiCust = localStorage.getItem('ekkinisiCust');
        this.passCounter = 0;
        this.newCustomPickupRoutes = [];
        this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = [];
        this.dataFromRouteList = this.activatedRoute.snapshot.paramMap.get('xyz');
        this.dataFromRouteListLogin = this.activatedRoute.snapshot.paramMap.get('loginid');
        this.dataFromRouteListJSON = JSON.parse(this.dataFromRouteList);
        this.dataFromRouteListLoginJSON = JSON.parse(this.dataFromRouteListLogin);
        console.log('%c DATA FROM ROUTELIST JSON', 'color:orange;');
        console.log(this.dataFromRouteListJSON);
        console.log(this.dataFromRouteListJSON.SERVICECODE);
        console.log('%c DATA FROM ROUTELIST LOGIN JSON', 'color:yellow;');
        console.log(this.dataFromRouteListLoginJSON);
        this.Id = this.dataFromRouteListLoginJSON;
        this.personId = this.dataFromRouteListJSON.PERSON_ID;
        var i = 0;
        if (this.custom == "true") {
            this.startPlace = this.ekkinisiCust;
            this.vehmapId = localStorage.getItem('custVehmap');
        }
        else if (this.custom == "false") {
            this.startPlace = this.ekkinisiRoute;
            this.vehmapId = localStorage.getItem('vehmapid');
        }
        for (i = 0; i < this.chrbus_passengers_json.length; i++) {
            if (this.vehmapId == this.chrbus_passengers_json[i].vehicle_map_id) {
                this.passCounter++;
            }
        }
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let loader = yield this.loading.create({
                message: 'Data Loading'
            });
            yield loader.present();
            this.platform.is('cordova') ? this.getCustomPickupFromNativeHttp() : this.getCustomPickupFromHttpClient();
            loader.dismiss();
        });
    }
    getCustomPickupFromHttpClient() {
        if (this.dataFromRouteListJSON.TYPE == 'FIX') {
            this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_route_pup.cfm?' + 'chrbus_code=' + this.dataFromRouteListJSON.SERVICECODE + '&userid=dmta')
                .subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                let loader = yield this.loading.create({
                    message: 'Data Loading'
                });
                console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                console.log(data);
                this.newCustomPickupRoutes = data;
                console.log(this.newCustomPickupRoutes);
                this.newCustomPickupRoutesJSON = this.newCustomPickupRoutes;
                this.goToMap = this.newCustomPickupRoutesJSON;
                console.log(this.newCustomPickupRoutesJSON);
                this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutesJSON;
                console.log(this.newCustomPickupRoutesJSONtoArray);
                this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = this.newCustomPickupRoutesJSONtoArray.FIXEDPICKUPS;
                console.log('auto thelw');
                console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME);
                this.startingPoint = this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME;
                loader.dismiss();
            }));
        }
        else {
            this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?' + 'route_id=' + this.dataFromRouteListJSON.SERVICECODE + '&userid=dmta')
                .subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                let loader = yield this.loading.create({
                    message: 'Data Loading'
                });
                yield loader.present();
                console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                console.log(data);
                this.newCustomPickupRoutes = data;
                console.log(this.newCustomPickupRoutes);
                this.newCustomPickupRoutesJSON = this.newCustomPickupRoutes;
                this.goToMap = this.newCustomPickupRoutesJSON;
                console.log(this.newCustomPickupRoutesJSON);
                this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutesJSON;
                console.log(this.newCustomPickupRoutesJSONtoArray);
                this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = this.newCustomPickupRoutesJSONtoArray.CUSTPICKUPS;
                console.log('auto thelw');
                console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].PICKUP_ADDRESS);
                this.startingPoint = this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].PICKUP_ADDRESS;
                loader.dismiss();
            }));
        }
        console.log('oute of check1', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
    }
    getCustomPickupFromNativeHttp() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // let loader = await this.loading.create();
            // await loader.present();
            let loader = yield this.loading.create();
            yield loader.present();
            //let url = 'http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=2&userid=dmta';
            let url = '';
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=' + this.dataFromRouteListJSON.SERVICECODE + '&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => loader.dismiss()))
                .subscribe(data => {
                let parsed = JSON.parse(data.data).CUSTPICKUPS;
                console.log('in');
                this.newCustomPickupRoutes = parsed;
                console.log(this.newCustomPickupRoutes);
                this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutes;
                console.log(this.newCustomPickupRoutesJSONtoArray);
                this.startingPoint = this.newCustomPickupRoutesJSONtoArray[0].PICKUP_ADDRESS;
                console.log(this.startingPoint);
            }, err => {
                console.log("native error", err);
            });
        });
    }
    ngOnInit() {
    }
    //
    startRoute() {
        console.log("the route has started");
        setTimeout((isClick) => { this.router.navigate(['map']); }, 3000);
        console.log("2", JSON.stringify(this.dataFromRouteListJSON));
        console.log("3", JSON.stringify(this.dataFromRouteListLoginJSON));
        console.log('fixed');
        this.router.navigate(["map/" + JSON.stringify(this.dataFromRouteListJSON) + '/' + JSON.stringify(this.dataFromRouteListLoginJSON)]);
    }
    getPassengersInformationFromRouteListSelection() {
        console.log('%c Routing to Passengers List', 'color:orange;');
        console.log(this.dataFromRouteListJSON);
        this.router.navigate(['routepassengers/' + JSON.stringify(this.dataFromRouteListLoginJSON) + '/' + JSON.stringify(this.dataFromRouteListJSON) + '/' + JSON.stringify(this.dataFromRouteListJSON)]);
    }
    getRoutingInformationPathFromRouteListSelection() {
        console.log('%c Routing to Route Path Details', 'color:yellow;');
        console.log(this.dataFromRouteListJSON);
        this.router.navigate(['routepassengers/' + JSON.stringify(this.dataFromRouteListJSON)]);
    }
    goBackToRouteListPage() {
        console.log('%c Going back to Routelist Page', 'color:red;');
        console.log(this.dataFromRouteListLoginJSON);
        this.router.navigate(['routelist/' + JSON.stringify(this.dataFromRouteListLoginJSON)]);
    }
    beforeRouteStartedGoTechInspectPage() {
        console.log('%c Going to Check Vehicle | DRIVER ID | ', 'color:red;');
        console.log(this.dataFromRouteListLoginJSON);
        console.log('%c Going to Check Vehicle | Route Information | ', 'color:red;');
        console.log(this.dataFromRouteListJSON);
        this.router.navigate(['techinspect/' + JSON.stringify(this.dataFromRouteListJSON) + '/' + JSON.stringify(this.dataFromRouteListLoginJSON)]);
    }
    goToWalletPageWithDriverId() {
        console.log('%c Going to Wallet Page | DRIVER ID |', 'color:pink;');
        console.log(this.dataFromRouteListLoginJSON);
        this.router.navigate(['wallet/' + JSON.stringify(this.dataFromRouteListLoginJSON) + '/' + JSON.stringify(this.dataFromRouteListJSON)]);
    }
    goToRouteDetails() {
        console.log('oute of check1', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
        console.log('oute of check1', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
        console.log('oute of check1', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
        console.log('oute of check1', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
        console.log('%c Going to RouteDetails | DRIVER ID |', 'color:pink;');
        console.log(this.dataFromRouteListLoginJSON);
        console.log(JSON.stringify(this.dataFromRouteListLoginJSON));
        console.log('%c Going to RouteDetails | RPT DRV ROUTES |', 'color:pink;');
        console.log(this.dataFromRouteListJSON);
        console.log(JSON.stringify(this.dataFromRouteListJSON));
        console.log('%c Going to RouteDetails | PickUps |', 'color:pink;');
        console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
        console.log(JSON.stringify(this.newCustomPickupRoutes));
        this.router.navigate(['routedetails/' + JSON.stringify(this.dataFromRouteListLoginJSON) + '/' + JSON.stringify(this.dataFromRouteListJSON)]);
    }
    navigateToWalletPage() {
        console.log('%c GOing to Wallet Page | DRIVER ID |', 'color:pink;');
        console.log(this.dataFromRouteListLoginJSON);
        this.router.navigate(['wallet/' + JSON.stringify(this.dataFromRouteListLoginJSON) + '/' + this.dataFromRouteListJSON]);
    }
    navigateToMapPage() {
        //	this.router.navigate(["map/" + JSON.stringify(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS) + '/' + JSON.stringify(this.dataFromRouteListJSON) + '/' + JSON.stringify(this.dataFromRouteListJSON)]);                                       // pickups  
    }
    takePicture4() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const image = yield Camera.getPhoto({
                quality: 90,
                allowEditing: true,
                resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_4__["CameraResultType"].Base64
            });
            this.img = image.base64String;
            console.log(image);
        });
    }
    takePhotoWithNativeHttpRequest() {
        this.nativeHttp.setDataSerializer('urlencoded');
        var formData2 = {
            photo: this.img
        };
        let headers = {
            "Accept": "application/json",
            "api-auth": 'apiAuthToken String',
            "User-Auth": 'userAuthToken String'
        };
        this.nativeHttp.setDataSerializer('urlencoded');
        this.nativeHttp.setHeader('*', 'Content-Type', 'application/x-www-form-urlencoded');
        var date = new Date().getHours();
        var date2 = new Date().getMinutes();
        var kati = date + "_" + date2;
        this.nativeHttp.post('http://cf11.travelsoft.gr/itourapi/chrbus_drv_img.cfm?driver_id=16&srv_type=CHT&srv_code=2&sp_id=1&sp_code=6&fromd=2020/11/27&tod=2020/11/27&vehicle_map_id=1025&vhc_id=1&vhc_plates=VFR111&version_id=1&VechicleTypeID=1&virtualversion_id=1&img_type=TOLL&latitude=37.865044&longitude=23.755045&pickup_address=kapou&first_name=christos24&last_name=christos24&time=' + kati + '&userid=dmta', formData2, headers)
            .then(data => {
            console.log(data);
        })
            .catch(error => { console.log(error); });
    }
    navigateToPassengersPage() {
        this.router.navigate(["routepassengers"]);
    }
    navigateToStartFeedPage() {
        this.router.navigate(['routelist/' + this.Id + '/' + this.personId]);
    }
    navigateToSettingsPage() {
        this.router.navigate(['settings']);
    }
    navigateToRouteDetails() {
        this.router.navigate(['routedetails']);
    }
    navigateToVehiclePage() {
        this.router.navigate(['techinspect']);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(['routehistory']);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(['techhistory']);
    }
    navigateToProfilePage() {
        this.router.navigate(['profile']);
    }
};
RoutestartedPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__["HTTP"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] }
];
RoutestartedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-routestarted',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./routestarted.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/routestarted/routestarted.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./routestarted.page.scss */ "./src/app/routestarted/routestarted.page.scss")).default]
    })
], RoutestartedPage);



/***/ })

}]);
//# sourceMappingURL=routestarted-routestarted-module-es2015.js.map