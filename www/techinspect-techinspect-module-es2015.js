(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["techinspect-techinspect-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect/techinspect.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect/techinspect.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home2/routelist\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToLoginPage()\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n\n\n<div class=\"ion-page\" id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'vehicleCheck-check.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n  <ion-content>\n    <ion-list>\n    <ion-label class=\"ion-text-center\">\n      <p class=\"titleStyle\">{{'vehicleCheck-pleaseCheck.title' | translate}}</p>\n    </ion-label>\n  <!--   <ion-list class=\"ionListClass\">\n      <ion-button [ngClass]=\"{'btnStyleChanged' : isChanged }\" (click)=\"color()\"  class=\"btnStyle\">TIRES</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button   class=\"btnStyle\">FUEL</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button class=\"btnStyle\">ENGINE</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button  class=\"btnStyle\">BUS</ion-button>\n    </ion-list> -->\n    <form [formGroup]=\"commentForm\" (ngSubmit)=\"submitWithEveryHttp()\">\n\n     <div *ngIf=\"eng\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'vehicleCheck-checkArea.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select interface=\"action-sheet\" formControlName=\"measurementData\" [(ngModel)]=\"checkpointen\"  >\n          <ion-select-option  class=\"custom-options\" *ngFor=\"let enlang of englishLanguageJSON; let i = index\" [value]=\"enlang\">\n                      {{enlang.CHECKPOINT}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n    </div>\n\n     <div *ngIf=\"el\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'vehicleCheck-checkArea.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select [(ngModel)]=\"checkpointel\"  interface=\"action-sheet\">\n          <ion-select-option  *ngFor=\"let ellang of ellangs\" [value]=\"ellang\">\n                      {{ellang}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n\n\n    </div>\n\n    <!-- <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button (click)=\"wannaBeData()\" class=\"adBtn2\">\n        {{'vehicleCheck-start.title' | translate}}\n      </button>\n    </ion-buttons> -->\n  \n    <ion-list class=\"ionListClass\">  \n      <textarea formControlName=\"comment\" type=\"text\" class=\"txtArea\" rows=\"4\" cols=\"50\" [(ngModel)]=\"amount\"  (change)=check(amount)>\n        {{'vehicleCheck-comments.title' | translate}}\n      </textarea>\n      <div *ngFor=\"let error of errorMessages.comment\">\n        <ng-container *ngIf=\"comment.hasError(error.type) && (comment.dirty || comment.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n    </ion-list>\n    <div class=\"myiontab\">\n\n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n        <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n          <button class=\"adBtn2\">\n            {{'vehicleCheck-start.title' | translate}}\n          </button>\n        </ion-buttons>\n       \n      </ion-tab-bar>\n    \n    </div>\n    <div class=\"myiontab2\">\n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n      <ion-buttons  (click)=\"navigateToroutelist()\"  class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n        <button  (click)=\"navigateToroutelist()\" class=\"adBtn2\">\n          CANCEL\n        </button>\n      </ion-buttons>      \n    </ion-tab-bar>\n    </div> \n  </form>\n\n    </ion-list>\n  </ion-content>\n  \n</div>\n\n\n\n\n\n <!-- <div class=\"myiontab\">\n\n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n    <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button class=\"adBtn2\">\n        {{'vehicleCheck-start.title' | translate}}\n      </button>\n    </ion-buttons>\n  </ion-tab-bar>\n\n</div>\n<div class=\"myiontab2\">\n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n  <ion-buttons  (click)=\"navigateToroutelist()\" type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <button  (click)=\"navigateToroutelist()\" class=\"adBtn2\">\n      CANCEL\n    </button>\n  </ion-buttons>      \n</ion-tab-bar>\n</div> -->\n");

/***/ }),

/***/ "./src/app/techinspect/techinspect-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/techinspect/techinspect-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: TechinspectPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectPageRoutingModule", function() { return TechinspectPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _techinspect_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./techinspect.page */ "./src/app/techinspect/techinspect.page.ts");




const routes = [
    {
        path: '',
        component: _techinspect_page__WEBPACK_IMPORTED_MODULE_3__["TechinspectPage"]
    }
];
let TechinspectPageRoutingModule = class TechinspectPageRoutingModule {
};
TechinspectPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TechinspectPageRoutingModule);



/***/ }),

/***/ "./src/app/techinspect/techinspect.module.ts":
/*!***************************************************!*\
  !*** ./src/app/techinspect/techinspect.module.ts ***!
  \***************************************************/
/*! exports provided: TechinspectPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectPageModule", function() { return TechinspectPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _techinspect_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./techinspect-routing.module */ "./src/app/techinspect/techinspect-routing.module.ts");
/* harmony import */ var _techinspect_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./techinspect.page */ "./src/app/techinspect/techinspect.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let TechinspectPageModule = class TechinspectPageModule {
};
TechinspectPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _techinspect_routing_module__WEBPACK_IMPORTED_MODULE_5__["TechinspectPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_techinspect_page__WEBPACK_IMPORTED_MODULE_6__["TechinspectPage"]]
    })
], TechinspectPageModule);



/***/ }),

/***/ "./src/app/techinspect/techinspect.page.scss":
/*!***************************************************!*\
  !*** ./src/app/techinspect/techinspect.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\n.error-message {\n  position: absolute;\n  top: 120px;\n  left: 1px;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-red);\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.my-custom-interface .select-interface-option {\n  color: red;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.labelIon {\n  position: absolute;\n  font-size: 20px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.itemIon {\n  width: 100%;\n  margin-top: 10%;\n}\n\nion-select {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding: 4px 5px 5px 29px;\n  text-align: center;\n}\n\n.centerItems {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n}\n\n.titleStyle {\n  position: relative;\n  top: -18px;\n  left: 0px;\n  color: var(--ion-color-ibus-darkblue);\n  font-size: 21px;\n  z-index: 9999;\n  margin-top: 40px;\n  font-family: monteBold;\n}\n\n.titleStyle {\n  font-size: 25px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-darkblue);\n  padding: 37px;\n}\n\n.ionListClass {\n  justify-content: center;\n  display: flex !important;\n  align-items: center !important;\n}\n\n.ionListClass .btnStyle {\n  width: 88%;\n  height: 50px;\n  border-radius: 12px;\n  --background: var(--ion-color-ibus-theblue);\n  font-family: monteBold;\n  font-size: 16px;\n  --background-activated: red;\n}\n\n.ionListClass .btnStyleChanged {\n  --background: var(--ion-color-ibus-lightgreen);\n}\n\n.ionListClass .txtArea {\n  width: 91%;\n  height: 150px;\n  border-radius: 8px;\n  border: none;\n  background-color: var(--ion-color-ibus-theblue);\n  color: white;\n  font-family: monteSBold;\n  font-size: 16px;\n  padding: 56px;\n  margin-top: 1%;\n}\n\nionic-selectable {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding-top: 8px;\n  opacity: 1 !important;\n  text-align: center;\n}\n\n.myiontab ion-tab-bar {\n  justify-content: center;\n  display: flex;\n  width: 100%;\n  height: 90px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-classicgreen);\n  border-radius: 70px;\n  width: 80px;\n  height: 80px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab2 ion-tab-bar {\n  justify-content: center;\n  display: flex;\n  width: 100%;\n  height: 90px;\n}\n\n.myiontab2 ion-tab-bar .addBtn {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab2 ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-red);\n  border-radius: 70px;\n  width: 80px;\n  height: 80px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab2 ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab2 ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.bottomBar .ionTabBar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\n\n.bottomBar .ionTabBar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.bottomBar .ionTabBar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.bottomBar .ionTabBar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.bottomBar .ionTabBar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.bottomBar .ionTabBar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.bottomBar .ionTabBar svg path {\n  fill: var(--ion-color-light);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.centerTitle {\n  justify-content: center;\n  width: 100%;\n  display: flex;\n  text-align: center;\n  margin-top: 6%;\n}\n\nion-select::part(icon) {\n  display: none !important;\n}\n\n.srchBtn {\n  width: 100%;\n  margin-top: 6%;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 13px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 10px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 8px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVjaGluc3BlY3QvdGVjaGluc3BlY3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNENBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSx5QkFBQTtFQUNBLHFDQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxxQ0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EscUNBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0EsOEJBQUE7QUFDSjs7QUFDSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0FBQ1I7O0FBRUk7RUFDSSw4Q0FBQTtBQUFSOztBQUdJO0VBQ0ksVUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSwrQ0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQURSOztBQU1BO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQUhKOztBQVFJO0VBQ0ksdUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFMUjs7QUFPUTtFQUVJLDBDQUFBO0VBRUEsaUJBQUE7QUFQWjs7QUFRWTtFQUNJLGtCQUFBO0VBQ0Esb0RBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQU5oQjs7QUFTWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFQaEI7O0FBVVk7RUFDSSx5QkFBQTtBQVJoQjs7QUFpQkk7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQWRSOztBQWdCUTtFQUVJLDBDQUFBO0VBRUEsaUJBQUE7QUFoQlo7O0FBaUJZO0VBQ0ksa0JBQUE7RUFDQSwyQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBZmhCOztBQWtCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFoQmhCOztBQW1CWTtFQUNJLHlCQUFBO0FBakJoQjs7QUFzRUk7RUFDSSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7QUFuRVI7O0FBcUVRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBbkVaOztBQXNFUTtFQUNJLG9DQUFBO0FBcEVaOztBQXVFUTtFQUNJLGlCQUFBO0VBQ0EsNkJBQUE7QUFyRVo7O0FBd0VRO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtBQXRFWjs7QUF5RVE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7QUF2RVo7O0FBeUVZO0VBQ0ksNEJBQUE7QUF2RWhCOztBQThFQTtFQUNJLHFDQUFBO0FBM0VKOztBQThFQTtFQUNJLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUEzRUo7O0FBOEVBO0VBQ0ksd0JBQUE7QUEzRUo7O0FBOEVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7QUEzRUo7O0FBOEVBO0VBQ0k7SUFDSSxxQkFBQTtJQUNBLGFBQUE7RUEzRU47O0VBOEVFO0lBQ0ksdUJBQUE7SUFDQSx3QkFBQTtJQUNBLDhCQUFBO0VBM0VOO0VBNkVNO0lBQ0ksVUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLDJDQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0lBQ0EsMkJBQUE7RUEzRVY7RUE4RU07SUFDSSw4Q0FBQTtFQTVFVjtFQStFTTtJQUNJLFVBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0lBQ0EsK0NBQUE7SUFDQSxZQUFBO0lBQ0EsdUJBQUE7SUFDQSxlQUFBO0lBQ0EsYUFBQTtJQUNBLGNBQUE7RUE3RVY7QUFDRjs7QUFrRkE7RUFDSTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQWhGTjtBQUNGOztBQW1GQTtFQUNJO0lBQ0kscUJBQUE7SUFDQSxZQUFBO0VBakZOOztFQW9GRTtJQUNJLGtCQUFBO0lBQ0EsVUFBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtFQWpGTjs7RUFvRkU7SUFDSSx1QkFBQTtJQUNBLHdCQUFBO0lBQ0EsOEJBQUE7RUFqRk47RUFtRk07SUFDSSxVQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsMkNBQUE7SUFDQSxzQkFBQTtJQUNBLGVBQUE7SUFDQSwyQkFBQTtFQWpGVjtFQW9GTTtJQUNJLDhDQUFBO0VBbEZWO0VBcUZNO0lBQ0ksVUFBQTtJQUNBLGFBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7SUFDQSwrQ0FBQTtJQUNBLFlBQUE7SUFDQSx1QkFBQTtJQUNBLGVBQUE7SUFDQSxhQUFBO0lBQ0EsY0FBQTtFQW5GVjtBQUNGOztBQXdGQTtFQUNJO0lBQ0ksdUJBQUE7SUFDQSx3QkFBQTtJQUNBLDhCQUFBO0VBdEZOO0VBd0ZNO0lBQ0ksVUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLDJDQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0lBQ0EsMkJBQUE7RUF0RlY7RUF5Rk07SUFDSSw4Q0FBQTtFQXZGVjtFQTBGTTtJQUNJLFVBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0lBQ0EsK0NBQUE7SUFDQSxZQUFBO0lBQ0EsdUJBQUE7SUFDQSxjQUFBO0lBQ0EsYUFBQTtJQUNBLGNBQUE7RUF4RlY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3RlY2hpbnNwZWN0L3RlY2hpbnNwZWN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbn1cclxuXHJcbi5pb3MgaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgaGVpZ2h0OiAxMjdweDtcclxufVxyXG5cclxuLm1kIGlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogODdweDtcclxufVxyXG5cclxuLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxMjBweDtcclxuICAgIGxlZnQ6IDFweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlUmVndWxhcjtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1yZWQpO1xyXG59XHJcblxyXG4ubm90QnRuIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiA0N3B4O1xyXG4gICAgaGVpZ2h0OiA2MnB4O1xyXG59XHJcblxyXG4ubWVudUJ0biB7XHJcbiAgICBmb250LXNpemU6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5teS1jdXN0b20taW50ZXJmYWNlIC5zZWxlY3QtaW50ZXJmYWNlLW9wdGlvbiB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4ubWVudVR4dCB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVSZWd1bGFyO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG5cclxuLmljb25TdHlsaW5nIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbi5tZW51VG9vbGJhciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA4MHB4O1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG59XHJcblxyXG4uaW9zICNwcm9maWxlIHtcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAyN3B4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL3Byb2ZpbGUuanBnKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogNjBweCA4MHB4O1xyXG59XHJcblxyXG4uaW9zIGlvbi10aXRsZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuXHJcbi5tZCAjcHJvZmlsZSB7XHJcbiAgICB3aWR0aDogNjBweDtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMjdweDtcclxuICAgIG1hcmdpbi10b3A6IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9wcm9maWxlLmpwZyk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDYwcHggODBweDtcclxufVxyXG5cclxuLmlvcyAubmF2LWJ1dHRvbnMge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLm1kIGlvbi10aXRsZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcblxyXG4ubWQgLm5hdi1idXR0b25zIHtcclxuICAgIG1hcmdpbi1ib3R0b206IC0xNXB4O1xyXG59XHJcblxyXG4ubGFiZWxJb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbi5pdGVtSW9uIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG59XHJcblxyXG5pb24tc2VsZWN0IHtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIHBhZGRpbmc6IDRweCA1cHggNXB4IDI5cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5jZW50ZXJJdGVtcyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLnRpdGxlU3R5bGUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtMThweDtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbiAgICB6LWluZGV4OiA5OTk5O1xyXG4gICAgbWFyZ2luLXRvcDogNDBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbn1cclxuXHJcbi50aXRsZVN0eWxlIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIHBhZGRpbmc6IDM3cHg7XHJcbn1cclxuXHJcbi5pb25MaXN0Q2xhc3Mge1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgLmJ0blN0eWxlIHtcclxuICAgICAgICB3aWR0aDogODglO1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogcmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC5idG5TdHlsZUNoYW5nZWQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWlidXMtbGlnaHRncmVlbik7XHJcbiAgICB9XHJcblxyXG4gICAgLnR4dEFyZWEge1xyXG4gICAgICAgIHdpZHRoOiA5MSU7XHJcbiAgICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgcGFkZGluZzogNTZweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxJTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmlvbmljLXNlbGVjdGFibGUge1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDhweDtcclxuICAgIG9wYWNpdHk6IDEgIWltcG9ydGFudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLm15aW9udGFiIHtcclxuXHJcbiAgICBpb24tdGFiLWJhciB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDkwcHg7XHJcblxyXG4gICAgICAgIC5hZGRCdG4ge1xyXG5cclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pO1xyXG5cclxuICAgICAgICAgICAgLyogZml4IG5vdGNoIGlvcyovXHJcbiAgICAgICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1jbGFzc2ljZ3JlZW4pO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICAgICAgc3Ryb2tlOiA1cHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlvbi1iYWRnZSB7XHJcbiAgICAgICAgICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufVxyXG5cclxuLm15aW9udGFiMiB7XHJcblxyXG4gICAgaW9uLXRhYi1iYXIge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiA5MHB4O1xyXG5cclxuICAgICAgICAuYWRkQnRuIHtcclxuXHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcclxuXHJcbiAgICAgICAgICAgIC8qIGZpeCBub3RjaCBpb3MqL1xyXG4gICAgICAgICAgICBidXR0b24ge1xyXG4gICAgICAgICAgICAgICAgLS1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtcmVkKTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogODBweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogODBweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgICAgIHN0cm9rZTogNXB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpb24tYmFkZ2Uge1xyXG4gICAgICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLSAhISEhISEgLS0tLS0tLS0tLS0tLS0tLSAvLyBcclxuLy8gQm90dG9tQmFyIENTUyBcclxuXHJcblxyXG5cclxuLy8gLnN0YXJ0QnV0dG9uQ2xhc3N7XHJcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuLy8gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgIHRvcDogLTEwcHg7XHJcbi8vICAgICBoZWlnaHQ6IDYwcHg7XHJcbi8vICAgICBib3JkZXI6IG5vbmU7XHJcbi8vICAgICAuYWRkQnRuIHtcclxuLy8gICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG4vLyAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgICAgICB0b3A6IDlweDtcclxuLy8gICAgICAgICBidXR0b24ge1xyXG4vLyAgICAgICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWNsYXNzaWNncmVlbik7XHJcbi8vICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbi8vICAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xyXG4vLyAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbi8vICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgICAgICAgICAgdG9wOiAtOXB4O1xyXG4vLyAgICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuLy8gICAgICAgICAgICAgZm9udC1mYW1pbHk6bW9udGVTQm9sZDtcclxuLy8gICAgICAgICAgICAgZm9udC1zaXplOjExcHg7XHJcbi8vICAgICAgICAgICAgIGJvdHRvbTogMHB4O1xyXG4vLyAgICAgICAgIH1cclxuLy8gICAgICAgICBpb24taWNvbntcclxuLy8gICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbi8vICAgICAgICAgICAgIHdpZHRoOjMwcHg7XHJcbi8vICAgICAgICAgICAgIGhlaWdodDozMHB4O1xyXG4vLyAgICAgICAgICAgICBzdHJva2U6IDVweDtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgICAgaW9uLWJhZGdle1xyXG4vLyAgICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbi8vICAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gfVxyXG4uYm90dG9tQmFyIHtcclxuXHJcbiAgICAvLyAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLy8gaGVpZ2h0OiA2MHB4O1xyXG4gICAgLmlvblRhYkJhciB7XHJcbiAgICAgICAgLS1ib3JkZXI6IDA7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICAgICY6YWZ0ZXIge1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgICAgICAgICAgaGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi10YWItYnV0dG9uIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW9uLXRhYi1idXR0b24uY29tbWVudHMge1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpb24tdGFiLWJ1dHRvbi5ub3RpZnMge1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc3ZnIHtcclxuICAgICAgICAgICAgd2lkdGg6IDcycHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDE5cHg7XHJcblxyXG4gICAgICAgICAgICBwYXRoIHtcclxuICAgICAgICAgICAgICAgIGZpbGw6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLSAhISEhISEgLS0tLS0tLS0tLS0tLS0tLSAvLyBcclxuLmljb25TdHlsaW5nIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbi5jZW50ZXJUaXRsZSB7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDYlO1xyXG59XHJcblxyXG5pb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnNyY2hCdG4ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiA2JTtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MTRweCkge1xyXG4gICAgaW9uLXRvb2xiYXIge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDEyN3B4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pb25MaXN0Q2xhc3Mge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgIC5idG5TdHlsZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4OCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiByZWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYnRuU3R5bGVDaGFuZ2VkIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC50eHRBcmVhIHtcclxuICAgICAgICAgICAgd2lkdGg6IDkxJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDU2cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDElO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDM3NXB4KSB7XHJcbiAgICAuaW9zIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pb3MgaW9uLXRpdGxlIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAtMTVweDtcclxuICAgICAgICBsZWZ0OiAtMTVweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pb25MaXN0Q2xhc3Mge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgIC5idG5TdHlsZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4OCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiByZWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYnRuU3R5bGVDaGFuZ2VkIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC50eHRBcmVhIHtcclxuICAgICAgICAgICAgd2lkdGg6IDkxJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDU2cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDElO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgIC5pb25MaXN0Q2xhc3Mge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICAgIC5idG5TdHlsZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4OCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiByZWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuYnRuU3R5bGVDaGFuZ2VkIHtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC50eHRBcmVhIHtcclxuICAgICAgICAgICAgd2lkdGg6IDkxJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDhweDtcclxuICAgICAgICAgICAgcGFkZGluZzogNTZweDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMSU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/techinspect/techinspect.page.ts":
/*!*************************************************!*\
  !*** ./src/app/techinspect/techinspect.page.ts ***!
  \*************************************************/
/*! exports provided: TechinspectPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectPage", function() { return TechinspectPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");









let TechinspectPage = class TechinspectPage {
    constructor(platform, nativeHttp, activatedRoute, loadingCtrl, http, router, formBuilder) {
        this.platform = platform;
        this.nativeHttp = nativeHttp;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.router = router;
        this.formBuilder = formBuilder;
        this.techchk_json = [{ "checkpoint_id": "33", "checkpoint_gre": "Τα στοιχεία του οχήματος (πινακίδες κυκλοφορίας, αρ. πλαισίου, αρ. κινητήρα σύμφωνα με την άδεια. κυκλοφορίας).", "checkpoint_eng": "Vehicle details (registration plates, chassis number, engine number according to the license. Registration)." },
            { "checkpoint_id": "34", "checkpoint_gre": "Το όχημα εσωτερικά- εξωτερικά (οπτικώς).", "checkpoint_eng": "The vehicle internally-externally (visually)." },
            { "checkpoint_id": "35", "checkpoint_gre": "Ο εξοπλισμός του οχήματος (τρίγωνο, πυροσβεστήρας, φαρμακείο).", "checkpoint_eng": "The equipment of the vehicle (triangle, fire extinguisher, pharmacy)." },
            { "checkpoint_id": "36", "checkpoint_gre": "Η πίεση των ελαστικών- η ύπαρξη ρεζέρβας.", "checkpoint_eng": "The tire pressure - the existence of a reserve." },
            { "checkpoint_id": "37", "checkpoint_gre": "Η ποιότητα και η σύνθεση των καυσαερίων.", "checkpoint_eng": "The quality and composition of the exhaust gases." },
            { "checkpoint_id": "38", "checkpoint_gre": "Η σύγκλιση- απόκλιση των τροχών.", "checkpoint_eng": "The convergence-deviation of the wheels." },
            { "checkpoint_id": "39", "checkpoint_gre": "Η απόδοση της ανάρτησης.", "checkpoint_eng": "Post performance." },
            { "checkpoint_id": "40", "checkpoint_gre": "Η απόδοση των φρένων.", "checkpoint_eng": "The performance of the brakes." },
            { "checkpoint_id": "41", "checkpoint_gre": "Τα φώτα ως προς την ένταση και την κλίση τους- γενική λειτουργία.", "checkpoint_eng": "Lights in terms of intensity and inclination - general function." },
            { "checkpoint_id": "42", "checkpoint_gre": "Το σύστημα διεύθυνσης.", "checkpoint_eng": "The steering system." },
            { "checkpoint_id": "43", "checkpoint_gre": "Το σύστημα μετάδοσης κίνησης.", "checkpoint_eng": "The drive system." },
            { "checkpoint_id": "44", "checkpoint_gre": "Οι άξονες – οι τροχοί- τα ελαστικά – η ανάρτηση.", "checkpoint_eng": "The axles - the wheels - the tires - the suspension." },
            { "checkpoint_id": "45", "checkpoint_gre": "Οι δίσκοι πέδησης.", "checkpoint_eng": "The brake discs." },
            { "checkpoint_id": "46", "checkpoint_gre": "Οι εύκαμπτες – άκαμπτες σωληνώσεις του συστήματος πέδησης.", "checkpoint_eng": "The flexible - rigid piping of the braking system." },
            { "checkpoint_id": "47", "checkpoint_gre": "Το κάτω μέρος του αμαξώματος (έλεγχος οξειδώσεων).", "checkpoint_eng": "The lower part of the body (oxidation control)." },
            { "checkpoint_id": "48", "checkpoint_gre": "Η εξάτμιση.", "checkpoint_eng": "The exhaust." }];
        this.langs = ["Ελληνικά", "English"];
        this.language = localStorage.getItem('lang');
        this.eng = false;
        this.el = false;
        this.com = false;
        this.imageChosen = 0;
        this.greekLanguage = [];
        this.englishLanguage = [];
        this.englishLanguageJSON = [];
        this.myCommentForm = [];
        this.checkThePoint = [];
        this.checkThePointComment = [];
        this.errorMessages = {
            comment: [
                { type: 'required', message: 'Comment is Required' },
                { type: 'maxlength', message: 'This field should be no longer than 200 characters' }
            ],
            measurementData: [
                {
                    type: 'reuired', message: 'Fill Data'
                }
            ]
        };
        this.commentForm = this.formBuilder.group({
            comment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(20)]],
            measurementData: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]]
        });
        this.serviceRegistration = "";
        this.dataFromService = "";
        const selects = document.querySelectorAll('.custom-options');
        //greg 
        this.dataFromStartedRouteWhileGoingBack = this.activatedRoute.snapshot.paramMap.get('dataFromRoute');
        this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverId = this.activatedRoute.snapshot.paramMap.get('datafromdriverId');
        this.dataFromStartedRouteWhileGoingBackJSON = JSON.parse(this.dataFromStartedRouteWhileGoingBack);
        this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON = JSON.parse(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverId);
        console.log('%c DATA FROM ROUTELIST JSON', 'color:orange;');
        console.log(this.dataFromStartedRouteWhileGoingBackJSON);
        console.log('%c DATA FROM ROUTELIST LOGIN JSON', 'color:yellow;');
        console.log(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON);
        this.personId = this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON;
        this.Id = this.dataFromStartedRouteWhileGoingBackJSON.PERSON_ID;
        this.enlangs = [];
        this.ellangs = [];
        this.ids = [];
        var k = 0;
        console.log(this.language);
        if (this.language == "en") {
            if (this.platform.is('cordova')) {
                let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=eng&userid=dmta', {}, {
                    'Content-Type': 'application/json'
                });
                Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => console.log('')))
                    .subscribe((data) => {
                    console.log(data);
                    let parsed = JSON.parse(data.data).CHECKS;
                    this.englishLanguageJSON = parsed;
                    this.test1 = this.englishLanguage;
                    console.log('%c English Language', 'color:orange;');
                    console.log(this.englishLanguageJSON);
                });
                this.el = false;
                this.eng = true;
            }
            else {
                this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=eng&userid=dmta').subscribe((data) => {
                    console.log(data);
                    this.englishLanguage = data;
                    console.log('%c English Language', 'color:orange;');
                    this.englishLanguageJSON = this.englishLanguage.CHECKS;
                    console.log(this.englishLanguageJSON);
                });
                this.el = false;
                this.eng = true;
            }
        }
        else if (this.language == "gr") {
            this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=gre&userid=dmta').subscribe((data) => {
                this.greekLanguage = data;
                console.log('%c Greek Language', 'color:orange;');
                this.greekLanguageJSON = this.greekLanguage.DATA;
                console.log(this.greekLanguageJSON);
            });
            this.el = true;
            this.eng = false;
        }
    }
    //greg
    get measurementData() {
        return this.commentForm.get('measurementData');
    }
    get comment() {
        return this.commentForm.get('comment');
    }
    ngOnInit() {
    }
    submitWithEveryHttp() {
        this.platform.is('cordova') ? this.submitNativeClient() : this.submitHttpClient();
    }
    submitHttpClient() {
        console.log('%c MY ENGLISH JSON', 'color:orange;');
        console.log(this.commentForm.value);
        this.myCommentForm = this.commentForm.value;
        console.log(this.myCommentForm.measurementData);
        this.checkThePointComment = this.myCommentForm.comment;
        console.log(this.checkThePointComment);
        this.checkThePoint = this.myCommentForm.measurementData;
        console.log(this.checkThePoint.CHECKPOINT);
        console.log();
        //
        this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_inspect_add.cfm?'
            + 'vhc_plates=' + this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES
            + '&chrbus_code=' + this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE
            + '&chrbus_sp_id=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.CHRBUS_SP_ID
            + '&sp_code=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.SP_CODE
            + '&fromd=' + this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE
            + '&tod=' + this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE
            + '&driver_id=' + this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID
            + '&checkpoint_id=' + this.checkThePoint.CHECKPOINT_ID
            + '&checkpoint_txt=' + this.checkThePoint.CHECKPOINT
            + '&checkpoint_status=' + this.checkThePoint.FLAG
            + '&comment=' + this.checkThePointComment
            + '&userid=dmta').subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(data);
            let loader = yield this.loadingCtrl.create({
                message: "Tech Inspect SuccessFully Done"
            });
            loader.present();
            setTimeout(() => {
                loader.dismiss();
                this.router.navigate(['routestarted/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackJSON) + '/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON)]);
            }, 1000);
        }));
    }
    submitNativeClient() {
        console.log('%c MY ENGLISH JSON', 'color:orange;');
        console.log(this.commentForm.value);
        this.myCommentForm = this.commentForm.value;
        console.log(this.myCommentForm.measurementData);
        this.checkThePointComment = this.myCommentForm.comment;
        console.log(this.checkThePointComment);
        this.checkThePoint = this.myCommentForm.measurementData;
        console.log(this.checkThePoint.CHECKPOINT);
        console.log();
        console.log("tod");
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID);
        console.log(this.checkThePoint.CHECKPOINT_ID);
        console.log(this.checkThePoint.CHECKPOINT);
        console.log(this.checkThePoint.FLAG);
        console.log(this.checkThePointComment);
        // let fromDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE;
        // let fromDateToGo = fromDate.split(/\s/).join('');
        // let fromDateToGo3 = fromDateToGo.replace('January,','');
        // let fromDateToGo4 = fromDateToGo3.replace(':','')
        // let fromDateToGo5 = fromDateToGo4.replace('0','')
        // console.log("DATE5",fromDateToGo5);
        // let toDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE;
        // let toDateToGo = toDate.split(/\s/).join('');
        // let toDateToGo3 = toDateToGo.replace('January,','');
        // let toDateToGo4 = toDateToGo3.replace(':','')
        // let toDateToGo5 = toDateToGo4.replace('0','')
        let fromDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE;
        let fromDateToGo555 = fromDate.split(/\s/).join(',');
        console.log("from Date", fromDate);
        let fromDateToGo = new Date(fromDateToGo555);
        console.log("from Date 2 ", fromDateToGo);
        let year = fromDateToGo.getFullYear();
        console.log("year", year);
        let month = fromDateToGo.getMonth() + 1;
        console.log(month);
        let date = fromDateToGo.getDate();
        console.log(date);
        let fulldateFromDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        let toDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE;
        let toDateToGo555 = toDate.split(/\s/).join(',');
        console.log("from Date", toDate);
        let toDateToGo = new Date(toDateToGo555);
        console.log("from Date 2 ", toDateToGo);
        let year2 = toDateToGo.getFullYear();
        console.log("year", year);
        let month2 = toDateToGo.getMonth() + 1;
        console.log(month2);
        let date2 = toDateToGo.getDate();
        console.log(date2);
        let fulldateToDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        //     getDate(); 27
        // getFullYear(); 2020
        let checkpoint = this.checkThePoint.CHECKPOINT;
        let checkpoint3 = checkpoint.split(/\s/).join('');
        console.log(checkpoint3);
        let commentTime = this.checkThePointComment;
        let commentToGo = commentTime.split(/\s/).join('');
        let whiteSpace = ' A  B C D ';
        let noWhiteSpace = whiteSpace.trim();
        console.log(noWhiteSpace);
        //
        let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_inspect_add.cfm?'
            + 'vhc_plates=' + this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES
            + '&chrbus_code=' + this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE
            + '&chrbus_sp_id=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.CHRBUS_SP_ID
            + '&sp_code=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.SP_CODE
            + '&fromd=' + fulldateFromDate
            + '&tod=' + fulldateToDate
            + '&driver_id=' + this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID
            + '&checkpoint_id=' + this.checkThePoint.CHECKPOINT_ID
            + '&checkpoint_txt=' + checkpoint3
            + '&checkpoint_status=' + this.checkThePoint.FLAG
            + '&comment=' + commentToGo
            + '&userid=dmta', {}, {
            'Content-Type': 'application/x-www-form-urlencoded',
        });
        console.log('after url call');
        Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => console.log('Ready to send')))
            .subscribe(data => {
            console.log(data);
            this.router.navigate(['routestarted/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackJSON) + '/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON)]);
            console.log('LEITOURGW');
        }, err => {
            console.log('Error of Vehicle check', err);
        });
    }
    map() {
        setTimeout(() => {
            this.router.navigate(['routestarted/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackJSON) + '/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON)]);
        }, 1000);
    }
    navigateToroutelist() {
        this.router.navigate(['routelist/' + this.personId + '/' + this.Id]);
    }
    navigateToWalletPage() {
        this.router.navigate(['home2/wallet']);
    }
    navigateToSettingsPage() {
        this.router.navigate(['home2/settings']);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(['home2/routehistory']);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(['home2/techhistory']);
    }
    navigateToProfilePage() {
        this.router.navigate(['profile']);
    }
    navigateToLoginPage() {
        this.router.navigate(['login']);
    }
    // check(amount){
    //   if(this.language=="en"){
    //     alert( "Check " + this.checkpointen + " has been added with your comments " + this.amount);
    //   }
    //   else
    //     if(this.language=="gr"){
    //    alert( "Ο Έλεγχος " + this.checkpointen + " έχει προστεθεί μαζί με τα σχόλιά σος " + this.amount);
    //   }
    // }
    navigateToStartRoutePage() {
        this.router.navigate(['routestarted']);
    }
};
TechinspectPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__["HTTP"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] }
];
TechinspectPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-techinspect',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./techinspect.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect/techinspect.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./techinspect.page.scss */ "./src/app/techinspect/techinspect.page.scss")).default]
    })
], TechinspectPage);



/***/ })

}]);
//# sourceMappingURL=techinspect-techinspect-module-es2015.js.map