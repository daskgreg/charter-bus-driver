(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"loginpage\">\n  <ion-toolbar color=\"blue\">\n  as\n  </ion-toolbar>\n\n  <div class=\"center profile-img\">\n    <div class=\"profile-img\">\n  \n    </div>\n    <div class=\"login-desc\">\n      <a>{{'LOGIN-DETAILS.title' | translate}} </a>\n       <a>{{'LOGIN-ACCESS.title' | translate}}</a>\n    </div>\n  </div>\n\n \n  <form id=\"form\">\n    <ion-list>\n      <ion-item lines='none'>\n        <ion-input  name=\"phone\"  type=\"text\" [(ngModel)]=\"mobile\" placeholder=\"{{'LOGIN-PHONE.title' | translate}}\" required>\n        </ion-input>  <ion-icon slot=\"end\" name=\"call\" ios=\"call\"></ion-icon>\n      </ion-item>\n\n      <ion-item lines=\"none\">\n        <ion-input  type=\"password\" name=\"password\" [(ngModel)]=\"pass\" placeholder=\"{{'LOGIN-PASSWORD.title' | translate}}\" requred></ion-input>\n        <ion-icon (click)=\"saveTodos()\" slot=\"end\" name=\"eye\"></ion-icon>\n      </ion-item>\n      <ion-button class=\"btn-login\"  (click)=\"getLoginFromEveryPlatform()\" expand=\"block\" shape=\"round\" >{{'LOGIN.title' | translate}}</ion-button>\n      <ion-item class=\"forgot-password\" (click)=\"forgot()\" lines='none'>\n       {{'LOGIN-FORGOT.title' | translate}} \n      </ion-item>\n\n      <ion-item class=\"registernow\" lines='none'>\n        {{'LOGIN-ACCOUNT.title' | translate}}<a (click)=\"register()\">{{'LOGIN-REGISTER.title' | translate}}</a>\n      </ion-item>\n\n    </ion-list>\n  </form>\n  <!-- <firebase-ui></firebase-ui>\n  <!-- <h1 *ngIf=\"afAuth.currentUser\">Welcome {{afAuth.currentUser}} \n\n  </h1>-->\n\n  <!-- <ion-button (click)=\"signOut()\">Sign out</ion-button> -->\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var firebaseui_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebaseui-angular */ "./node_modules/firebaseui-angular/__ivy_ngcc__/fesm2015/firebaseui-angular.js");









let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            firebaseui_angular__WEBPACK_IMPORTED_MODULE_8__["FirebaseUIModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".loginpage {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -8px;\n}\n\n.toolbar-title {\n  font-size: 21px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 0px;\n}\n\n.registernow {\n  font-size: 13px;\n  border: none;\n  background-color: transparent;\n  font-family: monteMedium;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 12px;\n  left: 25px;\n}\n\n.forgot-password {\n  font-size: 13px;\n  border: none;\n  background-color: transparent;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 12px;\n  left: 50px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n  background-color: white;\n  position: relative;\n  top: 6px;\n}\n\nion-icon {\n  margin-top: 3%;\n  position: absolute;\n  right: 29px;\n  z-index: 2000;\n  color: var(--ion-color-ibus-blue);\n}\n\nion-input {\n  height: 48px;\n  font-size: 15px;\n  color: var(--ion-color-ibus-darkblue);\n  font-family: monteMedium;\n  height: 60px;\n  margin-top: 5%;\n  border-radius: 21px;\n  position: relative;\n  border: 2px solid var(--ion-color-ibus-blue);\n  text-align: center;\n}\n\n.btn-login {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 7%;\n}\n\n.login-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteMedium;\n  font-size: 17px;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  top: 9px;\n}\n\n.profileimg {\n  top: 50px;\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBRUo7O0FBQUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FBR0o7O0FBREE7RUFDSSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQUlKOztBQUZBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLHdCQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBS0o7O0FBSEE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFNSjs7QUFKQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUFPSjs7QUFMQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsaUNBQUE7QUFRSjs7QUFMQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSx3QkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLDRDQUFBO0VBQ0Esa0JBQUE7QUFRSjs7QUFKQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxtRUFBQTtFQUNBLGNBQUE7QUFPSjs7QUFMQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUFRSjs7QUFOQTtFQUNJLFNBQUE7RUFDQSxrQkFBQTtBQVNKIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2lucGFnZXtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgdG9wOjU1cHg7XHJcbiAgICB6LWluZGV4OjIwMDtcclxufVxyXG4uYmFjay1idG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDMwcHg7XHJcbiAgICBsZWZ0OjIwcHg7XHJcbiAgICB3aWR0aDo1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbi5hcnJvdy1iYWNre1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMTBweDtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4udG9vbGJhci10aXRsZXtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDBweDtcclxufVxyXG4ucmVnaXN0ZXJub3d7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6bm9uZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7XHJcbiAgICBmb250LWZhbWlseTptb250ZU1lZGl1bTtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDEycHg7XHJcbiAgICBsZWZ0OiAyNXB4O1xyXG59XHJcbi5mb3Jnb3QtcGFzc3dvcmR7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6bm9uZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAxMnB4O1xyXG4gICAgbGVmdDogNTBweDtcclxufVxyXG4jZm9ybSB7XHJcbiAgICBtYXJnaW4tdG9wOjVweDtcclxuICAgIHBhZGRpbmctbGVmdDozMHB4O1xyXG4gICAgcGFkZGluZy1yaWdodDozMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDo2cHg7XHJcbn1cclxuaW9uLWljb257XHJcbiAgICBtYXJnaW4tdG9wOiAzJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAyOXB4O1xyXG4gICAgei1pbmRleDogMjAwMDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG59XHJcblxyXG5pb24taW5wdXR7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlTWVkaXVtO1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgbWFyZ2luLXRvcDo1JTtcclxuICAgIGJvcmRlci1yYWRpdXM6MjFweDtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgYm9yZGVyOjJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIFxyXG59XHJcblxyXG4uYnRuLWxvZ2lue1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgaGVpZ2h0OjE4O1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6MTNweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjNEU5OEZGIDkwJSwjNEU5OEZGIDEwJSk7XHJcbiAgICBtYXJnaW4tdG9wOjclO1xyXG59XHJcbi5sb2dpbi1kZXNje1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlTWVkaXVtO1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiA5cHg7XHJcbn1cclxuLnByb2ZpbGVpbWd7XHJcbiAgICB0b3A6IDUwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_background_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/background-geolocation/ngx */ "./node_modules/@ionic-native/background-geolocation/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _providers_connect_connect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/connect/connect */ "./src/providers/connect/connect.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");















let LoginPage = class LoginPage {
    constructor(afAuth, languageService, backgroundGeolocation, router, alertCtrl, loadingCtrl, connect, http, platform, nativeHttp) {
        this.afAuth = afAuth;
        this.languageService = languageService;
        this.backgroundGeolocation = backgroundGeolocation;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.connect = connect;
        this.http = http;
        this.platform = platform;
        this.nativeHttp = nativeHttp;
        this.drv_master_json = [{ "driver_id": "16", "person_id": "11", "password": "11" }];
        this.shit = [];
        this.usersData = [];
        this.postList = [];
        this.driversInformationLoginForChecks = [];
        this.edited = false;
        this.locations = [];
        this.backgroundGeolocation.on(_ionic_native_background_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["BackgroundGeolocationEvents"].stationary).subscribe((location) => {
            var locationstr1 = localStorage.getItem("location");
            console.log(locationstr1);
        });
        const { Storage } = _capacitor_core__WEBPACK_IMPORTED_MODULE_13__["Plugins"];
    }
    getDataFormLoginPageFromServerNatively() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('%c Native Http', 'color:yellow;');
            try {
                const url = 'http://cf11.travelsoft.gr/itourapi/trp_driver_login.cfm?userid=dmta';
                const params = {};
                const headers = {};
                const response = yield this.nativeHttp.get(url, params, headers);
                console.log(response.status);
                console.log(JSON.parse(response.data));
                this.test = response.data;
                console.log(this.test.DRIVER); // JSON data returned by server
                console.log(response.headers);
            }
            catch (error) {
                console.error(error.status);
                console.error(error.error); // Error message as string
                console.error(error.headers);
            }
        });
    }
    getDataFromServerOldFashionHttp() {
        console.log('%c Old Fashion Http', 'color:yellow;');
        this.http.get('http://cf11.travelsoft.gr/itourapi/trp_driver_login.cfm?userid=dmta').subscribe((data) => {
            console.log(data);
        }, err => {
            console.log('Propably http request no longer exist', err);
        });
    }
    ionViewWillEnter() {
        this.platform.is('cordova') ? this.getDataFormLoginPageFromServerNatively() : this.getDataFromServerOldFashionHttp();
    }
    login() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('%c Old Fashion Http Request', 'color:orange;6');
            let loader = yield this.loadingCtrl.create({
                message: "Logging in"
            });
            yield loader.present();
            loader.present();
            setTimeout(() => {
                loader.dismiss();
            }, 800);
            localStorage.setItem('mobile', this.mobile);
            localStorage.setItem('pass', this.pass);
            const params = {};
            const headers = {};
            this.http.get('http://cf11.travelsoft.gr/itourapi/trp_driver_login.cfm?'
                + '&mobile=' + this.mobile + '&password=' + this.pass + '&userid=dmta')
                .subscribe((response) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.usersData = response;
                this.items = JSON.stringify(this.usersData);
                this.allData = JSON.parse(this.items);
                console.log('%c This is the data of Login', 'color:orange;');
                console.log(this.allData);
                console.log(this.allData.DRIVER);
                this.driversInformationsLogin = this.allData.DRIVER;
                //  this.driversInformationLoginForChecks = this.driversInformationsLogin.DRIVER;
                for (var i = 0; i < this.driversInformationsLogin.length; i++) {
                    if (this.driversInformationsLogin[i].FLAG == 1) {
                        let loader = yield this.loadingCtrl.create({
                            message: "Successfull Login"
                        });
                        console.log('before routelist');
                        this.router.navigate(['routelist/' + this.driversInformationsLogin[i].DRIVER_ID + '/' + this.driversInformationsLogin[i].PERSON_ID]);
                        loader.present();
                        setTimeout(() => {
                            loader.dismiss();
                        }, 2000);
                        console.log(this.driversInformationsLogin[i].PERSON_ID);
                    }
                    else {
                        let loader = yield this.loadingCtrl.create({
                            message: "Password or login is wrong..."
                        });
                        loader.present();
                        setTimeout(() => {
                            loader.dismiss();
                        }, 800);
                    }
                }
            }), err => {
                console.log('Propably http request no longer exist', err);
            });
        });
    }
    nativeLogin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('Native Http Request');
            let loader = yield this.loadingCtrl.create({
                message: "Logging in"
            });
            yield loader.present();
            setTimeout(() => {
                loader.dismiss();
            }, 800);
            localStorage.setItem('mobile', this.mobile);
            localStorage.setItem('pass', this.pass);
            const params = {};
            const headers = {};
            let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/trp_driver_login.cfm?'
                + '&mobile=' + this.mobile + '&password=' + this.pass + '&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_9__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => loader.dismiss())).subscribe((response) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.allData = JSON.parse(response.data);
                console.log('%c This is the data of Login', 'color:orange;');
                console.log(this.allData);
                console.log(this.allData.DRIVER);
                this.driversInformationsLogin = this.allData;
                this.driversInformationLoginForChecks = this.driversInformationsLogin.DRIVER;
                for (var i = 0; i < this.driversInformationLoginForChecks.length; i++) {
                    if (this.driversInformationLoginForChecks[i].FLAG == 1) {
                        loader.present();
                        setTimeout(() => {
                            loader.dismiss();
                        }, 2000);
                        console.log(this.driversInformationLoginForChecks[i].PERSON_ID);
                        this.router.navigate(['routelist/' + this.driversInformationLoginForChecks[i].DRIVER_ID + '/' + this.driversInformationLoginForChecks[i].PERSON_ID]);
                    }
                    else {
                        let loader = yield this.loadingCtrl.create({
                            message: "Password or login is wrong..."
                        });
                        loader.present();
                        setTimeout(() => {
                            loader.dismiss();
                        }, 800);
                    }
                }
            }), err => {
                console.log('Propably http request no longer exist', err);
            });
        });
    }
    getLoginFromEveryPlatform() {
        this.platform.is('cordova') ? this.nativeLogin() : this.login();
    }
    signOut() {
        this.afAuth.signOut().then(() => location.reload());
    }
    ;
    presentLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Hellooo',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
    presentLoadingWithOptions() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                spinner: null,
                duration: 5000,
                message: 'Please wait...',
                translucent: true,
                cssClass: 'custom-class custom-loading'
            });
            return yield loading.present();
        });
    }
    forgot() {
        this.router.navigate(['forgotpass']);
    }
    register() {
        this.router.navigate(['register']);
    }
    saveTodos() {
        //show box msg
        this.edited = true;
        //wait 3 Seconds and hide
        setTimeout(function () {
            this.edited = false;
            console.log(this.edited);
        }.bind(this), 3000);
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_11__["AngularFireAuth"] },
    { type: _services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] },
    { type: _ionic_native_background_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["BackgroundGeolocation"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _providers_connect_connect__WEBPACK_IMPORTED_MODULE_6__["ConnectProvider"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__["HTTP"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map