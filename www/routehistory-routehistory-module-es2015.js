(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routehistory-routehistory-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/routehistory/routehistory.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routehistory/routehistory.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home2/routelist\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}t</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\" slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon><ion-badge></ion-badge></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'routelist-myRoutes.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n\n\n\n    <ion-item class=\"from\">\n      <ion-datetime placeholder=\"{{'routelist-from.title' | translate}}\" displayFormat=\"DD/MM/YYYY\" (ionChange)=\"getChangeFunctionFromEveryHTTP()\" [(ngModel)]=\"startDate\" ></ion-datetime>\n    </ion-item>\n    <ion-item class=\"to\" >\n      <ion-datetime class=\"text-center\" placeholder=\"{{'routelist-to.title' | translate}}\" displayFormat=\"DD/MM/YYYY\"(ionChange)=\"getChangeFunctionFromEveryHTTP()\" [(ngModel)]=\"endDate\"></ion-datetime>\n    </ion-item>\n    <!-- <ion-label class=\"customRoutes\">CUSTOM ROUTESS EDW{{custArrayPicks}}</ion-label>\n    <ion-list>\n      <ion-item *ngFor=\"let item of bcustArrayPicks\" (click)=\"sendDataToRouteStarted(select)\">\n        <ion-buttons item-start>\n        <ion-icon class=\"icon-size\" name=\"map\" ></ion-icon>\n      </ion-buttons>\n      <ion-buttons item-end>\n        <button  class=\"btn-accept\" ion-button color=\"light\">TAKE</button>\n      </ion-buttons>\n      <ion-label class=\"availability\" >{{CustStartingPointElementFromArrayOfPickups}}</ion-label> <br>\n      <ion-label class=\"destination\">{{CustTheLastPointElementFromArrayOfPickups}}</ion-label> <br>\n      <ion-label class=\"date\">{{item}}1</ion-label> \n    \n      </ion-item>\n     \n     \n    </ion-list> -->\n\n   \n    <ion-list >\n      <ion-label class=\"customRoutes\">Route History</ion-label>\n      <ion-item *ngFor=\"let select of rptDriverRoutesJSONparseToArray.DRVROUTES; \" (click)=\"sendDataToRouteStarted(select)\">\n        <ion-buttons item-start>\n        <ion-icon class=\"icon-size\" name=\"map\" ></ion-icon>\n      </ion-buttons>\n      <ion-buttons item-end>\n        <button  class=\"btn-accept\" ion-button color=\"light\">TAKE</button>\n      </ion-buttons>\n      \n      <ion-label class=\"availability\" *ngIf=\"select.TYPE == 'FIX' \">FIXED ROUTE</ion-label> <br>\n      <ion-label class=\"availability\" *ngIf=\"select.TYPE == 'CUST' \">CUSTOM ROUTE</ion-label> <br>\n      <ion-label class=\"destination\" *ngIf=\"select.TYPE == 'FIX' \" >{{FixedStartingPointElementFromArrayOfPickups}}</ion-label> <br>\n      <ion-label class=\"destination\" *ngIf=\"select.TYPE == 'CUST' \" >{{CustStartingPointElementFromArrayOfPickups}}</ion-label> <br>\n     <ion-label class=\"date\" *ngIf=\"select.TYPE == 'FIX' \">{{FixedTheLastPointElementFromArrayOfPickups}}</ion-label> \n     <ion-label class=\"date\" *ngIf=\"select.TYPE == 'CUST' \">{{CustTheLastPointElementFromArrayOfPickups}}</ion-label> \n\n\n      </ion-item>\n     \n     \n    </ion-list>\n  </ion-content>\n\n  <div class=\"myiontab\">\n    <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n\n      <ion-tab-button tab=\"routelist\" (click)=\"navigateToRouteListPage()\">\n        <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"takePhotoWithOldFashionHttpRequest()\">\n        <ion-icon class=\"iconStyling\" name=\"camera\"></ion-icon>\n      </ion-tab-button>\n  \n   \n      <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n      </ion-tab-button>\n    </ion-tab-bar>\n  \n  </div>\n");

/***/ }),

/***/ "./src/app/routehistory/routehistory-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/routehistory/routehistory-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: RoutehistoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutehistoryPageRoutingModule", function() { return RoutehistoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _routehistory_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routehistory.page */ "./src/app/routehistory/routehistory.page.ts");




const routes = [
    {
        path: '',
        component: _routehistory_page__WEBPACK_IMPORTED_MODULE_3__["RoutehistoryPage"]
    }
];
let RoutehistoryPageRoutingModule = class RoutehistoryPageRoutingModule {
};
RoutehistoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RoutehistoryPageRoutingModule);



/***/ }),

/***/ "./src/app/routehistory/routehistory.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/routehistory/routehistory.module.ts ***!
  \*****************************************************/
/*! exports provided: RoutehistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutehistoryPageModule", function() { return RoutehistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _routehistory_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routehistory-routing.module */ "./src/app/routehistory/routehistory-routing.module.ts");
/* harmony import */ var _routehistory_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./routehistory.page */ "./src/app/routehistory/routehistory.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let RoutehistoryPageModule = class RoutehistoryPageModule {
};
RoutehistoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _routehistory_routing_module__WEBPACK_IMPORTED_MODULE_5__["RoutehistoryPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        ],
        declarations: [_routehistory_page__WEBPACK_IMPORTED_MODULE_6__["RoutehistoryPage"]]
    })
], RoutehistoryPageModule);



/***/ }),

/***/ "./src/app/routehistory/routehistory.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/routehistory/routehistory.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".my-custom-class .alert-wrapper {\n  background: #e5e5e5;\n}\n\n.header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\n.ios ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n  position: fixed;\n}\n\n.ios ion-fab ion-fab-button {\n  --box-shadow: none;\n}\n\n.md ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n  position: fixed;\n}\n\n.md ion-fab ion-fab-button {\n  --box-shadow: none;\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.customRoutes {\n  font-size: 25px;\n  display: flex;\n  width: 100%;\n  justify-content: center;\n  margin-top: 10%;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-theblue);\n}\n\n.myiontab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\n\n.myiontab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 70px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 51px;\n  height: 51px;\n  position: absolute;\n  left: 181px;\n  top: -32px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.myiontab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.myiontab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.myiontab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.myiontab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.myiontab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.from {\n  width: 120px;\n  height: 35px;\n  --background: var(--ion-color-ibus-darkblue);\n  border-radius: 5px;\n  text-align: center;\n  color: white;\n  position: relative;\n  top: 35px;\n  left: 52px;\n}\n\n.to {\n  width: 120px;\n  height: 35px;\n  --background: var(--ion-color-ibus-darkblue);\n  border-radius: 5px;\n  text-align: center;\n  color: white;\n  position: relative;\n  top: 0px;\n  left: 209px;\n}\n\n::-moz-placeholder {\n  color: white;\n  opacity: 1;\n}\n\n::placeholder {\n  color: white;\n  opacity: 1;\n}\n\nion-datetime {\n  color: white !important;\n  font-family: monteBold;\n  font-size: 13px;\n  padding: 0px 0px 11px 7px;\n}\n\n.centerlabel {\n  display: flex;\n  justify-content: center;\n  width: auto;\n}\n\n.icon-size {\n  width: 60px;\n  height: 60px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.availability {\n  position: relative;\n  top: -19px;\n  left: 10px;\n  font-size: 10px;\n  opacity: 43%;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\nion-item {\n  width: 550px;\n}\n\n.destination {\n  position: relative;\n  right: 144px;\n  font-size: 14px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\nion-list {\n  margin-top: 6%;\n}\n\n.date {\n  position: relative;\n  right: 296px;\n  font-size: 12px;\n  opacity: 43%;\n  font-family: monteRegular;\n  margin-top: 49px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.currentDate {\n  position: relative;\n  top: 21px;\n  font-family: monteRegular;\n  font-size: 13px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.searchRoute {\n  position: relative;\n  top: 13px;\n  font-size: 13px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.btn-accept {\n  width: 80px;\n  height: 36px;\n  position: absolute;\n  background-color: var(--ion-color-ibus-theblue);\n  color: white;\n  font-size: 13px;\n  text-align: center;\n  border-radius: 5px;\n  left: 215px;\n}\n\n@media only screen and (max-width: 375px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 100px;\n  }\n\n  ion-list {\n    position: relative;\n    left: -8px;\n  }\n\n  .btn-accept {\n    width: 80px;\n    height: 36px;\n    position: absolute;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-size: 13px;\n    text-align: center;\n    border-radius: 5px;\n    left: 185px;\n  }\n\n  ion-title {\n    font-size: 15px;\n    position: relative;\n    top: -74px;\n    left: 10px;\n    color: white;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  #profile {\n    width: 60px;\n    height: 60px;\n    background-color: white;\n    border-radius: 50%;\n    position: relative;\n    left: 27px;\n    margin-bottom: 31px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -30px;\n    color: white;\n    font-size: 21px;\n  }\n\n  .nav-buttons {\n    margin-bottom: 28px;\n  }\n\n  .from {\n    width: 120px;\n    height: 35px;\n    --background: var(--ion-color-ibus-darkblue);\n    border-radius: 5px;\n    text-align: center;\n    color: white;\n    position: relative;\n    top: 35px;\n    left: 33px;\n  }\n\n  .to {\n    width: 120px;\n    height: 35px;\n    --background: var(--ion-color-ibus-darkblue);\n    border-radius: 5px;\n    text-align: center;\n    color: white;\n    position: relative;\n    top: 0px;\n    left: 189px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .from {\n    width: 120px;\n    height: 35px;\n    --background: var(--ion-color-ibus-darkblue);\n    border-radius: 5px;\n    text-align: center;\n    color: white;\n    position: relative;\n    top: 35px;\n    left: 13px;\n  }\n\n  .to {\n    width: 120px;\n    height: 35px;\n    --background: var(--ion-color-ibus-darkblue);\n    border-radius: 5px;\n    text-align: center;\n    color: white;\n    position: relative;\n    top: 0px;\n    left: 149px;\n  }\n\n  .btn-accept {\n    width: 60px;\n    height: 36px;\n    position: absolute;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-size: 13px;\n    text-align: center;\n    border-radius: 5px;\n    left: 149px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm91dGVoaXN0b3J5L3JvdXRlaGlzdG9yeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxtQkFBQTtBQUFKOztBQUdBO0VBQ0ksNENBQUE7QUFBSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBRUo7O0FBQUE7RUFDSSwwQ0FBQTtFQUE0QyxpQkFBQTtFQUl4QyxlQUFBO0FBQ1I7O0FBSkU7RUFDQyxrQkFBQTtBQU1IOztBQUZBO0VBQ0ksMENBQUE7RUFBNEMsaUJBQUE7RUFJeEMsZUFBQTtBQUdSOztBQU5FO0VBQ0Msa0JBQUE7QUFRSDs7QUFKQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQU9KOztBQUxBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQVFKOztBQU5BO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0VBQ0EscUNBQUE7QUFTSjs7QUFQQTtFQUNJLHFDQUFBO0FBVUo7O0FBUkE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQVdKOztBQVRBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBWUo7O0FBVEE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFZSjs7QUFWQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtBQWFKOztBQVhBO0VBQ0ksbUJBQUE7QUFjSjs7QUFaQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQWVKOztBQWJBO0VBQ0ksb0JBQUE7QUFnQko7O0FBZEE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLG9DQUFBO0FBaUJKOztBQWRDO0VBQ0MsMENBQUE7RUFBNEMsaUJBQUE7QUFrQjlDOztBQWpCRTtFQUNDLGtCQUFBO0VBQ0EsVUFBQTtBQW1CSDs7QUFoQkM7RUF1QkMsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUFRLFFBQUE7RUFDRixXQUFBO0VBQ0EsWUFBQTtBQUhSOztBQXpCUTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsMENBQUE7RUFBNEMsaUJBQUE7QUE0QnhEOztBQTNCWTtFQUNJLGtCQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQTZCaEI7O0FBM0JZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQTZCaEI7O0FBbkJFO0VBQ0MsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBcUJIOztBQW5CRTtFQUNDLG9DQUFBO0FBcUJIOztBQW5CRTtFQUNDLGlCQUFBO0VBQ0EsNkJBQUE7QUFxQkg7O0FBbkJFO0VBQ0MsZ0JBQUE7RUFDQSw0QkFBQTtBQXFCSDs7QUFuQkU7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7QUFxQkg7O0FBcEJHO0VBQ0MsNEJBQUE7QUFzQko7O0FBVkE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDRDQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBYUo7O0FBWEE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDRDQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FBY0o7O0FBWEE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtBQWNKOztBQWhCQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FBY0o7O0FBWkE7RUFDSSx1QkFBQTtFQUlBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FBWUo7O0FBVEE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FBWUo7O0FBVkE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNDLHFDQUFBO0FBYUw7O0FBWEE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFDQUFBO0FBY0o7O0FBWkE7RUFDSSxZQUFBO0FBZUo7O0FBYkE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQ0FBQTtBQWdCSjs7QUFEQTtFQUNJLGNBQUE7QUFJSjs7QUFGQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFDQUFBO0FBS0o7O0FBSEE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtBQU1KOztBQUpBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFPSjs7QUFMQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSwrQ0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFRSjs7QUFKQTtFQUNHO0lBQ0MscUJBQUE7SUFDQSxhQUFBO0VBT0Y7O0VBTEM7SUFDQyxrQkFBQTtJQUNBLFVBQUE7RUFRRjs7RUFOQztJQUNDLFdBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSwrQ0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtJQUNBLFdBQUE7RUFTRjs7RUFQQztJQUNDLGVBQUE7SUFDQSxrQkFBQTtJQUNBLFVBQUE7SUFDQSxVQUFBO0lBQ0EsWUFBQTtFQVVGO0FBQ0Y7O0FBUEU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsYUFBQTtFQVNOO0FBQ0Y7O0FBUEU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQVNOO0FBQ0Y7O0FBTkU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQVFOOztFQU5FO0lBQ0ksV0FBQTtJQUNBLFlBQUE7SUFDQSx1QkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7RUFTTjs7RUFQRTtJQUNJLGtCQUFBO0lBQ0EsVUFBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtFQVVOOztFQVJFO0lBQ0ksbUJBQUE7RUFXTjs7RUFURTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsNENBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFVBQUE7RUFZTjs7RUFWSztJQUNDLFlBQUE7SUFDQSxZQUFBO0lBQ0EsNENBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0lBQ0EsUUFBQTtJQUNBLFdBQUE7RUFhTjtBQUNGOztBQVhFO0VBQ0M7SUFDQyxZQUFBO0lBQ0EsWUFBQTtJQUNBLDRDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtJQUNBLFNBQUE7SUFDQSxVQUFBO0VBYUY7O0VBWEM7SUFDQyxZQUFBO0lBQ0EsWUFBQTtJQUNBLDRDQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtJQUNBLFFBQUE7SUFDQSxXQUFBO0VBY0Y7O0VBWkM7SUFDQyxXQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0lBQ0EsK0NBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxXQUFBO0VBZUY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JvdXRlaGlzdG9yeS9yb3V0ZWhpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8yMDIxXHJcbi5teS1jdXN0b20tY2xhc3MgLmFsZXJ0LXdyYXBwZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2U1ZTVlNTtcclxuICB9XHJcbi8vMjAyMFxyXG4uaGVhZGVye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTtcclxufVxyXG4uaW9zIGlvbi10b29sYmFye1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgaGVpZ2h0OiAxMjdweDtcclxufVxyXG4ubWQgaW9uLXRvb2xiYXJ7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICBoZWlnaHQ6IDg3cHg7XHJcbn1cclxuLmlvcyBpb24tZmFie1xyXG4gICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwb3NpdGlvbjpmaXhlZDtcclxufVxyXG4ubWQgaW9uLWZhYntcclxuICAgIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTsgLyogZml4IG5vdGNoIGlvcyovXHJcblx0XHRpb24tZmFiLWJ1dHRvbiB7XHJcblx0XHRcdC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcG9zaXRpb246Zml4ZWQ7XHJcbn1cclxuLm5vdEJ0bntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiA0N3B4O1xyXG4gICAgaGVpZ2h0OiA2MnB4O1xyXG59XHJcbi5tZW51QnRue1xyXG4gICAgZm9udC1zaXplOiAxMDBweDtcclxuICAgIHdpZHRoOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5tZW51VHh0e1xyXG4gICAgZm9udC1zaXplOjE4cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZVJlZ3VsYXI7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLmljb25TdHlsaW5ne1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5tZW51VG9vbGJhcntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IDgwcHg7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbn1cclxuLmlvcyAjcHJvZmlsZXtcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAyN3B4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL3Byb2ZpbGUuanBnKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogNjBweCA4MHB4O1xyXG4gICAgXHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5tZCAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxufVxyXG4uY3VzdG9tUm91dGVze1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxufVxyXG4ubXlpb250YWJ7XHJcblx0aW9uLWZhYiB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG5cdFx0aW9uLWZhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmU7XHJcblx0XHRcdHotaW5kZXg6IDE7XHJcblx0XHR9XHJcblx0fVxyXG5cdGlvbi10YWItYmFyIHtcclxuICAgICAgICAuYWRkQnRuIHtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuICAgICAgICAgICAgYnV0dG9uIHtcclxuICAgICAgICAgICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgbGVmdDogMTgxcHg7XHJcbiAgICAgICAgICAgICAgICB0b3A6IC0zMnB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDozMHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OjMwcHg7XHJcbiAgICAgICAgICAgICAgICBzdHJva2U6IDVweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHRcdC0tYm9yZGVyOiAwO1xyXG5cdFx0LS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdGJvdHRvbTogMDtcclxuXHRcdGxlZnQ6MDsgcmlnaHQ6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OjcwcHg7XHJcblx0XHQmOmFmdGVye1xyXG5cdFx0XHRjb250ZW50OiBcIiBcIjtcclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdGJvdHRvbTogMDtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdFx0aGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uIHtcclxuXHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24uY29tbWVudHMge1xyXG5cdFx0XHRtYXJnaW4tcmlnaHQ6IDBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5ub3RpZnMge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xyXG5cdFx0fVxyXG5cdFx0c3ZnIHsgICAgXHJcblx0XHRcdHdpZHRoOiA3MnB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxOXB4O1xyXG5cdFx0XHRwYXRoe1xyXG5cdFx0XHRcdGZpbGw6ICB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHR9XHRcdFxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuICAgIFxyXG4gICAgLy8gaW9uLWJhZGdle1xyXG4gICAgLy8gICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAvLyB9XHJcblxyXG5cclxuLmZyb217XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAzNXB4O1xyXG4gICAgbGVmdDogNTJweDtcclxufVxyXG4udG97XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAwcHg7XHJcbiAgICBsZWZ0OiAyMDlweDtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgb3BhY2l0eToxO1xyXG59XHJcbmlvbi1kYXRldGltZSB7XHJcbiAgICBjb2xvcjp3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgLy8gcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLy8gdG9wOiAtNnB4O1xyXG4gICAgLy8gbGVmdDogM3B4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMHB4IDExcHggN3B4O1xyXG59XHJcblxyXG4uY2VudGVybGFiZWx7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICB9XHJcbi5pY29uLXNpemV7XHJcbiAgICB3aWR0aDo2MHB4O1xyXG4gICAgaGVpZ2h0OjYwcHg7XHJcbiAgICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5hdmFpbGFiaWxpdHl7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC0xOXB4O1xyXG4gICAgbGVmdDogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIG9wYWNpdHk6IDQzJTtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlUmVndWxhcjtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG5pb24taXRlbXtcclxuICAgIHdpZHRoOiA1NTBweDtcclxufVxyXG4uZGVzdGluYXRpb257XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICByaWdodDogMTQ0cHg7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIC8vIHJpZ2h0OiAyNzBweDtcclxuICAgIC8vIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIC8vIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgLy8gY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIC8vIHRvcDogNTBweDtcclxuXHJcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAvLyByaWdodDogMzIwcHg7XHJcbiAgICAvLyBmb250LXNpemU6IDE0cHg7XHJcbiAgICAvLyBmb250LWZhbWlseTogbW9udGVTQm9sZDtcclxuICAgIC8vIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICAvLyB0b3A6IDE2cHg7XHJcbn1cclxuaW9uLWxpc3R7XHJcbiAgICBtYXJnaW4tdG9wOiA2JTtcclxufVxyXG4uZGF0ZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHJpZ2h0OiAyOTZweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG9wYWNpdHk6IDQzJTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVJlZ3VsYXI7XHJcbiAgICBtYXJnaW4tdG9wOiA0OXB4O1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5jdXJyZW50RGF0ZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMjFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlUmVndWxhcjtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5zZWFyY2hSb3V0ZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMTNweDtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5idG4tYWNjZXB0e1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbGVmdDogMjE1cHg7XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDM3NXB4KSB7XHJcbiAgIGlvbi10b29sYmFye1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgfVxyXG4gICBpb24tbGlzdHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IC04cHg7XHJcbiAgIH1cclxuICAgLmJ0bi1hY2NlcHR7XHJcbiAgICB3aWR0aDogODBweDtcclxuICAgIGhlaWdodDogMzZweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbGVmdDogMTg1cHg7XHJcbiAgIH1cclxuICAgaW9uLXRpdGxle1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtNzRweDtcclxuICAgIGxlZnQ6IDEwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgIH1cclxuXHJcbiAgfVxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuICAgIGlvbi10b29sYmFye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDEyN3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgICBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLmlvcyBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgfVxyXG4gICAgI3Byb2ZpbGV7XHJcbiAgICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbGVmdDogMjdweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgfVxyXG4gICAgLmlvcyBpb24tdGl0bGV7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogLTE1cHg7XHJcbiAgICAgICAgbGVmdDogLTMwcHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIH1cclxuICAgIC5uYXYtYnV0dG9uc3tcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyOHB4O1xyXG4gICAgfVxyXG4gICAgLmZyb217XHJcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAzNXB4O1xyXG4gICAgICAgIGxlZnQ6IDMzcHg7XHJcbiAgICAgICB9XHJcbiAgICAgICAudG97XHJcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgbGVmdDogMTg5cHg7XHJcbiAgICAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgLmZyb217XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMzVweDtcclxuICAgIGxlZnQ6IDEzcHg7XHJcbiAgIH1cclxuICAgLnRve1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDBweDtcclxuICAgIGxlZnQ6IDE0OXB4O1xyXG4gICB9XHJcbiAgIC5idG4tYWNjZXB0e1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGxlZnQ6IDE0OXB4O1xyXG4gICB9XHJcblxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/routehistory/routehistory.page.ts":
/*!***************************************************!*\
  !*** ./src/app/routehistory/routehistory.page.ts ***!
  \***************************************************/
/*! exports provided: RoutehistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutehistoryPage", function() { return RoutehistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_language_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");










const { Camera } = _capacitor_core__WEBPACK_IMPORTED_MODULE_9__["Plugins"];
let RoutehistoryPage = class RoutehistoryPage {
    constructor(nativeHttp, platform, alertController, activatedRoute, loadingCtrl, http, router, languageService) {
        //greg
        this.nativeHttp = nativeHttp;
        this.platform = platform;
        this.alertController = alertController;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.router = router;
        this.languageService = languageService;
        this.serviceRegistration = "";
        this.dataFromService = "";
        //greg 
        this.myDate = new Date().toISOString();
        this.invalidSelection = false;
        this.routes = [];
        this.filtered = [...this.routes];
        this.chrbusCust = [];
        this.chrBusCustRoutes = [];
        this.chrBusCustRoutesJSONparseToArray = [];
        this.chrBusCustRoutesJSONparseToArrayCUSTPICKUP = [];
        this.rptDriverRoutes = [];
        this.rptDriverRoutesJSONparseToArray = [];
        this.customPickUps = [];
        this.theRealPickUp = [];
        this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES = [];
        this.newCustomPickupRoutes = [];
        this.newCustomPickupRoutesJSONtoArray = [];
        this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = [];
        this.nativePickup = [];
        this.data = [];
        this.nativePickupArrayArray = [];
        this.dataFromLoginPage = this.activatedRoute.snapshot.paramMap.get('id');
        this.dataFromLoginPageJSON = JSON.parse(this.dataFromLoginPage);
        console.log('%c Print Data from Login', 'color:orange;');
        console.log(this.dataFromLoginPageJSON);
        this.dataFromLoginPageProfile = this.activatedRoute.snapshot.paramMap.get('theprofile');
        this.dataFromLoginPageProfileJSON = JSON.parse(this.dataFromLoginPageProfile);
        console.log(this.dataFromLoginPageProfileJSON);
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        console.log('RERE');
        this.platform.is('cordova') ? this.getCustomPickupDataWithNativeHTTP() : this.getCustomPickupDataWithHTTP();
    }
    getCustomPickupDataWithHTTP() {
        console.log('Old Fashion HTTP Custom PIckup');
        // this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=2&userid=dmta')
        //   .subscribe((data) => {
        //     console.log(data);
        //     this.customPickUps = data;
        //     this.theRealPickUp = this.myPickUp.CUSTPICKUPS;
        //     console.log('%c JSON PARSE', 'color:red;');
        //     console.log(this.myPickUp);
        //     console.log('%c TheReal array Pickups', 'color:orange;');
        //     console.log(this.theRealPickUp);
        //     for (var i = 0; i < this.theRealPickUp.length; i++) {
        //       console.log(this.theRealPickUp[0].PICKUP_ADDRESS);
        //       console.log(this.theRealPickUp[i].PICKUP_ADDRESS);
        //       this.startingPointElementFromArrayOfPickups = this.theRealPickUp[0].PICKUP_ADDRESS
        //       this.theLastPointElementFromArrayOfPickups = this.theRealPickUp[i].PICKUP_ADDRESS;
        //     }
        //   })
    }
    getCustomPickupDataWithNativeHTTP() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let url = '';
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=16&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log('ola kala')))
                .subscribe(data => {
                let parsed = JSON.parse(data.data).CUSTPICKUPS;
                this.data = parsed;
                this.nativePickupArray = this.data;
                console.log(this.nativePickupArray);
                this.nativePickupArrayArray = this.nativePickupArray.PICKUP_ADDRESS;
                console.log(this.nativePickupArrayArray);
                for (var i = 0; i < this.nativePickupArray.length; i++) {
                    console.log(this.nativePickupArray[0].PICKUP_ADDRESS);
                    console.log(this.nativePickupArray[i].PICKUP_ADDRESS);
                    this.startingPointElementFromArrayOfPickups = this.nativePickupArray[0].PICKUP_ADDRESS;
                    this.theLastPointElementFromArrayOfPickups = this.nativePickupArray[i].PICKUP_ADDRESS;
                }
                console.log(this.startingPointElementFromArrayOfPickups);
                console.log(this.theLastPointElementFromArrayOfPickups);
            }, err => {
                console.log('Native error', err);
            });
        });
    }
    getChangeFunctionFromEveryHTTP() {
        this.platform.is('cordova') ? this.onChangeFromFunctionWitheNativeHTTP() : this.onChangeFromFunctionHTTP();
    }
    onChangeFromFunctionHTTP() {
        console.log('Old Fashion HTTP');
        const theFirstDate = this.startDate.split('T')[0];
        console.log("THE FIRST DATE", theFirstDate);
        const theLastDate = this.endDate.split('T')[0];
        console.log("THE LAST DATE", theLastDate);
        console.log("You selected FROM:");
        this.http.get('http://cf11.travelsoft.gr/itourapi/rpt_drv_routes.cfm?'
            + 'driver_id=' + this.dataFromLoginPageJSON + '&from_date=' + theFirstDate + '&to_date=' + theLastDate + '&userid=dmta').subscribe((data) => {
            console.log(data);
            this.rptDriverRoutes = data;
            this.rptDriverRoutesJSONparseToArray = this.rptDriverRoutes;
            console.log('%c RPT DRIVER ROUTES', 'color:pink;');
            console.log(this.rptDriverRoutesJSONparseToArray);
            this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES = this.rptDriverRoutesJSONparseToArray.DRVROUTES;
            console.log(this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES);
            for (var j = 0; j < this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES.length; j++) {
                if (this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].TYPE === "FIX") {
                    this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_route_pup.cfm?' + 'chrbus_code=' + this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].SERVICECODE + '&userid=dmta')
                        .subscribe((data) => {
                        this.customPickUps = data;
                        this.theRealPickUp = this.customPickUps.FIXEDPICKUPS;
                        for (var i = 0; i < this.theRealPickUp.length; i++) {
                            console.log(this.theRealPickUp[i].CTY_NAME);
                            this.FixedStartingPointElementFromArrayOfPickups = this.theRealPickUp[0].CTY_NAME;
                            this.FixedTheLastPointElementFromArrayOfPickups = this.theRealPickUp[i].CTY_NAME;
                        }
                    });
                }
                else {
                    this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?' + 'route_id=' + this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].SERVICECODE + '&userid=dmta')
                        .subscribe((data) => {
                        console.log(data);
                        this.customPickUps = data;
                        this.theRealPickUp = this.customPickUps.CUSTPICKUPS;
                        for (var i = 0; i < this.theRealPickUp.length; i++) {
                            console.log(this.theRealPickUp[i].PICKUP_ADDRESS);
                            this.CustStartingPointElementFromArrayOfPickups = this.theRealPickUp[0].PICKUP_ADDRESS;
                            this.CustTheLastPointElementFromArrayOfPickups = this.theRealPickUp[i].PICKUP_ADDRESS;
                        }
                    });
                }
            }
            var i = 0;
        });
    }
    onChangeFromFunctionWitheNativeHTTP() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log('Native HTTP');
            const theFirstDate = this.startDate.split('T')[0];
            console.log("THE FIRST DATE", theFirstDate);
            const theLastDate = this.endDate.split('T')[0];
            console.log("THE LAST DATE", theLastDate);
            console.log("You selected FROM:");
            let loader = yield this.loadingCtrl.create({
                message: "searching"
            });
            yield loader.present();
            loader.present();
            setTimeout(() => {
                loader.dismiss();
            }, 800);
            let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/rpt_drv_routes.cfm?'
                + 'driver_id=' + this.dataFromLoginPageJSON
                + '&from_date=' + theFirstDate +
                '&to_date=' + theLastDate +
                '&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => loader.dismiss()))
                .subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.rptDriverRoutesJSONparse = JSON.parse(data.data);
                this.rptDriverRoutesJSONparseToArray = this.rptDriverRoutesJSONparse;
                console.log('%c RPT DRIVER ROUTES', 'color:pink;');
                console.log(this.rptDriverRoutesJSONparseToArray);
                console.log(this.rptDriverRoutesJSONparseToArray.DRVROUTES.DRIVER_ID);
                this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES = this.rptDriverRoutesJSONparseToArray.DRVROUTES;
                console.log(this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES.SERVICECODE);
                var i = 0;
                for (var j = 0; j < this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES.length; j++) {
                    if (this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].TYPE === "FIX") {
                        let fixedRoutesNativelyCalled = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_route_pup.cfm?' + 'chrbus_code=' + this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].SERVICECODE + '&userid=dmta', {}, {
                            'Content-Type': 'application/json'
                        });
                        Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(fixedRoutesNativelyCalled).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => loader.dismiss()))
                            .subscribe((data) => {
                            this.customPickUps = JSON.parse(data.data);
                            this.theRealPickUp = this.customPickUps.FIXEDPICKUPS;
                            for (var i = 0; i < this.theRealPickUp.length; i++) {
                                this.FixedStartingPointElementFromArrayOfPickups = this.theRealPickUp[0].CTY_NAME;
                                this.FixedTheLastPointElementFromArrayOfPickups = this.theRealPickUp[i].CTY_NAME;
                            }
                        });
                    }
                    else {
                        let customRoutesNativelyCalled = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?' + 'route_id=' + this.chrBusCustRoutesJSONparseToArrayRPTDRIVERROUTES[j].SERVICECODE + '&userid=dmta', {}, {
                            'Content-Type': 'application/json'
                        });
                        Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(customRoutesNativelyCalled).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => loader.dismiss()))
                            .subscribe((data) => {
                            console.log(data);
                            this.customPickUps = JSON.parse(data.data);
                            this.theRealPickUp = this.customPickUps.CUSTPICKUPS;
                            for (var i = 0; i < this.theRealPickUp.length; i++) {
                                this.CustStartingPointElementFromArrayOfPickups = this.theRealPickUp[0].PICKUP_ADDRESS;
                                this.CustTheLastPointElementFromArrayOfPickups = this.theRealPickUp[i].PICKUP_ADDRESS;
                            }
                        });
                    }
                }
                // while (i < this.rptDriverRoutesJSONparseToArray.DRVROUTES.length) { // Looking for Chapter Bus Service 
                //   if (this.rptDriverRoutesJSONparseToArray.DRVROUTES[i].SERVICE == 'CHT') {
                //     let nativeCalling = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?'
                //       + 'route_id=' + this.rptDriverRoutesJSONparseToArray.DRVROUTES[i].SERVICECODE
                //       + '&userid=dmta', {}, {
                //     });
                //     from(nativeCalling).pipe(
                //       finalize(() => loader.dismiss())
                //     )
                //       .subscribe((data) => {
                //         this.newCustomPickupRoutesJSON = JSON.parse(data.data);
                //         this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutesJSON;
                //         this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = this.newCustomPickupRoutesJSONtoArray.CUSTPICKUPS
                //       }, err => {
                //         console.log('Error on Cust Routes');
                //       })
                //     i++;
                //   }
                //   console.log('finish');
                // }
            }));
        });
    }
    sendDataToRouteStarted(selectedDatesFromCustomPickups) {
        this.saveMyData(selectedDatesFromCustomPickups).subscribe((dataReturnFromService) => {
            this.dataFromService = JSON.stringify(dataReturnFromService);
            console.log(this.dataFromService['_body']);
            console.log(selectedDatesFromCustomPickups);
            console.log(JSON.stringify(selectedDatesFromCustomPickups));
            this.router.navigate(['techinspect/' + JSON.stringify(selectedDatesFromCustomPickups) + '/' + JSON.stringify(this.dataFromLoginPageJSON)]);
        });
    }
    saveMyData(dataToSend) {
        var url = "https://reqres.in/api/users";
        return this.http.post(url, dataToSend, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ "Content-type": "Application/json" })
        });
    }
    takePicture4() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const image = yield Camera.getPhoto({
                quality: 90,
                allowEditing: true,
                resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_9__["CameraResultType"].Base64
            });
            this.img = image.base64String;
            console.log(image);
        });
    }
    takePhotoWithOldFashionHttpRequest() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const image = yield Camera.getPhoto({
                quality: 90,
                allowEditing: true,
                resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_9__["CameraResultType"].Base64
            });
            this.img = image.base64String;
            console.log(image);
            const formData2 = new FormData();
            formData2.append("photo", this.img);
            console.log(formData2);
            var date = new Date().getHours();
            var date2 = new Date().getMinutes();
            var kati = date + "_" + date2;
            this.http.post('http://cf11.travelsoft.gr/itourapi/chrbus_drv_img.cfm?driver_id=16&srv_type=CHT&srv_code=2&sp_id=1&sp_code=6&fromd=2020/11/27&tod=2020/11/27&vehicle_map_id=1025&vhc_id=1&vhc_plates=VFR111&version_id=1&VechicleTypeID=1&virtualversion_id=1&img_type=TOLL&latitude=37.865044&longitude=23.755045&pickup_address=kapou&first_name=christos24&last_name=christos24&time=' + kati + '&userid=dmta', formData2)
                .subscribe(data => {
                console.log(data);
            });
        });
    }
    //
    takePhotoWithNativeHttpRequest() {
        this.nativeHttp.setDataSerializer('urlencoded');
        var formData2 = {
            photo: this.img
        };
        let headers = {
            "Accept": "application/json",
            "api-auth": 'apiAuthToken String',
            "User-Auth": 'userAuthToken String'
        };
        this.nativeHttp.setDataSerializer('urlencoded');
        this.nativeHttp.setHeader('*', 'Content-Type', 'application/x-www-form-urlencoded');
        var date = new Date().getHours();
        var date2 = new Date().getMinutes();
        var kati = date + "_" + date2;
        this.nativeHttp.post('http://cf11.travelsoft.gr/itourapi/chrbus_drv_img.cfm?driver_id=16&srv_type=CHT&srv_code=2&sp_id=1&sp_code=6&fromd=2020/11/27&tod=2020/11/27&vehicle_map_id=1025&vhc_id=1&vhc_plates=VFR111&version_id=1&VechicleTypeID=1&virtualversion_id=1&img_type=TOLL&latitude=37.865044&longitude=23.755045&pickup_address=kapou&first_name=christos24&last_name=christos24&time=' + kati + '&userid=dmta', formData2, headers)
            .then(data => {
            console.log(data);
        })
            .catch(error => { console.log(error); });
    }
    navigateToSettingsPage() {
        this.router.navigate(["settings"]);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(["routehistory"]);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(["techhistory"]);
    }
    navigateToWalletPage() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Select Route!',
                message: '<strong>Select Route before trying to enter into Wallet</strong>!!!',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    navigateToProfilePage() {
        console.log('%c going to profile');
        console.log(this.dataFromLoginPageJSON.PERSON_ID);
        this.router.navigate(['profile/' + JSON.stringify(this.dataFromLoginPageProfileJSON)]);
    }
    navigateToNotificationsPage() {
        this.router.navigate(['notifications']);
    }
    navigateToRouteListPage() {
        this.router.navigate(['routelist']);
    }
    navigateToCreateRoutePage() {
        this.router.navigate(['createroute']);
    }
};
RoutehistoryPage.ctorParameters = () => [
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_language_service__WEBPACK_IMPORTED_MODULE_8__["LanguageService"] }
];
RoutehistoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-routehistory',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./routehistory.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/routehistory/routehistory.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./routehistory.page.scss */ "./src/app/routehistory/routehistory.page.scss")).default]
    })
], RoutehistoryPage);



/***/ })

}]);
//# sourceMappingURL=routehistory-routehistory-module-es2015.js.map