(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item (click)=\"goingBackToRouteList()\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"weAreAlreadyInWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon><ion-badge></ion-badge></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'routelist-myWallet.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <div class=\"centerItems titleStyle\">{{'myWallet-balance.title' | translate}}</div>\n    <ion-item lines=\"none\">\n      <ion-img class=\"centerItems\" src=\"assets/images/wallet_board.svg\">\n   \n      </ion-img>\n\n     \n       <p  class=\"money\">25</p>  \n \n\n    </ion-item>\n    \n\n  <form [formGroup]=\"paymentForm\" (ngSubmit)=\"submit()\">\n    <div class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'myWallet-transactionType.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select formControlName=\"typeOfPayment\"  [(ngModel)]=\"selectedValue\" (ionChange)=\"confirmPaymentWithEveryHttp()\"  interface=\"action-sheet\">\n          <ion-select-option  *ngFor=\"let type of types\" [value]=\"type\">\n                      {{type}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n    </div>\n    <div *ngIf=\"costinc\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'myWallet-transaction.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select formControlName=\"transactionType\"  [(ngModel)]=\"checkpointen\" interface=\"action-sheet\">\n          <ion-select-option  *ngFor=\"let test of walletsJSON; let mi = index\">\n                                                    {{test.TRAN_TYPE_DESCR}}\n      \n                      \n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n    </div>\n\n     <div *ngIf=\"el\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'myWallet-transaction.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select formControlName=\"otherTransactionType\"  [(ngModel)]=\"checkpointel\"  interface=\"action-sheet\">\n          <ion-select-option  *ngFor=\"let ellang of ellangs\" [value]=\"ellang\">\n                      {{ellang}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n    </div>\n\n    <div class=\"centerItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'myWallet-amount.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-input formControlName=\"ammountOfPayment\"  name=\"number\" placeholder=\"23€\" [(ngModel)]=\"amount\"  (change)=check(amount)  ></ion-input>\n      </ion-item>\n    </div>\n\n    <ion-button class=\"btn-send\" expand=\"block\" type=\"submit\" shape=\"round\" >{{'wallet-update.title' | translate}}</ion-button>\n    <div class=\"myiontab\">\n\n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n        <ion-buttons class=\"addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n          <button (click)=\"goBackToRouteListPageWithDriverId()\" class=\"adBtn2\">\n            <ion-icon name=\"arrow-back\"></ion-icon>\n          </button>\n        </ion-buttons>\n        <ion-tab-button tab=\"routelist\" (click)=\"goingBackToRouteList()\">\n          <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"weAreAlreadyInWalletPage()\">\n          <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n        </ion-tab-button>\n    \n        <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n    \n        <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"navigateToRouteHistoryPage()\">\n          <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n          <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n        </ion-tab-button>\n      </ion-tab-bar>\n    \n    </div>\n  \n  </form>\n\n  </ion-content>\n  <!-- ---------------- !!!!!! ---------------- --> \n  \n");

/***/ }),

/***/ "./src/app/wallet/wallet-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/wallet/wallet-routing.module.ts ***!
  \*************************************************/
/*! exports provided: WalletPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageRoutingModule", function() { return WalletPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wallet.page */ "./src/app/wallet/wallet.page.ts");




const routes = [
    {
        path: '',
        component: _wallet_page__WEBPACK_IMPORTED_MODULE_3__["WalletPage"]
    }
];
let WalletPageRoutingModule = class WalletPageRoutingModule {
};
WalletPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], WalletPageRoutingModule);



/***/ }),

/***/ "./src/app/wallet/wallet.module.ts":
/*!*****************************************!*\
  !*** ./src/app/wallet/wallet.module.ts ***!
  \*****************************************/
/*! exports provided: WalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wallet-routing.module */ "./src/app/wallet/wallet-routing.module.ts");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wallet.page */ "./src/app/wallet/wallet.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let WalletPageModule = class WalletPageModule {
};
WalletPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__["WalletPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })
], WalletPageModule);



/***/ }),

/***/ "./src/app/wallet/wallet.page.scss":
/*!*****************************************!*\
  !*** ./src/app/wallet/wallet.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\nion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n  position: fixed;\n}\n\nion-fab ion-fab-button {\n  --box-shadow: none;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.keyboard-open .bottomBar {\n  display: none;\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 7%;\n  width: 100%;\n}\n\n.myiontab {\n  position: relative;\n  top: 10px;\n  padding-top: 199px;\n}\n\n.myiontab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\n\n.myiontab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 70px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 51px;\n  height: 51px;\n  position: absolute;\n  left: 181px;\n  top: -32px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.myiontab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.myiontab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.myiontab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.myiontab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.myiontab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.centerItems {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n}\n\n.titleStyle {\n  position: relative;\n  top: -18px;\n  left: 0px;\n  color: var(--ion-color-ibus-darkblue);\n  font-size: 21px;\n  z-index: 9999;\n  margin-top: 40px;\n  font-family: monteBold;\n}\n\n.labelIon {\n  position: absolute;\n  font-size: 20px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.itemIon {\n  width: 100%;\n  margin-top: 10%;\n}\n\nion-select {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding: 4px 5px 5px 29px;\n  text-align: center;\n}\n\n.money {\n  position: absolute;\n  color: white;\n  justify-content: center;\n  display: flex;\n  background-color: transparent;\n  font-size: 80px;\n  width: 100%;\n  font-family: monteBold;\n}\n\n.md .money {\n  position: absolute;\n  color: white;\n  justify-content: center;\n  display: flex;\n  background-color: transparent;\n  font-size: 80px;\n  width: 100%;\n  font-family: monteBold;\n}\n\n.ios .money {\n  position: absolute;\n  color: white;\n  justify-content: center;\n  display: flex;\n  background-color: transparent;\n  font-size: 80px;\n  width: 100%;\n  font-family: monteBold;\n}\n\nion-input {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding: 3px 1px 1px 34px;\n  text-align: center;\n}\n\n::-moz-placeholder {\n  color: white;\n  opacity: 1;\n}\n\n::placeholder {\n  color: white;\n  opacity: 1;\n}\n\n@media only screen and (max-width: 375px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 100px;\n  }\n\n  ion-list {\n    position: relative;\n    left: -8px;\n  }\n\n  .btn-accept {\n    width: 80px;\n    height: 36px;\n    position: absolute;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-size: 13px;\n    text-align: center;\n    border-radius: 5px;\n    left: 185px;\n  }\n\n  ion-title {\n    font-size: 15px;\n    position: relative;\n    top: -74px;\n    left: 10px;\n    color: white;\n  }\n\n  .ios .money {\n    position: absolute;\n    color: white;\n    justify-content: center;\n    width: 100%;\n    display: flex;\n    font-size: 80px;\n    font-family: monteBold;\n    top: -50px;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  #profile {\n    width: 60px;\n    height: 60px;\n    background-color: white;\n    border-radius: 50%;\n    position: relative;\n    left: 27px;\n    margin-bottom: 31px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -30px;\n    color: white;\n    font-size: 21px;\n  }\n\n  .nav-buttons {\n    margin-bottom: 28px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw0Q0FBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0FBRUo7O0FBQUE7RUFDSSxxQkFBQTtFQUNBLFlBQUE7QUFHSjs7QUFEQTtFQUNJLDBDQUFBO0VBQTRDLGlCQUFBO0VBSXhDLGVBQUE7QUFFUjs7QUFMRTtFQUNDLGtCQUFBO0FBT0g7O0FBSEE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7QUFNSjs7QUFKQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQU9KOztBQUxBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBUUo7O0FBTkE7RUFDSSxtQkFBQTtBQVNKOztBQVBBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBVUo7O0FBUkE7RUFDSSxvQkFBQTtBQVdKOztBQVRBO0VBQ0ksYUFBQTtBQVlKOztBQVRBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG1FQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUFZSjs7QUFUQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FBWUo7O0FBWEM7RUFDQywwQ0FBQTtFQUE0QyxpQkFBQTtBQWM5Qzs7QUFiRTtFQUNDLGtCQUFBO0VBQ0EsVUFBQTtBQWVIOztBQVpDO0VBNkJDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFBUSxRQUFBO0VBQ0YsV0FBQTtFQUNBLFlBQUE7QUFiUjs7QUFyQlE7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLDBDQUFBO0VBQTRDLGlCQUFBO0FBd0J4RDs7QUF2Qlk7RUFDSSxrQkFBQTtFQUNBLCtDQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBeUJoQjs7QUF2Qlk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBeUJoQjs7QUF2Qlk7RUFDQSx5QkFBQTtBQXlCWjs7QUFmRTtFQUNDLFlBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtBQWlCSDs7QUFmRTtFQUNDLG9DQUFBO0FBaUJIOztBQWZFO0VBQ0MsaUJBQUE7RUFDQSw2QkFBQTtBQWlCSDs7QUFmRTtFQUNDLGdCQUFBO0VBQ0EsNEJBQUE7QUFpQkg7O0FBZkU7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7QUFpQkg7O0FBaEJHO0VBQ0MsNEJBQUE7QUFrQko7O0FBYkE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFnQko7O0FBZEE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBaUJKOztBQWZBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0VBQ0EscUNBQUE7QUFrQko7O0FBaEJBO0VBQ0kscUNBQUE7QUFtQko7O0FBakJBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFvQko7O0FBakJBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtBQW9CSjs7QUFsQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EscUNBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFxQko7O0FBbkJBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQXNCSjs7QUFwQkE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQXVCSjs7QUFyQkE7RUFDSSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsK0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQXdCSjs7QUF0QkE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUF5Qko7O0FBdkJBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FBMEJKOztBQXJCQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLDZCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQXdCSjs7QUF0QkE7RUFDSSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsK0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQXlCSjs7QUF0QkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtBQXlCSjs7QUEzQkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtBQXlCSjs7QUFwQkE7RUFDSTtJQUNDLHFCQUFBO0lBQ0EsYUFBQTtFQXVCSDs7RUFyQkU7SUFDQyxrQkFBQTtJQUNBLFVBQUE7RUF3Qkg7O0VBdEJFO0lBQ0MsV0FBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtJQUNBLCtDQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsV0FBQTtFQXlCSDs7RUF2QkU7SUFDQyxlQUFBO0lBQ0Esa0JBQUE7SUFDQSxVQUFBO0lBQ0EsVUFBQTtJQUNBLFlBQUE7RUEwQkg7O0VBeEJFO0lBQ0ksa0JBQUE7SUFDQSxZQUFBO0lBQ0EsdUJBQUE7SUFDQSxXQUFBO0lBQ0EsYUFBQTtJQUNBLGVBQUE7SUFDQSxzQkFBQTtJQUNBLFVBQUE7RUEyQk47QUFDRjs7QUF4Qkc7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsYUFBQTtFQTBCUDtBQUNGOztBQXhCRztFQUNFO0lBQ0kscUJBQUE7SUFDQSxZQUFBO0VBMEJQO0FBQ0Y7O0FBdkJHO0VBQ0U7SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUF5QlA7O0VBdkJHO0lBQ0ksV0FBQTtJQUNBLFlBQUE7SUFDQSx1QkFBQTtJQUNBLGtCQUFBO0lBQ0Esa0JBQUE7SUFDQSxVQUFBO0lBQ0EsbUJBQUE7RUEwQlA7O0VBeEJHO0lBQ0ksa0JBQUE7SUFDQSxVQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VBMkJQOztFQXpCRztJQUNJLG1CQUFBO0VBNEJQO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG59XHJcbi5pb3MgaW9uLXRvb2xiYXJ7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICBoZWlnaHQ6IDEyN3B4O1xyXG59XHJcbi5tZCBpb24tdG9vbGJhcntcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogODdweDtcclxufVxyXG5pb24tZmFie1xyXG4gICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwb3NpdGlvbjpmaXhlZDtcclxufVxyXG4uaW9zICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5tZCAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxufVxyXG4ua2V5Ym9hcmQtb3BlbiAuYm90dG9tQmFye1xyXG4gICAgZGlzcGxheTpub25lO1xyXG59XHJcblxyXG4uYnRuLXNlbmR7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBoZWlnaHQ6MTg7XHJcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICM0RTk4RkYgOTAlLCM0RTk4RkYgMTAlKTtcclxuICAgIG1hcmdpbi10b3A6NyU7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG59XHJcblxyXG4ubXlpb250YWJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTk5cHg7XHJcblx0aW9uLWZhYiB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG5cdFx0aW9uLWZhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmU7XHJcblx0XHRcdHotaW5kZXg6IDE7XHJcblx0XHR9XHJcblx0fVxyXG5cdGlvbi10YWItYmFyIHtcclxuICAgICAgICAuYWRkQnRuIHtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuICAgICAgICAgICAgYnV0dG9uIHtcclxuICAgICAgICAgICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgbGVmdDogMTgxcHg7XHJcbiAgICAgICAgICAgICAgICB0b3A6IC0zMnB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOjExcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaW9uLWljb257XHJcbiAgICAgICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOjMwcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6MzBweDtcclxuICAgICAgICAgICAgICAgIHN0cm9rZTogNXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlvbi1iYWRnZXtcclxuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblx0XHQtLWJvcmRlcjogMDtcclxuXHRcdC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHRsZWZ0OjA7IHJpZ2h0OiAwO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDo3MHB4O1xyXG5cdFx0JjphZnRlcntcclxuXHRcdFx0Y29udGVudDogXCIgXCI7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRib3R0b206IDA7XHJcblx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHRcdGhlaWdodDogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pO1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbiB7XHJcblx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uLmNvbW1lbnRzIHtcclxuXHRcdFx0bWFyZ2luLXJpZ2h0OiAwcHg7XHJcblx0XHRcdGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxOHB4O1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24ubm90aWZzIHtcclxuXHRcdFx0bWFyZ2luLWxlZnQ6IDBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMThweDtcclxuXHRcdH1cclxuXHRcdHN2ZyB7ICAgIFxyXG5cdFx0XHR3aWR0aDogNzJweDtcclxuXHRcdFx0bWFyZ2luLXRvcDogMTlweDtcclxuXHRcdFx0cGF0aHtcclxuXHRcdFx0XHRmaWxsOiAgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdFx0fVx0XHRcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuLm5vdEJ0bntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiA0N3B4O1xyXG4gICAgaGVpZ2h0OiA2MnB4O1xyXG59XHJcbi5tZW51QnRue1xyXG4gICAgZm9udC1zaXplOiAxMDBweDtcclxuICAgIHdpZHRoOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5tZW51VHh0e1xyXG4gICAgZm9udC1zaXplOjE4cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZVJlZ3VsYXI7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLmljb25TdHlsaW5ne1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5tZW51VG9vbGJhcntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi10b3A6IDgwcHg7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbn1cclxuXHJcbi5jZW50ZXJJdGVtc3tcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4udGl0bGVTdHlsZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTE4cHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG4gICAgei1pbmRleDogOTk5OTtcclxuICAgIG1hcmdpbi10b3A6IDQwcHg7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG59XHJcbi5sYWJlbElvbntcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgZm9udC1zaXplOjIwcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLml0ZW1Jb257XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDoxMCU7XHJcbn1cclxuaW9uLXNlbGVjdHtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIHBhZGRpbmc6IDRweCA1cHggNXB4IDI5cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1vbmV5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBmb250LXNpemU6IDgwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbn1cclxuLm1kIC5tb25leXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgZm9udC1zaXplOiA4MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgXHJcbiAgIFxyXG4gICBcclxufVxyXG4uaW9zIC5tb25leXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgZm9udC1zaXplOiA4MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG59XHJcbmlvbi1pbnB1dHtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIHBhZGRpbmc6M3B4IDFweCAxcHggMzRweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgXHJcbn1cclxuOjpwbGFjZWhvbGRlcntcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgb3BhY2l0eToxO1xyXG59XHJcblxyXG5cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzc1cHgpIHtcclxuICAgIGlvbi10b29sYmFye1xyXG4gICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgfVxyXG4gICAgaW9uLWxpc3R7XHJcbiAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgIGxlZnQ6IC04cHg7XHJcbiAgICB9XHJcbiAgICAuYnRuLWFjY2VwdHtcclxuICAgICB3aWR0aDogODBweDtcclxuICAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICBsZWZ0OiAxODVweDtcclxuICAgIH1cclxuICAgIGlvbi10aXRsZXtcclxuICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgIHRvcDogLTc0cHg7XHJcbiAgICAgbGVmdDogMTBweDtcclxuICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuaW9zIC5tb25leXtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZm9udC1zaXplOiA4MHB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICAgICAgdG9wOi01MHB4O1xyXG4gICAgfVxyXG4gXHJcbiAgIH1cclxuICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MTRweCkge1xyXG4gICAgIGlvbi10b29sYmFye1xyXG4gICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgIGhlaWdodDogMTI3cHg7XHJcbiAgICAgfVxyXG4gICB9XHJcbiAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgICBpb24tdG9vbGJhcntcclxuICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgICAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICAgfVxyXG4gICB9XHJcbiBcclxuICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgIC5pb3MgaW9uLXRvb2xiYXJ7XHJcbiAgICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgIH1cclxuICAgICAjcHJvZmlsZXtcclxuICAgICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICBsZWZ0OiAyN3B4O1xyXG4gICAgICAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgIH1cclxuICAgICAuaW9zIGlvbi10aXRsZXtcclxuICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICB0b3A6IC0xNXB4O1xyXG4gICAgICAgICBsZWZ0OiAtMzBweDtcclxuICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICBmb250LXNpemU6IDIxcHg7XHJcbiAgICAgfVxyXG4gICAgIC5uYXYtYnV0dG9uc3tcclxuICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjhweDtcclxuICAgICB9XHJcblxyXG4gICB9XHJcbiAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuXHJcblxyXG4gXHJcbiAgIH0iXX0= */");

/***/ }),

/***/ "./src/app/wallet/wallet.page.ts":
/*!***************************************!*\
  !*** ./src/app/wallet/wallet.page.ts ***!
  \***************************************/
/*! exports provided: WalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPage", function() { return WalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");










let WalletPage = class WalletPage {
    constructor(platform, nativeHttp, activatedRoute, loadingCtrl, http, router, formBuilder) {
        this.platform = platform;
        this.nativeHttp = nativeHttp;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.router = router;
        this.formBuilder = formBuilder;
        this.wallettypegre_json = [{ "tran_type_id": "1", "tran_type_sh_name": "TOLL", "tran_type_descr_gre": "Διόδια", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "2", "tran_type_sh_name": "FUEL", "tran_type_descr_gre": "Καύσιμα", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "3", "tran_type_sh_name": "OILS", "tran_type_descr_gre": "Λάδια", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "4", "tran_type_sh_name": "MAINT", "tran_type_descr_gre": "Συντήρηση", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "5", "tran_type_sh_name": "PARK", "tran_type_descr_gre": "Πάρκινγκ", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "6", "tran_type_sh_name": "PAY", "tran_type_descr_gre": "Πληρωμή από πελάτη", "tran_type": "INCOME", "is_active": "1" }];
        this.wallettypeeng_json = [{ "tran_type_id": "1", "tran_type_sh_name": "TOLL", "tran_type_descr_eng": "Toll", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "2", "tran_type_sh_name": "FUEL", "tran_type_descr_eng": "Fuel", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "3", "tran_type_sh_name": "OILS", "tran_type_descr_eng": "Oils", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "4", "tran_type_sh_name": "MAINT", "tran_type_descr_eng": "Maintenace", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "5", "tran_type_sh_name": "PARK", "tran_type_descr_eng": "Parking", "tran_type": "COST", "is_active": "1" },
            { "tran_type_id": "6", "tran_type_sh_name": "PAY", "tran_type_descr_eng": "Payment from customer", "tran_type": "INCOME", "is_active": "1" }];
        this.types = ["Cost", "Income"];
        this.language = localStorage.getItem('lang');
        this.eng = false;
        this.el = false;
        this.errorMessages = {
            typeOfPayment: [
                { type: 'required', message: 'Type of payment is required' },
            ],
            ammountOfPayment: [
                { type: 'required', message: 'Amount of Payment is required' },
            ]
        };
        this.paymentForm = this.formBuilder.group({
            typeOfPayment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            ammountOfPayment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            transactionType: [''],
            otherTransactionType: [''],
            tranId: ['']
        });
        this.serviceRegistration = "";
        this.dataFromService = "";
        this.wallets = [];
        this.walletsJSONtoArray = [];
        this.walletsArray = [];
        //   ionViewWillEnter(){
        // 	console.log('wallet');
        // 	let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_list.cfm?' 
        // 	+ 'driver_id=' + this.dataTakenFromRouteSTartedListJSON 
        // 	+ '&userid=dmta'
        // 	, {}, {
        // 		'Content-Type': 'application/json'
        // 	})
        // 	from(myNativeCall).pipe(
        // 		finalize( () => console.log())
        // 		)
        // 	.subscribe( (data) => {
        // 		let parsed = JSON.parse(data.data).WTRANS;
        // 		this.wallets = parsed;
        // 		this.walletsJSON = this.wallets;
        // 		console.log(this.walletsJSON);
        // 		for(var i = 0; i<this.walletsJSON.length; i++){
        // 			console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
        // 			console.log(this.walletsJSON[i].TRAN_TYPE_ID);
        // 			console.log(this.walletsJSON[i].DEBIT);
        // 		}
        // 	})
        //   }
        this.costinc = false;
        this.donedone = [];
        this.tranid = false;
        this.thePaymentForm = [];
        this.dataTakenFromRouteStartedList = this.activatedRoute.snapshot.paramMap.get('routestartedetails');
        this.dataTakenFromRouteSTartedListJSON = JSON.parse(this.dataTakenFromRouteStartedList);
        console.log('%c DATA TAKEN FROM ROUTE STARTED', 'color:yellow');
        console.log(this.dataTakenFromRouteSTartedListJSON.PERSON_ID);
        this.personId = this.dataTakenFromRouteSTartedListJSON.PERSON_ID;
        // TA ROUTE DETAILS 
        console.log(this.dataTakenFromRouteSTartedListJSON);
        this.dataFromAllOverTheApplicationBringingDriverId = this.activatedRoute.snapshot.paramMap.get('fordriverid');
        this.dataFromAllOverTheApplicationBringingDriverIdJSON = JSON.parse(this.dataFromAllOverTheApplicationBringingDriverId);
        console.log('%c DATA FROM ROUTELIST JSON', 'color:orange;');
        console.log(this.dataFromAllOverTheApplicationBringingDriverIdJSON);
        this.Id = this.dataFromAllOverTheApplicationBringingDriverIdJSON;
        console.log(this.language);
        this.enlangs = [];
        this.ellangs = [];
        this.ids = [];
    }
    //greg 
    get typeOfPayment() {
        return this.paymentForm.get('typeOfPayment');
    }
    get tranId() {
        return this.paymentForm.get('tranId');
    }
    get transactionType() {
        return this.paymentForm.get('transactionType');
    }
    get otherTransactionType() {
        return this.paymentForm.get('otherTransactionType');
    }
    get ammountOfPayment() {
        return this.paymentForm.get('ammountOfPayment');
    }
    ngOnInit() {
    }
    confirmPaymentWithEveryHttp() {
        this.platform.is('cordova') ? this.confirm() : this.confirmHttpClient();
    }
    confirmHttpClient() {
        console.log('%c HTTP CLIENT ', 'color:orange;');
        if (this.selectedValue == 'Cost' && this.language == 'en') {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            this.http.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=eng&tran_type=COST&userid=dmta')
                .subscribe((data) => {
                let parsed = data;
                this.wallets = parsed;
                this.walletsJSON = this.wallets.WTYPES;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_ID;
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else if (this.selectedValue == 'Income' && this.language == 'en') {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            this.http.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=eng&tran_type=INCOME&userid=dmta')
                .subscribe((data) => {
                let parsed = data;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets.WTYPES;
                ;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else if (this.selectedValue == 'Cost' && this.language == 'gr') {
            console.log('inside gr');
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=gre&tran_type=COST&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=gre&tran_type=INCOME&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        // this.enlangs=[];
        // this.ellangs=[];
        //  		var i=0;
        // 	var j=0;
        // if(this.language=="en" && this.selectedValue=="Cost"){
        // 	console.log("here");
        // 	for(i=0; i<this.wallettypeeng_json.length; i++){
        // 		console.log("here2");
        // 		if(this.wallettypeeng_json[i].tran_type=="COST"){
        // 			console.log("here3");
        // 		this.enlangs[j]=this.wallettypeeng_json[i].tran_type_descr_eng;
        // 		this.ids[j]=this.wallettypeeng_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.el=false;
        // 	this.eng=true;
        // }
        //  if(this.language=="gr" && this.selectedValue=="Cost"){
        // 	for(i=0; i<this.wallettypegre_json.length; i++){
        // 		if(this.wallettypegre_json[i].tran_type=="COST"){
        // 		this.ellangs[j]=this.wallettypegre_json[i].tran_type_descr_gre;
        // 		this.ids[j]=this.wallettypegre_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.eng=false;
        // 	this.el=true;
        // }
        //  if(this.language=="en" && this.selectedValue=="Income"){
        // 	for(i=0; i<this.wallettypeeng_json.length; i++){
        // 		if(this.wallettypeeng_json[i].tran_type=="INCOME"){
        // 		this.enlangs[j]=this.wallettypeeng_json[i].tran_type_descr_eng;
        // 		this.ids[j]=this.wallettypeeng_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.el=false;
        // 	this.eng=true;
        // }
        //  if(this.language=="gr" && this.selectedValue=="Income"){
        // 	for(i=0; i<this.wallettypegre_json.length; i++){
        // 		if(this.wallettypegre_json[i].tran_type=="INCOME"){
        // 		this.ellangs[j]=this.wallettypegre_json[i].tran_type_descr_gre;
        // 		this.ids[j]=this.wallettypegre_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.eng=false;
        // 	this.el=true;
        // }
        console.log(myPaymentForm.tranId);
    }
    confirm() {
        if (this.selectedValue == 'Cost' && this.language == 'en') {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=eng&tran_type=COST&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_ID;
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else if (this.selectedValue == 'Income' && this.language == 'en') {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=eng&tran_type=INCOME&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else if (this.selectedValue == 'Cost' && this.language == 'gr') {
            console.log('inside gr');
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=gre&tran_type=COST&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        else {
            var myPaymentForm = this.paymentForm.value;
            console.log('im in send data');
            console.log(myPaymentForm.typeOfPayment); // Income
            console.log('call this');
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_type.cfm?lang=gre&tran_type=INCOME&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log()))
                .subscribe((data) => {
                let parsed = JSON.parse(data.data).WTYPES;
                this.wallets = parsed;
                //
                this.walletsJSON = this.wallets;
                console.log(this.walletsJSON);
                for (var i = 0; i < this.walletsJSON.length; i++) {
                    console.log(this.walletsJSON[i].TRAN_TYPE_ID);
                    console.log(this.walletsJSON[i].TRAN_TYPE_SH_NAME);
                    console.log(this.walletsJSON[i].TRAN_TYPE_DESCR);
                    this.donedone = this.walletsJSON[i].TRAN_TYPE_DESCR;
                    console.log(this.donedone);
                    console.log(this.walletsJSON[i].INCOME);
                }
                this.costinc = true;
            });
        }
        // this.enlangs=[];
        // this.ellangs=[];
        //  		var i=0;
        // 	var j=0;
        // if(this.language=="en" && this.selectedValue=="Cost"){
        // 	console.log("here");
        // 	for(i=0; i<this.wallettypeeng_json.length; i++){
        // 		console.log("here2");
        // 		if(this.wallettypeeng_json[i].tran_type=="COST"){
        // 			console.log("here3");
        // 		this.enlangs[j]=this.wallettypeeng_json[i].tran_type_descr_eng;
        // 		this.ids[j]=this.wallettypeeng_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.el=false;
        // 	this.eng=true;
        // }
        //  if(this.language=="gr" && this.selectedValue=="Cost"){
        // 	for(i=0; i<this.wallettypegre_json.length; i++){
        // 		if(this.wallettypegre_json[i].tran_type=="COST"){
        // 		this.ellangs[j]=this.wallettypegre_json[i].tran_type_descr_gre;
        // 		this.ids[j]=this.wallettypegre_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.eng=false;
        // 	this.el=true;
        // }
        //  if(this.language=="en" && this.selectedValue=="Income"){
        // 	for(i=0; i<this.wallettypeeng_json.length; i++){
        // 		if(this.wallettypeeng_json[i].tran_type=="INCOME"){
        // 		this.enlangs[j]=this.wallettypeeng_json[i].tran_type_descr_eng;
        // 		this.ids[j]=this.wallettypeeng_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.el=false;
        // 	this.eng=true;
        // }
        //  if(this.language=="gr" && this.selectedValue=="Income"){
        // 	for(i=0; i<this.wallettypegre_json.length; i++){
        // 		if(this.wallettypegre_json[i].tran_type=="INCOME"){
        // 		this.ellangs[j]=this.wallettypegre_json[i].tran_type_descr_gre;
        // 		this.ids[j]=this.wallettypegre_json[i].tran_type_id;
        // 		j=j+1;
        // 	}
        // }
        // 	this.eng=false;
        // 	this.el=true;
        // }
        console.log(myPaymentForm.tranId);
    }
    check(amount) {
        if (this.language == "en") {
            alert(this.amount + " euros as " + this.selectedValue + " for the transaction " + this.tranId);
        }
        else if (this.language == "gr") {
            alert(this.amount + " ευρώ ως " + this.selectedValue + " για την συνναλαγή " + this.checkpointel);
        }
    }
    sendData(paymentForm) {
        this.submit();
    }
    submit() {
        console.log(this.paymentForm.value);
        this.thePaymentForm = this.paymentForm.value;
        console.log(this.thePaymentForm);
        let fromDate = this.dataTakenFromRouteSTartedListJSON.ASSIGNMENT_FROM_DATE;
        let fromDateToGo555 = fromDate.split(/\s/).join(',');
        console.log("from Date", fromDate);
        let fromDateToGo = new Date(fromDateToGo555);
        console.log("from Date 2 ", fromDateToGo);
        let year = fromDateToGo.getFullYear();
        console.log("year", year);
        let month = fromDateToGo.getMonth() + 1;
        console.log(month);
        let date = fromDateToGo.getDate();
        console.log(date);
        let fulldateFromDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        let toDate = this.dataTakenFromRouteSTartedListJSON.ASSIGNMENT_TO_DATE;
        let toDateToGo555 = toDate.split(/\s/).join(',');
        console.log("from Date", toDate);
        let toDateToGo = new Date(toDateToGo555);
        console.log("from Date 2 ", toDateToGo);
        let year2 = toDateToGo.getFullYear();
        console.log("year", year);
        let month2 = toDateToGo.getMonth() + 1;
        console.log(month2);
        let date2 = toDateToGo.getDate();
        console.log(date2);
        let fulldateToDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        console.log('Service code');
        console.log('', this.thePaymentForm.ammountOfPayment);
        // let url='http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_add.cfm'? 
        // + 'driver_id=' + 16 
        // + '&sp_id=' + 1
        // + '&sp_code=' + 2
        // + '&fromd=' + '2021-01-16'
        // + '&tod=' + '2021-01-16'
        // + '&tran_type=' + 'COST'
        // + '&tran_type_id=' +   1
        // + '&srv_type=' + 'CHT'
        // + '&srv_code=' + 2
        // + '&credit=' + 0
        // + '&debit=' + 20.0
        // + '&userid=dmta'
        //  this.http.get(url, {}, {
        // 	'Content-Type': 'application/json'
        // })
        if (this.thePaymentForm.typeOfPayment == 'Cost') {
            if (this.platform.is('cordova')) {
                let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_add.cfm?'
                    + 'driver_id=' + this.dataTakenFromRouteSTartedListJSON.DRIVER_ID
                    + '&sp_id=' + 1
                    + '&sp_code=' + 1 //this.dataTakenFromRouteSTartedListJSON.SP_CODE
                    + '&tod=' + fulldateToDate
                    + '&tran_type=' + this.thePaymentForm.typeOfPayment
                    + '&tran_type_id=' + 1
                    + '&srv_type=' + this.dataTakenFromRouteSTartedListJSON.SERVICE
                    + '&srv_code=' + this.dataTakenFromRouteSTartedListJSON.SERVICECODE
                    + '&credit=' + 0
                    + '&debit=' + this.thePaymentForm.ammountOfPayment
                    + '&userid=dmta', {}, {
                    'Content-Type': 'application/x-www-form-urlencoded',
                });
                Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log('tete')))
                    .subscribe(data => {
                    console.log('mpikes?');
                    console.log(data);
                    this.router.navigate(['routestarted/' + JSON.stringify(this.dataTakenFromRouteSTartedListJSON) + '/' + JSON.stringify(this.dataFromAllOverTheApplicationBringingDriverIdJSON)]);
                }, err => {
                    console.log('Error of Vehicle check', err);
                });
            }
            else {
                this.http.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_add.cfm?'
                    + 'driver_id=' + this.dataTakenFromRouteSTartedListJSON.DRIVER_ID
                    + '&sp_id=' + 1
                    + '&sp_code=' + 1 //this.dataTakenFromRouteSTartedListJSON.SP_CODE
                    + '&fromd=' + fulldateFromDate
                    + '&tod=' + fulldateToDate
                    + '&tran_type=' + this.thePaymentForm.typeOfPayment
                    + '&tran_type_id=' + 1
                    + '&srv_type=' + this.dataTakenFromRouteSTartedListJSON.SERVICE
                    + '&srv_code=' + this.dataTakenFromRouteSTartedListJSON.SERVICECODE
                    + '&credit=' + 0
                    + '&debit=' + this.thePaymentForm.ammountOfPayment
                    + '&userid=dmta')
                    .subscribe(data => {
                    console.log('mpikes?');
                    console.log(data);
                    this.router.navigate(['routestarted/' + JSON.stringify(this.dataTakenFromRouteSTartedListJSON) + '/' + JSON.stringify(this.dataFromAllOverTheApplicationBringingDriverIdJSON)]);
                }, err => {
                    console.log('Error of Vehicle check', err);
                });
            }
        }
        else {
            if (this.platform.is('cordova')) {
                let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_add.cfm?'
                    + 'driver_id=' + this.dataTakenFromRouteSTartedListJSON.DRIVER_ID
                    + '&sp_id=' + 1
                    + '&sp_code=' + 1 //this.dataTakenFromRouteSTartedListJSON.SP_CODE
                    + '&fromd=' + fulldateFromDate
                    + '&tod=' + fulldateToDate
                    + '&tran_type=' + this.thePaymentForm.typeOfPayment
                    + '&tran_type_id=' + 1
                    + '&srv_type=' + this.dataTakenFromRouteSTartedListJSON.SERVICE
                    + '&srv_code=' + this.dataTakenFromRouteSTartedListJSON.SERVICECODE
                    + '&credit=' + this.thePaymentForm.ammountOfPayment
                    + '&debit=' + 0
                    + '&userid=dmta', {}, {
                    'Content-Type': 'application/x-www-form-urlencoded',
                });
                Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => console.log('tete')))
                    .subscribe(data => {
                    console.log('mpikes?');
                    console.log(data);
                    this.router.navigate(['routestarted/' + JSON.stringify(this.dataTakenFromRouteSTartedListJSON) + '/' + JSON.stringify(this.dataFromAllOverTheApplicationBringingDriverIdJSON)]);
                }, err => {
                    console.log('Error of Vehicle check', err);
                });
            }
            else {
                this.http.get('http://cf11.travelsoft.gr/itourapi/drv_wallet_tran_add.cfm?'
                    + 'driver_id=' + this.dataTakenFromRouteSTartedListJSON.DRIVER_ID
                    + '&sp_id=' + 1
                    + '&sp_code=' + 1 //this.dataTakenFromRouteSTartedListJSON.SP_CODE
                    + '&fromd=' + fulldateFromDate
                    + '&tod=' + fulldateToDate
                    + '&tran_type=' + this.thePaymentForm.typeOfPayment
                    + '&tran_type_id=' + 1
                    + '&srv_type=' + this.dataTakenFromRouteSTartedListJSON.SERVICE
                    + '&srv_code=' + this.dataTakenFromRouteSTartedListJSON.SERVICECODE
                    + '&credit=' + this.thePaymentForm.ammountOfPayment
                    + '&debit=' + 0
                    + '&userid=dmta')
                    .subscribe(data => {
                    console.log('mpikes?');
                    console.log(data);
                    this.router.navigate(['routestarted/' + JSON.stringify(this.dataTakenFromRouteSTartedListJSON) + '/' + JSON.stringify(this.dataFromAllOverTheApplicationBringingDriverIdJSON)]);
                }, err => {
                    console.log('Error of Vehicle check', err);
                });
            }
        }
    }
    weAreAlreadyInWalletPage() {
        console.log('%c You are already in Wallet Page', 'color:green;');
    }
    goBackToRouteListPageWithDriverId() {
        console.log('%c Going Back To Route list Page with Driver Id', 'color:orange;');
        console.log(this.dataFromAllOverTheApplicationBringingDriverIdJSON);
        this.router.navigate(['routestarted/' + JSON.stringify(this.dataTakenFromRouteSTartedListJSON) + '/' + this.Id]);
    }
    navigateToSettingsPage() {
        this.router.navigate(['settings']);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(['routehistory']);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(['techhistory']);
    }
    navigateToWalletPage() {
        this.router.navigate(['wallet']);
    }
    navigateToProfilePage() {
        this.router.navigate(['profile']);
    }
    navigateToNotificationsPage() {
        this.router.navigate(['notifications']);
    }
    goingBackToRouteList() {
        this.router.navigate(['routelist/' + this.Id + '/' + this.personId]);
    }
};
WalletPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_8__["HTTP"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }
];
WalletPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-wallet',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./wallet.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./wallet.page.scss */ "./src/app/wallet/wallet.page.scss")).default]
    })
], WalletPage);



/***/ })

}]);
//# sourceMappingURL=wallet-wallet-module-es2015.js.map