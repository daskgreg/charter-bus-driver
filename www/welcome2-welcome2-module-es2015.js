(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["welcome2-welcome2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>welcome2</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/welcome2/welcome2-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/welcome2/welcome2-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: Welcome2PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Welcome2PageRoutingModule", function() { return Welcome2PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _welcome2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./welcome2.page */ "./src/app/welcome2/welcome2.page.ts");




const routes = [
    {
        path: '',
        component: _welcome2_page__WEBPACK_IMPORTED_MODULE_3__["Welcome2Page"]
    }
];
let Welcome2PageRoutingModule = class Welcome2PageRoutingModule {
};
Welcome2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Welcome2PageRoutingModule);



/***/ }),

/***/ "./src/app/welcome2/welcome2.module.ts":
/*!*********************************************!*\
  !*** ./src/app/welcome2/welcome2.module.ts ***!
  \*********************************************/
/*! exports provided: Welcome2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Welcome2PageModule", function() { return Welcome2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _welcome2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./welcome2-routing.module */ "./src/app/welcome2/welcome2-routing.module.ts");
/* harmony import */ var _welcome2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./welcome2.page */ "./src/app/welcome2/welcome2.page.ts");







let Welcome2PageModule = class Welcome2PageModule {
};
Welcome2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _welcome2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Welcome2PageRoutingModule"]
        ],
        declarations: [_welcome2_page__WEBPACK_IMPORTED_MODULE_6__["Welcome2Page"]]
    })
], Welcome2PageModule);



/***/ }),

/***/ "./src/app/welcome2/welcome2.page.scss":
/*!*********************************************!*\
  !*** ./src/app/welcome2/welcome2.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUyL3dlbGNvbWUyLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/welcome2/welcome2.page.ts":
/*!*******************************************!*\
  !*** ./src/app/welcome2/welcome2.page.ts ***!
  \*******************************************/
/*! exports provided: Welcome2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Welcome2Page", function() { return Welcome2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let Welcome2Page = class Welcome2Page {
    constructor() { }
    ngOnInit() {
    }
};
Welcome2Page.ctorParameters = () => [];
Welcome2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-welcome2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./welcome2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./welcome2.page.scss */ "./src/app/welcome2/welcome2.page.scss")).default]
    })
], Welcome2Page);



/***/ })

}]);
//# sourceMappingURL=welcome2-welcome2-module-es2015.js.map