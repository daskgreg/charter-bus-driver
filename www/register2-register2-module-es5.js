(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register2-register2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/register2/register2.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register2/register2.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRegister2Register2PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"registration-page\">\n  <ion-toolbar color=\"blue\">\n    <ion-title class=\"toolbar-title center\">{{'register2-info2.title' | translate}}</ion-title>\n    <ion-buttons>\n    <ion-button class=\"back-btn\" (click)=\"navigateBack()\"><ion-icon class=\"arrow-back\" slot=\"start\" name=\"arrow-back\"></ion-icon></ion-button>\n  </ion-buttons>\n  </ion-toolbar>\n\n  <!-- Code goes here -->\n\n  <div class=\"center\">\n    <img src=\"assets/images/registration_steps_one.svg\" alt=\"ibus registration steps\" />\n  </div>\n  <div class=\"steps-desc\">\n    <a class=\"step-1\">{{'register-info.title' | translate}}</a>\n    <a class=\"step-2\">{{'register-addressinfo.title' | translate}}</a>\n    <a class=\"step-3\">{{'register-driverinfo.title' | translate}}</a>\n  </div>\n  <div class=\"registration-desc\">\n    <a>{{'register-form.title' | translate}}</a>\n  </div>\n\n\n  <form id=\"form\" [formGroup]=\"registrationForm2\" (ngSubmit)=\"submit()\">\n    <ion-list>\n      <ion-item lines='none'>\n        <ion-input  formControlName=\"country\" name=\"country\"  type=\"text\" placeholder=\"{{'register2-country.title' | translate}}\" required>\n        </ion-input>  <ion-icon slot=\"end\" name=\"flag\" ios=\"flag\"></ion-icon>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.country\">\n        <ng-container *ngIf=\"country.hasError(error.type) && (country.dirty || country.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n      <ion-item lines=\"none\">\n        <ion-input  formControlName=\"location\" name=\"Location\" type=\"text\" placeholder=\"{{'register2-state.title' | translate}}\" required></ion-input>\n        <ion-icon slot=\"end\" name=\"ribbon\"></ion-icon>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.location\">\n        <ng-container *ngIf=\"location.hasError(error.type) && (location.dirty || location.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n      <ion-item lines=\"none\">\n        <ion-input formControlName=\"address\" name=\"address\" type=\"text\" placeholder=\"{{'register2-address.title' | translate}}\" required></ion-input>\n        <ion-icon slot=\"end\" name=\"home\"></ion-icon>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.address\">\n        <ng-container *ngIf=\"address.hasError(error.type) && (address.dirty || address.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n      <ion-item lines=\"none\">\n        <ion-input formControlName=\"zip\" name=\"zip\" type=\"text\" placeholder=\"{{'register2-zip.title' | translate}}\" required></ion-input>\n        <ion-icon slot=\"end\" name=\"code\"></ion-icon>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.zip\">\n        <ng-container *ngIf=\"zip.hasError(error.type) && (zip.dirty || zip.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n\n\n      <ion-button [disabled]=\"!registrationForm2.valid\" (click)=\"sendData()\" type=\"submit\" class=\"btn-send\" expand=\"block\" shape=\"round\" >{{'register-next.title' | translate}} </ion-button>\n    </ion-list>\n  </form>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/register2/register2-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/register2/register2-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: Register2PageRoutingModule */

    /***/
    function srcAppRegister2Register2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register2PageRoutingModule", function () {
        return Register2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _register2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./register2.page */
      "./src/app/register2/register2.page.ts");

      var routes = [{
        path: '',
        component: _register2_page__WEBPACK_IMPORTED_MODULE_3__["Register2Page"]
      }];

      var Register2PageRoutingModule = function Register2PageRoutingModule() {
        _classCallCheck(this, Register2PageRoutingModule);
      };

      Register2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Register2PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/register2/register2.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/register2/register2.module.ts ***!
      \***********************************************/

    /*! exports provided: Register2PageModule */

    /***/
    function srcAppRegister2Register2ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register2PageModule", function () {
        return Register2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _register2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register2-routing.module */
      "./src/app/register2/register2-routing.module.ts");
      /* harmony import */


      var _register2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register2.page */
      "./src/app/register2/register2.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var Register2PageModule = function Register2PageModule() {
        _classCallCheck(this, Register2PageModule);
      };

      Register2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _register2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Register2PageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_register2_page__WEBPACK_IMPORTED_MODULE_6__["Register2Page"]]
      })], Register2PageModule);
      /***/
    },

    /***/
    "./src/app/register2/register2.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/register2/register2.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppRegister2Register2PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".registration-page {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -6px;\n}\n\n.toolbar-title {\n  font-size: 16px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 2px;\n}\n\n.registration-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteBold;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 65px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n  background-color: white;\n  position: relative;\n  top: 100px;\n}\n\nion-icon {\n  position: absolute;\n  right: 29px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\nion-input {\n  height: 80px;\n  font-size: 15px;\n  color: var(--ion-color-ibus-darkblue);\n  font-family: monteMedium;\n  margin-top: 3%;\n  border-radius: 21px;\n  position: relative;\n  border: 2px solid var(--ion-color-ibus-blue);\n  text-align: center;\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 7%;\n}\n\n.steps-desc {\n  position: relative;\n  top: 10px;\n}\n\n.step-1 {\n  position: relative;\n  left: 30px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-2 {\n  position: relative;\n  left: 70px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-3 {\n  position: relative;\n  left: 120px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n@media only screen and (max-width: 428px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 7px;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 0px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 14px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -2px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 12px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIyL3JlZ2lzdGVyMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUFDSjs7QUFDQTtFQUNJLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBRUo7O0FBSUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FBREo7O0FBS0E7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBRko7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxxQ0FBQTtBQURKOztBQUlBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSw0Q0FBQTtFQUNBLGtCQUFBO0FBREo7O0FBS0E7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUVBQUE7RUFDQSxjQUFBO0FBRko7O0FBS0E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7QUFGSjs7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBREo7O0FBR0E7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQUFKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFDSjs7QUFDQTtFQUVJO0lBQ0ksa0JBQUE7SUFDQSxTQUFBO0lBQ0EsU0FBQTtFQUNOO0FBQ0Y7O0FBQ0E7RUFFSTtJQUNJLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUFBTjtBQUNGOztBQUVBO0VBQ0k7SUFDSSxhQUFBO0VBQU47O0VBRUU7SUFDSSxlQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQ0FBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtFQUNOO0FBQ0Y7O0FBQ0E7RUFDSTtJQUNJLGFBQUE7RUFDTjs7RUFDRTtJQUNJLGVBQUE7SUFDQSxzQkFBQTtJQUNBLGlDQUFBO0lBQ0Esa0JBQUE7SUFDQSxTQUFBO0VBRU47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyMi9yZWdpc3RlcjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlZ2lzdHJhdGlvbi1wYWdle1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICB0b3A6NTVweDtcclxuICAgIHotaW5kZXg6MjAwO1xyXG59XHJcblxyXG4uYmFjay1idG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDMwcHg7XHJcbiAgICBsZWZ0OjIwcHg7XHJcbiAgICB3aWR0aDo1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcblxyXG4uYXJyb3ctYmFja3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDEwcHg7XHJcbiAgICB0b3A6IC02cHg7XHJcbn1cclxuLnRvb2xiYXItdGl0bGV7XHJcbiAgICBmb250LXNpemU6MTZweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpOyAgIFxyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBib3R0b206MnB4O1xyXG59XHJcbi8vIGltZ3tcclxuLy8gICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4vLyAgICAgbGVmdDozNXB4O1xyXG4vLyB9XHJcbi5yZWdpc3RyYXRpb24tZGVzY3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiA2NXB4O1xyXG59XHJcblxyXG5cclxuI2Zvcm0ge1xyXG4gICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6MzBweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6MzBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6MTAwcHg7XHJcbn1cclxuaW9uLWljb257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMjlweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbmlvbi1pbnB1dHtcclxuICAgIGhlaWdodDogODBweDtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVNZWRpdW07XHJcbiAgICBtYXJnaW4tdG9wOjMlO1xyXG4gICAgYm9yZGVyLXJhZGl1czoyMXB4O1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBib3JkZXI6MnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgXHJcbn1cclxuXHJcbi5idG4tc2VuZHtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGhlaWdodDoxODtcclxuICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgIzRFOThGRiA5MCUsIzRFOThGRiAxMCUpO1xyXG4gICAgbWFyZ2luLXRvcDo3JTtcclxufVxyXG5cclxuLnN0ZXBzLWRlc2N7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHRvcDoxMHB4O1xyXG59XHJcbi5zdGVwLTF7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIGxlZnQ6MzBweDtcclxuICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5zdGVwLTJ7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIGxlZnQ6NzBweDtcclxuICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5zdGVwLTN7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIGxlZnQ6MTIwcHg7XHJcbiAgICBmb250LXNpemU6MTFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyOHB4KSB7XHJcbiBcclxuICAgIC5zdGVwcy1kZXNje1xyXG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDoxMHB4O1xyXG4gICAgICAgIGxlZnQ6N3B4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuIFxyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOjEwcHg7XHJcbiAgICAgICAgbGVmdDowcHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgfVxyXG4gICAgLnRvb2xiYXItdGl0bGV7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6LTJweDtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgICAuc3RlcHMtZGVzY3tcclxuICAgICAgICBkaXNwbGF5Om5vbmU7XHJcbiAgICB9XHJcbiAgICAudG9vbGJhci10aXRsZXtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDotNXB4O1xyXG4gICAgfVxyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/register2/register2.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/register2/register2.page.ts ***!
      \*********************************************/

    /*! exports provided: Register2Page */

    /***/
    function srcAppRegister2Register2PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register2Page", function () {
        return Register2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var Register2Page = /*#__PURE__*/function () {
        function Register2Page(activatedRoute, http, formBuilder, router) {
          _classCallCheck(this, Register2Page);

          this.activatedRoute = activatedRoute;
          this.http = http;
          this.formBuilder = formBuilder;
          this.router = router;
          this.errorMessages = {
            country: [{
              type: 'required',
              message: 'Country is required'
            }, {
              type: 'maxlength',
              message: 'This field should be no longer than 20 characters'
            }],
            location: [{
              type: 'required',
              message: 'Location is required'
            }, {
              type: 'maxlength',
              message: 'This field should be no longer than 20 characters'
            }],
            address: [{
              type: 'required',
              message: 'Address is required'
            }, {
              type: 'maxlength',
              message: 'This field should be no longer than 20 characters'
            }],
            zip: [{
              type: 'required',
              message: 'Zip Code is required'
            }, {
              type: 'maxlength',
              message: 'This field should be no longer than 20 characters'
            }]
          };
          this.registrationForm2 = this.formBuilder.group({
            country: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(20)]],
            location: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(20)]],
            address: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(20)]],
            zip: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[0-9]{4,15}$')]]
          });
          this.serviceRegistration = "";
          this.dataFromService = "";
          this.dataCameFromRegisterPage = this.activatedRoute.snapshot.paramMap.get('information');
          this.dataCameFromRegisterPageJSON = JSON.parse(this.dataCameFromRegisterPage);
          console.log('%c Data Came', 'color:yellow;');
          console.log(this.dataCameFromRegisterPageJSON);
          console.log(this.dataCameFromRegisterPageJSON.email);
        } //greg 


        _createClass(Register2Page, [{
          key: "submit",
          value: function submit() {
            var _this = this;

            console.log(this.registrationForm2.value);
            this.thisIsMyRegistrationFromThatIwillAccessData = this.registrationForm2.value; //address: "address"
            // country: "country"
            // location: "state"
            //zip: "01234"

            console.log('%c COUNTRY ', 'color:orange;');
            console.log(this.thisIsMyRegistrationFromThatIwillAccessData.country);
            var myRegistrationForm = this.registrationForm2.value;
            this.sendData(myRegistrationForm).subscribe(function (dataReturnFromService) {
              _this.dataFromService = JSON.stringify(dataReturnFromService);
              console.log(JSON.stringify(dataReturnFromService));
              console.log(dataReturnFromService['_body']);

              _this.router.navigate(['register3/' + JSON.stringify(_this.dataCameFromRegisterPageJSON) + '/' + JSON.stringify(_this.thisIsMyRegistrationFromThatIwillAccessData)]);
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "sendData",
          value: function sendData(myRegistrationForm) {
            var url = "https://reqres.in/api/users";
            return this.http.post(url, myRegistrationForm, {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                "content-type": "application/json"
              })
            });
          }
        }, {
          key: "navigateBack",
          value: function navigateBack() {
            this.router.navigate(['register']);
          }
        }, {
          key: "country",
          get: function get() {
            return this.registrationForm2.get('country');
          }
        }, {
          key: "location",
          get: function get() {
            return this.registrationForm2.get('location');
          }
        }, {
          key: "address",
          get: function get() {
            return this.registrationForm2.get('address');
          }
        }, {
          key: "zip",
          get: function get() {
            return this.registrationForm2.get('zip');
          }
        }]);

        return Register2Page;
      }();

      Register2Page.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      Register2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./register2.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/register2/register2.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./register2.page.scss */
        "./src/app/register2/register2.page.scss"))["default"]]
      })], Register2Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=register2-register2-module-es5.js.map