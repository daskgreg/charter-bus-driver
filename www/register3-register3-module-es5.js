(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register3-register3-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/register3/register3.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register3/register3.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRegister3Register3PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"registration-page\">\n  <ion-toolbar color=\"blue\">\n    <ion-title class=\"toolbar-title center\">{{'register-driverinfo.title' | translate}}</ion-title>\n    <ion-buttons>\n    <ion-button class=\"back-btn\" (click)=\"navigateBack()\"><ion-icon class=\"arrow-back\" slot=\"start\" name=\"arrow-back\"></ion-icon></ion-button>\n  </ion-buttons>\n  </ion-toolbar>\n\n  <!-- Code goes here -->\n\n  <div class=\"center\">\n    <img src=\"assets/images/registration_steps_two.svg\" alt=\"ibus registration steps\" />\n  </div>\n  <div class=\"steps-desc\">\n    <a class=\"step-1\">{{'register-info.title' | translate}}</a>\n    <a class=\"step-2\">{{'register-addressinfo.title' | translate}}</a>\n    <a class=\"step-3\">{{'register-driverinfo.title' | translate}}</a>\n  </div>\n  <div class=\"registration-desc\">\n    <a>{{'register-form.title' | translate}}</a>\n  </div>\n<!--   \n  <ion-button class=\"btn-login\"  (click)=\"takePicture()\" expand=\"block\" shape=\"round\" >reg</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"takePicture2()\" expand=\"block\" shape=\"round\" >ins</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"takePicture3()\" expand=\"block\" shape=\"round\" >prof</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"takePicture4()\" expand=\"block\" shape=\"round\" >drv_img</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"call2()\" expand=\"block\" shape=\"round\" >send</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"takePicture4()\" expand=\"block\" shape=\"round\" >drv_img</ion-button>\n  <ion-button class=\"btn-login\"  (click)=\"call3()\" expand=\"block\" shape=\"round\" >send_drv_img</ion-button>\n   -->\n   <ion-button  class=\"btn-pic\" (click)=\"takePicture()\" expand=\"block\" shape=\"round\" >Licence</ion-button>\n   <ion-button  class=\"btn-pic\" (click)=\"takePicture2()\" expand=\"block\" shape=\"round\" >Insurance</ion-button>\n   <ion-button  class=\"btn-pic\"  (click)=\"takePicture3()\" expand=\"block\" shape=\"round\" >Profile</ion-button>\n  <form id=\"form\" [formGroup]=\"finalRegistrationForm\" (ngSubmit)=\"getSignUpFromEveryPlatForm()\">\n    \n    <ion-list>\n      <ion-item lines='none'>\n        <ion-icon  name=\"newspaper\" ios=\"newspaper\"></ion-icon>\n        <ion-input formControlName=\"licNumber\" class=\"fileclass\" name=\"licNumber\"  type=\"text\" placeholder=\"{{'register3-lnumber.title' | translate}}\" required>\n        </ion-input>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.licNumber\">\n        <ng-container *ngIf=\"licNumber.hasError(error.type) && (licNumber.dirty || licNumber.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n      <ion-item lines='none'>\n        <ion-icon  name=\"newspaper\" ios=\"newspaper\"></ion-icon>\n        <ion-input formControlName=\"plate\" class=\"fileclass\" name=\"plate\"  type=\"text\" placeholder=\"{{'register3-lnumber.title' | translate}}\" required>\n        </ion-input>\n      </ion-item>\n      <div *ngFor=\"let error of errorMessages.plate\">\n        <ng-container *ngIf=\"plate.hasError(error.type) && (plate.dirty || plate.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n    \n      <ion-button [disabled]=\"!finalRegistrationForm.valid\" class=\"btn-send\" expand=\"block\" type=\"submit\" shape=\"round\" (click)=\"call2()\" >{{'register-next.title' | translate}}</ion-button>\n    </ion-list>\n  </form>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/register3/register3-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/register3/register3-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: Register3PageRoutingModule */

    /***/
    function srcAppRegister3Register3RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register3PageRoutingModule", function () {
        return Register3PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _register3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./register3.page */
      "./src/app/register3/register3.page.ts");

      var routes = [{
        path: '',
        component: _register3_page__WEBPACK_IMPORTED_MODULE_3__["Register3Page"]
      }];

      var Register3PageRoutingModule = function Register3PageRoutingModule() {
        _classCallCheck(this, Register3PageRoutingModule);
      };

      Register3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Register3PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/register3/register3.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/register3/register3.module.ts ***!
      \***********************************************/

    /*! exports provided: Register3PageModule */

    /***/
    function srcAppRegister3Register3ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register3PageModule", function () {
        return Register3PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _register3_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register3-routing.module */
      "./src/app/register3/register3-routing.module.ts");
      /* harmony import */


      var _register3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register3.page */
      "./src/app/register3/register3.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var Register3PageModule = function Register3PageModule() {
        _classCallCheck(this, Register3PageModule);
      };

      Register3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _register3_routing_module__WEBPACK_IMPORTED_MODULE_5__["Register3PageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_register3_page__WEBPACK_IMPORTED_MODULE_6__["Register3Page"]]
      })], Register3PageModule);
      /***/
    },

    /***/
    "./src/app/register3/register3.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/register3/register3.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppRegister3Register3PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".registration-page {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.fileInput {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1;\n}\n\n.fileclass {\n  margin-top: 3%;\n  height: 70px;\n  font-size: 16px;\n  color: var(--ion-color-ibus-darkblue);\n  font-family: monteMedium;\n  border-radius: 21px;\n  border: 2px solid var(--ion-color-ibus-blue);\n  text-align: center;\n  position: relative;\n  width: 100%;\n  padding: 19px;\n}\n\n.fileInput label {\n  color: #717171;\n  background-color: white;\n  display: inline-block;\n  cursor: pointer;\n  padding: 0.5em 1em;\n  border: 1px solid #ccc;\n  cursor: pointer;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -6px;\n}\n\n.toolbar-title {\n  font-size: 16px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 2px;\n}\n\n.registration-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteBold;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 65px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n  background-color: white;\n  position: relative;\n  top: 100px;\n}\n\nion-icon {\n  position: absolute;\n  right: 29px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.licence-number-icon {\n  position: absolute;\n  top: 21px;\n  right: 21px;\n  z-index: 3000;\n  margin-top: 3px;\n}\n\nion-input {\n  height: 70px;\n  font-size: 15px;\n  color: var(--ion-color-ibus-darkblue);\n  font-family: monteMedium;\n  border-radius: 21px;\n  border: 2px solid var(--ion-color-ibus-blue);\n  text-align: center;\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 4%;\n}\n\n.btn-pic {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n}\n\n.steps-desc {\n  position: relative;\n  top: 10px;\n}\n\n.step-1 {\n  position: relative;\n  left: 30px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-2 {\n  position: relative;\n  left: 70px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-3 {\n  position: relative;\n  left: 120px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n@media only screen and (max-width: 428px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 7px;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 0px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 14px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -2px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 12px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIzL3JlZ2lzdGVyMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFDQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBRUo7O0FBQUE7RUFDSSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7RUFDQSw0Q0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQUdKOztBQURBO0VBQ0ksY0FBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7QUFJSjs7QUFEQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUFJSjs7QUFGQTtFQUNJLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBS0o7O0FBQ0E7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FBRUo7O0FBRUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBQ0o7O0FBUUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxxQ0FBQTtBQUxKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FBSko7O0FBT0E7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0Esd0JBQUE7RUFDQSxtQkFBQTtFQUNBLDRDQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFPQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxtRUFBQTtFQUNBLGNBQUE7QUFKSjs7QUFNQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxtRUFBQTtBQUhKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0FBSko7O0FBTUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQUhKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFGSjs7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBREo7O0FBSUE7RUFFSTtJQUNJLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUFGTjtBQUNGOztBQUlBO0VBRUk7SUFDSSxrQkFBQTtJQUNBLFNBQUE7SUFDQSxTQUFBO0VBSE47QUFDRjs7QUFLQTtFQUNJO0lBQ0ksYUFBQTtFQUhOOztFQUtFO0lBQ0ksZUFBQTtJQUNBLHNCQUFBO0lBQ0EsaUNBQUE7SUFDQSxrQkFBQTtJQUNBLFNBQUE7RUFGTjtBQUNGOztBQUlBO0VBQ0k7SUFDSSxhQUFBO0VBRk47O0VBSUU7SUFDSSxlQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQ0FBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtFQUROO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3RlcjMvcmVnaXN0ZXIzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yZWdpc3RyYXRpb24tcGFnZXtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgdG9wOjU1cHg7XHJcbiAgICB6LWluZGV4OjIwMDtcclxufVxyXG5cclxuLmJhY2stYnRue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm90dG9tOiAzMHB4O1xyXG4gICAgbGVmdDoyMHB4O1xyXG4gICAgd2lkdGg6NTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG4uZmlsZUlucHV0IHtcclxuICAgIHdpZHRoOiAwLjFweDtcclxuICAgIGhlaWdodDogMC4xcHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IC0xO1xyXG59XHJcbi5maWxlY2xhc3N7XHJcbiAgICBtYXJnaW4tdG9wOjMlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZU1lZGl1bTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMTlweDtcclxufVxyXG4uZmlsZUlucHV0ICAgbGFiZWwge1xyXG4gICAgY29sb3I6ICM3MTcxNzE7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHBhZGRpbmc6IC41ZW0gMWVtO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmFycm93LWJhY2t7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAxMHB4O1xyXG4gICAgdG9wOiAtNnB4O1xyXG59XHJcbi50b29sYmFyLXRpdGxle1xyXG4gICAgZm9udC1zaXplOjE2cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTsgICBcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgYm90dG9tOjJweDtcclxufVxyXG4vLyBpbWd7XHJcbi8vICAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuLy8gICAgIGxlZnQ6MzVweDtcclxuLy8gfVxyXG4ucmVnaXN0cmF0aW9uLWRlc2N7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgZm9udC1zaXplOiAyNnB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogNjVweDtcclxufVxyXG5cclxuXHJcbiNmb3JtIHtcclxuICAgIG1hcmdpbi10b3A6NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OjMwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OjMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOjEwMHB4O1xyXG59XHJcbi8vIGlvbi1pdGVte1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICBoZWlnaHQ6IDgwcHg7XHJcbi8vICAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4vLyAgICAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbi8vICAgICBtYXJnaW4tdG9wOjMlXHJcbi8vIH1cclxuaW9uLWljb257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMjlweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLmxpY2VuY2UtbnVtYmVyLWljb257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDIxcHg7XHJcbiAgICByaWdodDogMjFweDtcclxuICAgIHotaW5kZXg6IDMwMDA7XHJcbiAgICBtYXJnaW4tdG9wOjNweDtcclxuXHJcbn1cclxuaW9uLWlucHV0eyBcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVNZWRpdW07XHJcbiAgICBib3JkZXItcmFkaXVzOjIxcHg7XHJcbiAgICBib3JkZXI6MnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uYnRuLXNlbmR7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBoZWlnaHQ6MTg7XHJcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICM0RTk4RkYgOTAlLCM0RTk4RkYgMTAlKTtcclxuICAgIG1hcmdpbi10b3A6NCU7XHJcbn1cclxuLmJ0bi1waWN7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBoZWlnaHQ6MTg7XHJcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICM0RTk4RkYgOTAlLCM0RTk4RkYgMTAlKTtcclxuICAgIFxyXG59XHJcblxyXG4uc3RlcHMtZGVzY3tcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgdG9wOjEwcHg7XHJcbn1cclxuLnN0ZXAtMXtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDozMHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLnN0ZXAtMntcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDo3MHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLnN0ZXAtM3tcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDoxMjBweDtcclxuICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyOHB4KSB7XHJcbiBcclxuICAgIC5zdGVwcy1kZXNje1xyXG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDoxMHB4O1xyXG4gICAgICAgIGxlZnQ6N3B4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuIFxyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOjEwcHg7XHJcbiAgICAgICAgbGVmdDowcHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgfVxyXG4gICAgLnRvb2xiYXItdGl0bGV7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6LTJweDtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDM3NXB4KSB7XHJcbiAgICAuc3RlcHMtZGVzY3tcclxuICAgICAgICBkaXNwbGF5Om5vbmU7XHJcbiAgICB9XHJcbiAgICAudG9vbGJhci10aXRsZXtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDotNXB4O1xyXG4gICAgfVxyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/register3/register3.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/register3/register3.page.ts ***!
      \*********************************************/

    /*! exports provided: Register3Page */

    /***/
    function srcAppRegister3Register3PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Register3Page", function () {
        return Register3Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/http/ngx */
      "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");

      var Camera = _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"].Camera;

      var Register3Page = /*#__PURE__*/function () {
        function Register3Page(platform, loadingCtrl, activatedRoute, formBuilder, http, router, nativeHttp) {
          _classCallCheck(this, Register3Page);

          this.platform = platform;
          this.loadingCtrl = loadingCtrl;
          this.activatedRoute = activatedRoute;
          this.formBuilder = formBuilder;
          this.http = http;
          this.router = router;
          this.nativeHttp = nativeHttp;
          this.errorMessages = {
            licNumber: [{
              type: 'required',
              message: 'Please Fill Licence Number'
            }],
            plate: [{
              type: 'required',
              message: 'Please Fill Plates Number'
            }]
          };
          this.finalRegistrationForm = this.formBuilder.group({
            licNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            plate: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
          });
          this.dataFromRegisterOne = this.activatedRoute.snapshot.paramMap.get('infofromregisterone');
          this.dataFromRegisterOneJSON = JSON.parse(this.dataFromRegisterOne);
          console.log('%c Register 1 ', 'color:lightblue;');
          console.log(this.dataFromRegisterOneJSON);
          this.dataFromRegisterTwo = this.activatedRoute.snapshot.paramMap.get('infofromregistertwo');
          this.dataFromRegisterTwoJSON = JSON.parse(this.dataFromRegisterTwo);
          console.log('%c Register 2 ', 'color:lightblue;');
          console.log(this.dataFromRegisterTwoJSON);
        } // http://localhost:8100/register2/%7B%22firstName%22:%22Greg%22,%22lastName%22:%22Daskalakis%22,%22birth%22:%222021-02-12%22,%22email%22:%22dask.gregory@gmail.com%22,%22phoneNumber%22:%226984863548%22,%22pass%22:%22123123%22,%22confirmPassword%22:%22123123%22%7D


        _createClass(Register3Page, [{
          key: "takePicture4",
          value: function takePicture4() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var image;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return Camera.getPhoto({
                        quality: 90,
                        allowEditing: true,
                        resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["CameraResultType"].Base64
                      });

                    case 2:
                      image = _context.sent;
                      this.img = image.base64String;
                      console.log(image);

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "call3",
          value: function call3() {
            var formData2 = new FormData();
            formData2.append("photo", this.img);
            console.log(formData2);
            var date = new Date().getHours();
            var date2 = new Date().getMinutes();
            var kati = date + "_" + date2;
            this.http.post('http://cf11.travelsoft.gr/itourapi/chrbus_drv_img.cfm?driver_id=16&srv_type=CHT&srv_code=2&sp_id=1&sp_code=6&fromd=2020/11/27&tod=2020/11/27&vehicle_map_id=1025&vhc_id=1&vhc_plates=VFR111&version_id=1&VechicleTypeID=1&virtualversion_id=1&img_type=TOLL&latitude=37.865044&longitude=23.755045&pickup_address=kapou&first_name=christos24&last_name=christos24&time=' + kati + '&userid=dmta', formData2).subscribe(function (data) {
              console.log(data);
            }); //native
          }
        }, {
          key: "takePicture",
          value: function takePicture() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var image;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Camera.getPhoto({
                        quality: 90,
                        allowEditing: true,
                        resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["CameraResultType"].Base64
                      });

                    case 2:
                      image = _context2.sent;
                      this.reg = image.base64String;
                      console.log(image);
                      this.kappa = true;

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "takePicture2",
          value: function takePicture2() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var image;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return Camera.getPhoto({
                        quality: 90,
                        allowEditing: true,
                        resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["CameraResultType"].Base64
                      });

                    case 2:
                      image = _context3.sent;
                      this.ins = image.base64String;
                      console.log(image);

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "takePicture3",
          value: function takePicture3() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var image;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return Camera.getPhoto({
                        quality: 90,
                        allowEditing: true,
                        resultType: _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["CameraResultType"].Base64
                      });

                    case 2:
                      image = _context4.sent;
                      this.prof = image.base64String;
                      console.log(image);

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "call2",
          value: function call2() {
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            var formData = new FormData();
            formData.append("reg", this.reg);
            formData.append("ins", this.ins);
            formData.append("prof", this.prof);
            console.log(formData);
            this.http.post('http://cf11.travelsoft.gr/itourapi/trp_driver_signup.cfm?first_name=christos24&last_name=christos24&license_number=1235456&birthday=23/02/2020&password=1234567&mobile=0123455555&country=1&location=greece&address=krimpa&zip=123465&email=christos23test@mail.gr&userid=dmta', formData).subscribe(function (data) {
              console.log(data);
            });
          }
        }, {
          key: "submit",
          value: function submit() {
            var _this = this;

            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            var formData = new FormData();
            formData.append("reg", this.reg);
            formData.append("ins", this.ins);
            formData.append("prof", this.prof);
            console.log(formData);
            console.log('%c Data From Final Registration Form', 'color:yellow;');
            console.log(this.finalRegistrationForm.value);
            this.signupIsDone = this.finalRegistrationForm.value;
            console.log('%c Passed Data From Final Registration Form', 'color:red;');
            console.log(this.signupIsDone.licNumber);
            this.http.post('http://cf11.travelsoft.gr/itourapi/trp_driver_signup.cfm?' + '&firstName=' + this.dataFromRegisterOneJSON.firstName + '&lastName=' + this.dataFromRegisterOneJSON.lastName + '&licNumber=' + this.signupIsDone.licNumber + '&birthday=' + this.dataFromRegisterOneJSON.birth + '&password=' + this.dataFromRegisterOneJSON.pass + '&mobile=' + this.dataFromRegisterOneJSON.phoneNumber + '&location=' + this.dataFromRegisterTwoJSON.location + '&country=' + 1 + '&location=' + this.dataFromRegisterTwoJSON.country + '&address=' + this.dataFromRegisterTwoJSON.address + '&zip=' + this.dataFromRegisterTwoJSON.zip + '&email=' + this.dataFromRegisterOneJSON.email + '&userid=dmta', formData // + '&plate='      + this.signupIsDone.plate
            ).subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                var _this2 = this;

                var loader;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                  while (1) {
                    switch (_context5.prev = _context5.next) {
                      case 0:
                        console.log('%c FINAL DATA FROM SIGN UP BEFORE SEND TO API', 'color:orange;');
                        console.log(data);
                        _context5.next = 4;
                        return this.loadingCtrl.create({
                          message: "You have successfully sign up"
                        });

                      case 4:
                        loader = _context5.sent;
                        loader.present();
                        setTimeout(function () {
                          loader.dismiss();

                          _this2.router.navigate(['registercomplete']);
                        }, 1000);

                      case 7:
                      case "end":
                        return _context5.stop();
                    }
                  }
                }, _callee5, this);
              }));
            });
          }
        }, {
          key: "nativeSubmit",
          value: function nativeSubmit() {
            var _this3 = this;

            console.log('%c Data From Final Registration Form', 'color:yellow;');
            console.log(this.finalRegistrationForm.value);
            this.signupIsDone = this.finalRegistrationForm.value;
            console.log('%c Passed Data From Final Registration Form', 'color:red;');
            console.log(this.signupIsDone.licNumber);
            this.nativeHttp.setDataSerializer('urlencoded');
            var formData2 = {
              reg: this.reg,
              ins: this.ins,
              prof: this.prof
            };
            var headers = {
              "Accept": "application/json",
              "api-auth": 'apiAuthToken String',
              "User-Auth": 'userAuthToken String'
            }; // const formData2 = new FormData();
            //  formData2.append("photo", this.img);
            //  console.log(formData2);

            this.nativeHttp.setDataSerializer('urlencoded');
            this.nativeHttp.setHeader('*', 'Content-Type', 'application/x-www-form-urlencoded'); // this.http_native.post('url String', formData, headers).then(api_response => {
            // });

            var date = new Date().getHours();
            var date2 = new Date().getMinutes();
            var kati = date + "_" + date2;
            this.nativeHttp.post('http://cf11.travelsoft.gr/itourapi/trp_driver_signup.cfm?' + '&first_name=' + this.dataFromRegisterOneJSON.firstName + '&last_name=' + this.dataFromRegisterOneJSON.lastName + '&license_number=' + this.signupIsDone.licNumber + '&birthday=' + this.dataFromRegisterOneJSON.birth + '&password=' + this.dataFromRegisterOneJSON.pass + '&mobile=' + this.dataFromRegisterOneJSON.phoneNumber + '&location=' + this.dataFromRegisterTwoJSON.location + '&country=' + 1 + '&address=' + this.dataFromRegisterTwoJSON.address + '&zip=' + this.dataFromRegisterTwoJSON.zip + '&email=' + this.dataFromRegisterOneJSON.email + '&userid=dmta', formData2, headers // + '&plate='      + this.signupIsDone.plate
            ).then(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                var _this4 = this;

                var loader;
                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                  while (1) {
                    switch (_context6.prev = _context6.next) {
                      case 0:
                        console.log('%c FINAL DATA FROM SIGN UP BEFORE SEND TO API', 'color:orange;');
                        console.log(data);
                        _context6.next = 4;
                        return this.loadingCtrl.create({
                          message: "You have successfully sign up"
                        });

                      case 4:
                        loader = _context6.sent;
                        loader.present();
                        setTimeout(function () {
                          loader.dismiss();

                          _this4.router.navigate(['registercomplete']);
                        }, 1000);

                      case 7:
                      case "end":
                        return _context6.stop();
                    }
                  }
                }, _callee6, this);
              }));
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getSignUpFromEveryPlatForm",
          value: function getSignUpFromEveryPlatForm() {
            this.platform.is('cordova') ? this.nativeSubmit() : this.submit();
          }
        }, {
          key: "onFileChange",
          value: function onFileChange(fileChangeEvent) {
            this.file = fileChangeEvent.target.files[0];
          }
        }, {
          key: "registerCompleted",
          value: function registerCompleted() {
            this.router.navigate(['registercomplete']);
          }
        }, {
          key: "navigateBack",
          value: function navigateBack() {
            this.router.navigate(['register2/' + JSON.stringify(this.dataFromRegisterTwoJSON)]);
          }
        }, {
          key: "licNumber",
          get: function get() {
            return this.finalRegistrationForm.get('licNumber');
          }
        }, {
          key: "plate",
          get: function get() {
            return this.finalRegistrationForm.get('plate');
          }
        }, {
          key: "registerId",
          get: function get() {
            return this.finalRegistrationForm.get('registerId');
          }
        }, {
          key: "licenceId",
          get: function get() {
            return this.finalRegistrationForm.get('licenceId');
          }
        }, {
          key: "profileId",
          get: function get() {
            return this.finalRegistrationForm.get('profileId');
          }
        }]);

        return Register3Page;
      }();

      Register3Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__["HTTP"]
        }];
      };

      Register3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-register3',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./register3.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/register3/register3.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./register3.page.scss */
        "./src/app/register3/register3.page.scss"))["default"]]
      })], Register3Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=register3-register3-module-es5.js.map