(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-register-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRegisterRegisterPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"registration-page\">\n  <ion-toolbar color=\"blue\">\n    <ion-title class=\"toolbar-title center\">{{'register-info1.title' | translate}}</ion-title>\n    <ion-buttons>\n    <ion-button class=\"back-btn\" (click)=\"navigateBack()\"><ion-icon class=\"arrow-back\" slot=\"start\" name=\"arrow-back\"></ion-icon></ion-button>\n  </ion-buttons>\n  </ion-toolbar>\n\n  <!-- Code goes here -->\n\n  <div class=\"center\">\n    <img src=\"assets/images/registration_steps.svg\" alt=\"ibus registration steps\" />\n  </div>\n  <div class=\"steps-desc\">\n    <a class=\"step-1\">{{'register-info.title' | translate}}</a>\n    <a class=\"step-2\">{{'register-addressinfo.title' | translate}}</a>\n    <a class=\"step-3\">{{'register-driverinfo.title' | translate}}</a>\n  </div>\n  <div class=\"registration-desc\">\n    <a>{{'register-form.title' | translate}}</a>\n  </div>\n\n\n  <form id=\"form\" [formGroup]=\"registrationForm\" (ngSubmit)=\"submit()\">\n    <ion-list>\n      <ion-item lines='none'>\n        <ion-input  formControlName=\"firstName\" name=\"firstName\" inputmode=\"text\" type=\"text\" placeholder=\"{{'register-fname.title' | translate}}\" required>\n        </ion-input>  <ion-icon slot=\"end\" name=\"person\" ios=\"mail\"></ion-icon>\n      </ion-item>\n        <div *ngFor=\"let error of errorMessages.firstName\">\n          <ng-container *ngIf=\"firstName.hasError(error.type) && (firstName.dirty || firstName.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"lastName\" name=\"lastName\" inputmode=\"text\" type=\"text\" placeholder=\"{{'register-lname.title' | translate}}\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"person\"></ion-icon>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.lastName\">\n          <ng-container *ngIf=\"lastName.hasError(error.type) && (lastName.dirty || lastName.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"birth\" name=\"birth\" type=\"date\" placeholder=\"mm/dd/yyyy\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"calendar\"></ion-icon>\n        </ion-item>\n  \n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"email\" name=\"email\" inputmode=\"email\" type=\"email\" placeholder=\"Email\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"mail\"></ion-icon>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.email\">\n          <ng-container *ngIf=\"email.hasError(error.type) && (email.dirty || email.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"phoneNumber\" name=\"phoneNumber\" inputmode=\"tel\" type=\"tel\" placeholder=\"{{'forgot-phone.title' | translate}}\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"call\"></ion-icon>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.phoneNumber\">\n          <ng-container *ngIf=\"phoneNumber.hasError(error.type) && (phoneNumber.dirty || phoneNumber.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n         \n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"pass\" name=\"pass\" inputmode=\"password\" type=\"password\" placeholder=\"{{'register-confirmpass.title' | translate}}\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"eye\"></ion-icon>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.pass\">\n          <ng-container *ngIf=\"pass.hasError(error.type) && (pass.dirty || pass.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n        <ion-item lines=\"none\">\n          <ion-input formControlName=\"confirmPassword\" name=\"confirmPassword\" inputmode=\"confirmPassword\"type=\"password\" placeholder=\"{{'register-pass.title' | translate}}\" required></ion-input>\n          <ion-icon slot=\"end\" name=\"eye\"></ion-icon>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.confirmPassword\">\n          <ng-container *ngIf=\"confirmPassword.hasError(error.type) && (confirmPassword.dirty || confirmPassword.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div> \n  \n      <ion-button [disabled]=\"!registrationForm.valid\" type=\"submit\" class=\"btn-send\" expand=\"block\" shape=\"round\"  (click)=\"sendData()\"  >{{'register-next.title' | translate}}</ion-button>\n    </ion-list>\n  </form>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/register/register-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/register/register-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: RegisterPageRoutingModule */

    /***/
    function srcAppRegisterRegisterRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageRoutingModule", function () {
        return RegisterPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./register.page */
      "./src/app/register/register.page.ts");

      var routes = [{
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
      }];

      var RegisterPageRoutingModule = function RegisterPageRoutingModule() {
        _classCallCheck(this, RegisterPageRoutingModule);
      };

      RegisterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegisterPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/register/register.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/register/register.module.ts ***!
      \*********************************************/

    /*! exports provided: RegisterPageModule */

    /***/
    function srcAppRegisterRegisterModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function () {
        return RegisterPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _register_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register-routing.module */
      "./src/app/register/register-routing.module.ts");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register.page */
      "./src/app/register/register.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var RegisterPageModule = function RegisterPageModule() {
        _classCallCheck(this, RegisterPageModule);
      };

      RegisterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _register_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]]
      })], RegisterPageModule);
      /***/
    },

    /***/
    "./src/app/register/register.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/register/register.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppRegisterRegisterPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".registration-page {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -6px;\n}\n\n.toolbar-title {\n  font-size: 16px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 2px;\n}\n\n.registration-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteBold;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 65px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n  background-color: white;\n  position: relative;\n  top: 100px;\n}\n\nion-icon {\n  position: absolute;\n  right: 29px;\n  color: var(--ion-color-ibus-darkblue);\n}\n\nion-input {\n  height: 70px;\n  font-size: 15px;\n  color: var(--ion-color-ibus-darkblue);\n  font-family: monteMedium;\n  margin-top: 3%;\n  border-radius: 21px;\n  position: relative;\n  border: 2px solid var(--ion-color-ibus-blue);\n  text-align: center;\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 7%;\n}\n\n.steps-desc {\n  position: relative;\n  top: 10px;\n}\n\n.step-1 {\n  position: relative;\n  left: 30px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-2 {\n  position: relative;\n  left: 70px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-3 {\n  position: relative;\n  left: 120px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n@media only screen and (max-width: 428px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 7px;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 0px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 14px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -2px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 12px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBQ0E7RUFDSSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQUVKOztBQU1BO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQUhKOztBQU9BO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUpKOztBQU1BO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EscUNBQUE7QUFISjs7QUFNQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNENBQUE7RUFDQSxrQkFBQTtBQUhKOztBQU9BO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG1FQUFBO0VBQ0EsY0FBQTtBQUpKOztBQU9BO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0FBSko7O0FBTUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQUhKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFGSjs7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBREo7O0FBS0E7RUFFSTtJQUNJLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUFITjtBQUNGOztBQUtBO0VBRUk7SUFDSSxrQkFBQTtJQUNBLFNBQUE7SUFDQSxTQUFBO0VBSk47QUFDRjs7QUFNQTtFQUNJO0lBQ0ksYUFBQTtFQUpOOztFQU1FO0lBQ0ksZUFBQTtJQUNBLHNCQUFBO0lBQ0EsaUNBQUE7SUFDQSxrQkFBQTtJQUNBLFNBQUE7RUFITjtBQUNGOztBQUtBO0VBQ0k7SUFDSSxhQUFBO0VBSE47O0VBS0U7SUFDSSxlQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQ0FBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtFQUZOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmVnaXN0cmF0aW9uLXBhZ2V7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHRvcDo1NXB4O1xyXG4gICAgei1pbmRleDoyMDA7XHJcbn1cclxuXHJcbi5iYWNrLWJ0bntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvdHRvbTogMzBweDtcclxuICAgIGxlZnQ6MjBweDtcclxuICAgIHdpZHRoOjUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuXHJcbi5hcnJvdy1iYWNre1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMTBweDtcclxuICAgIHRvcDogLTZweDtcclxufVxyXG4udG9vbGJhci10aXRsZXtcclxuICAgIGZvbnQtc2l6ZToxNnB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7ICAgXHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIGJvdHRvbToycHg7XHJcbn1cclxuLy8gaW1ne1xyXG4vLyAgICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbi8vICAgICBsZWZ0OjM1cHg7XHJcbi8vICAgICB3aWR0aDogYXV0bztcclxuLy8gICAgIGhlaWdodDogYXV0bztcclxuLy8gfVxyXG4ucmVnaXN0cmF0aW9uLWRlc2N7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgZm9udC1zaXplOiAyNnB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogNjVweDtcclxufVxyXG5cclxuXHJcbiNmb3JtIHtcclxuICAgIG1hcmdpbi10b3A6NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OjMwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OjMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOjEwMHB4O1xyXG59XHJcbmlvbi1pY29ue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDI5cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcblxyXG5pb24taW5wdXR7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlTWVkaXVtO1xyXG4gICAgbWFyZ2luLXRvcDozJTtcclxuICAgIGJvcmRlci1yYWRpdXM6MjFweDtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgYm9yZGVyOjJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIFxyXG59XHJcblxyXG4uYnRuLXNlbmR7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBoZWlnaHQ6MTg7XHJcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICM0RTk4RkYgOTAlLCM0RTk4RkYgMTAlKTtcclxuICAgIG1hcmdpbi10b3A6NyU7XHJcbn1cclxuXHJcbi5zdGVwcy1kZXNje1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICB0b3A6MTBweDtcclxufVxyXG4uc3RlcC0xe1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjMwcHg7XHJcbiAgICBmb250LXNpemU6MTFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uc3RlcC0ye1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjcwcHg7XHJcbiAgICBmb250LXNpemU6MTFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uc3RlcC0ze1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjEyMHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyOHB4KSB7XHJcbiBcclxuICAgIC5zdGVwcy1kZXNje1xyXG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDoxMHB4O1xyXG4gICAgICAgIGxlZnQ6N3B4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuIFxyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOjEwcHg7XHJcbiAgICAgICAgbGVmdDowcHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgfVxyXG4gICAgLnRvb2xiYXItdGl0bGV7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6LTJweDtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgICAuc3RlcHMtZGVzY3tcclxuICAgICAgICBkaXNwbGF5Om5vbmU7XHJcbiAgICB9XHJcbiAgICAudG9vbGJhci10aXRsZXtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDotNXB4O1xyXG4gICAgfVxyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/register/register.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/register/register.page.ts ***!
      \*******************************************/

    /*! exports provided: RegisterPage */

    /***/
    function srcAppRegisterRegisterPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPage", function () {
        return RegisterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var RegisterPage = /*#__PURE__*/function () {
        function RegisterPage(http, formBuilder, router) {
          _classCallCheck(this, RegisterPage);

          this.http = http;
          this.formBuilder = formBuilder;
          this.router = router;
          this.errorMessages = {
            first_name: [{
              type: 'required',
              message: 'First Name is required'
            }, {
              type: 'maxlength',
              message: 'First Name cant be longer than 100 characters'
            }],
            last_name: [{
              type: 'required',
              message: 'Last Name is required'
            }, {
              type: 'maxlength',
              message: 'Last Name cant be longer than 100 characters'
            }, {
              type: 'minlength',
              message: 'Last Name should be bigger than 2 characters'
            }],
            birthday: [{
              type: 'required',
              message: 'Birth Date is required'
            }],
            email: [{
              type: 'required',
              message: 'Email is required'
            }, {
              type: 'pattern',
              message: 'Please enter a valid Email'
            }],
            mobile: [{
              type: 'required',
              message: 'Phone number is required'
            }, {
              type: 'minlength',
              message: 'Your Phone number should be more than 4 numbers.'
            }, {
              type: 'maxlength',
              message: 'Your Phone number should be less than 15 numbers.'
            }, {
              type: 'pattern',
              message: 'Please enter a valid Phone Number'
            }],
            password: [{
              type: 'required',
              message: 'Password is required'
            }, {
              type: 'minlength',
              message: 'Please enter a password bigger than 5 characters'
            }],
            confirmPassword: [{
              type: 'required',
              message: 'Confirmation of your password is required'
            }, {
              type: 'minlength',
              message: 'Your Passwords do not match'
            }]
          };
          this.registrationForm = this.formBuilder.group({
            firstName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(100)]],
            lastName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(100)]],
            birth: [''],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+.[a-zA-Z]{2,4}$')]],
            phoneNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[0-9]{4,15}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(15), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(4)]],
            pass: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(5)]],
            confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(5)]]
          });
          this.serviceRegistration = "";
          this.dataFromService = "";
          this.isMyRegisterForm = [];
        }

        _createClass(RegisterPage, [{
          key: "submit",
          value: function submit() {
            var _this = this;

            console.log(this.registrationForm.value);
            this.isMyRegisterForm = this.registrationForm.value;
            console.log('%c BIRTH', 'color:yellow;');
            console.log(this.isMyRegisterForm.birth);
            this.router.navigate(['register2/' + JSON.stringify(this.isMyRegisterForm)]);
            var myRegistrationForm = this.registrationForm.value;
            this.sendData(myRegistrationForm).subscribe(function (dataReturnFromService) {
              _this.dataFromService = JSON.stringify(dataReturnFromService);
              console.log(JSON.stringify(dataReturnFromService));
              console.log(dataReturnFromService['_body']);
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "sendData",
          value: function sendData(myRegistrationForm) {
            var url = ""; // var url="http://cf11.travelsoft.gr/itourapi/trp_driver_signup.cfm?userid=dmta";

            return this.http.post(url, myRegistrationForm, {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                "content-type": "application/json"
              })
            });
          }
        }, {
          key: "navigateRegOne",
          value: function navigateRegOne() {
            this.router.navigate(['register2']);
          }
        }, {
          key: "navigateBack",
          value: function navigateBack() {
            this.router.navigate(['login']);
          }
        }, {
          key: "firstName",
          get: function get() {
            return this.registrationForm.get('first_name');
          }
        }, {
          key: "lastName",
          get: function get() {
            return this.registrationForm.get('last_name');
          }
        }, {
          key: "birth",
          get: function get() {
            return this.registrationForm.get('birthday');
          }
        }, {
          key: "email",
          get: function get() {
            return this.registrationForm.get('email');
          }
        }, {
          key: "phoneNumber",
          get: function get() {
            return this.registrationForm.get('mobile');
          }
        }, {
          key: "pass",
          get: function get() {
            return this.registrationForm.get('password');
          }
        }, {
          key: "confirmPassword",
          get: function get() {
            return this.registrationForm.get('confirmPassword');
          }
        }]);

        return RegisterPage;
      }();

      RegisterPage.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-register',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./register.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./register.page.scss */
        "./src/app/register/register.page.scss"))["default"]]
      })], RegisterPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=register-register-module-es5.js.map