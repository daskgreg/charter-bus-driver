(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routedetails-routedetails-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/routedetails/routedetails.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routedetails/routedetails.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding class=\"background\">\n  <ion-title class=\"ion-text-center\">{{'routedetails-details.title' | translate}}</ion-title>\n\n   <ion-label class=\"ion-text-center timeItem\" lines=\"none\">\n      <button class=\"startTimeBtn\">{{thisIsMyStartingPoint}}</button> <!-- {{'routedetails-start.title' | translate}}-->\n    </ion-label>\n  <ion-list class=\"theList\">\n  \t  \n    <ion-label class=\"ion-text-center timeItem\" lines=\"none\" *ngFor=\"let pickup of toMyNewArray\">\n      <button class=\"timeBtn\">{{pickup.PICKUP_ADDRESS}}</button>\n    </ion-label>\n\n  </ion-list>\n   <ion-label class=\"ion-text-center timeItem\" lines=\"none\">\n      <button class=\"endTimeBtn\">{{thisIsmyLastPoint}}</button> <!--{{'routedetails-end.title' | translate}}-->\n    </ion-label>\n\n  <ion-buttons class=\"addBtn\" (click)=\"navigateToMyroutePage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <button (click)=\"navigateToMyroutePage()\" class=\"adBtn2\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n    </button>\n  </ion-buttons>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/routedetails/routedetails-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/routedetails/routedetails-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: RoutedetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutedetailsPageRoutingModule", function() { return RoutedetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _routedetails_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routedetails.page */ "./src/app/routedetails/routedetails.page.ts");




const routes = [
    {
        path: '',
        component: _routedetails_page__WEBPACK_IMPORTED_MODULE_3__["RoutedetailsPage"]
    }
];
let RoutedetailsPageRoutingModule = class RoutedetailsPageRoutingModule {
};
RoutedetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RoutedetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/routedetails/routedetails.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/routedetails/routedetails.module.ts ***!
  \*****************************************************/
/*! exports provided: RoutedetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutedetailsPageModule", function() { return RoutedetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _routedetails_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routedetails-routing.module */ "./src/app/routedetails/routedetails-routing.module.ts");
/* harmony import */ var _routedetails_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./routedetails.page */ "./src/app/routedetails/routedetails.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let RoutedetailsPageModule = class RoutedetailsPageModule {
};
RoutedetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _routedetails_routing_module__WEBPACK_IMPORTED_MODULE_5__["RoutedetailsPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_routedetails_page__WEBPACK_IMPORTED_MODULE_6__["RoutedetailsPage"]]
    })
], RoutedetailsPageModule);



/***/ }),

/***/ "./src/app/routedetails/routedetails.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/routedetails/routedetails.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content.background {\n  --ion-background-color: var( --ion-color-ibus-theblue);\n}\n\n.timeIcon {\n  width: 160px;\n  height: 50px;\n}\n\n.ion-text-center {\n  display: flex;\n  justify-content: center;\n  width: 100vw;\n}\n\n.dmpIconStyle {\n  color: white;\n}\n\n.startTimeBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: var(--ion-color-ibus-lightgreen);\n  font-size: 15px;\n  font-family: monteBold;\n  color: white;\n}\n\n.endTimeBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: var(--ion-color-danger);\n  font-size: 15px;\n  font-family: monteBold;\n  color: white;\n}\n\n.timeBtn {\n  width: 210px;\n  height: 57px;\n  border-radius: 27px;\n  background-color: white;\n  font-size: 15px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\nion-item {\n  position: relative;\n  top: 30px;\n  left: -25px;\n}\n\n.addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.addBtn button {\n  --box-shadow: none;\n  background-color: white;\n  border-radius: 70px;\n  width: 60px;\n  height: 60px;\n  position: relative;\n  top: -9px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n  margin-top: 40px;\n}\n\n.addBtn ion-icon {\n  color: var(--ion-color-ibus-darkblue);\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.addBtn ion-badge {\n  --background: transparent;\n}\n\nion-label {\n  margin-top: 15px;\n}\n\n.md ion-title {\n  color: white;\n  font-size: 30px;\n  margin-top: 47px;\n  margin-bottom: 40px;\n  font-family: monteBold;\n}\n\n.ios ion-title {\n  color: white;\n  font-size: 19px;\n  position: absolute;\n  top: -300px;\n  font-family: monteBold;\n}\n\n.ios .theList {\n  position: relative;\n  top: 200px;\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-title {\n    color: white;\n    font-size: 15px;\n    position: absolute;\n    top: -280px;\n    font-family: monteBold;\n  }\n\n  .ios .theList {\n    position: relative;\n    top: 70px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm91dGVkZXRhaWxzL3JvdXRlZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzREFBQTtBQUNKOztBQUNBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFFSjs7QUFBQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUFHSjs7QUFEQTtFQUNJLFlBQUE7QUFJSjs7QUFGQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxrREFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7QUFLSjs7QUFIQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx5Q0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7QUFNSjs7QUFKQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBT0o7O0FBTEE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBUUo7O0FBSEM7RUFDTyx1QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLDBDQUFBO0VBQTRDLGlCQUFBO0FBT3BEOztBQU5RO0VBQ0ksa0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFRWjs7QUFOUTtFQUNJLHFDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBUVo7O0FBTlE7RUFDQSx5QkFBQTtBQVFSOztBQUpBO0VBQ0ksZ0JBQUE7QUFPSjs7QUFMQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FBUUo7O0FBTkE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FBU0o7O0FBUEE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QUFVSjs7QUFQQTtFQUNJO0lBQ0ksWUFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtJQUNBLFdBQUE7SUFDQSxzQkFBQTtFQVVOOztFQVJFO0lBQ0ksa0JBQUE7SUFDQSxTQUFBO0VBV047QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JvdXRlZGV0YWlscy9yb3V0ZWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQuYmFja2dyb3VuZHtcclxuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IHZhciggLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICB9XHJcbi50aW1lSWNvbntcclxuICAgIHdpZHRoOiAxNjBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG4uaW9uLXRleHQtY2VudGVye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG59XHJcbi5kbXBJY29uU3R5bGV7XHJcbiAgICBjb2xvcjogd2hpdGU7ICBcclxufVxyXG4uc3RhcnRUaW1lQnRue1xyXG4gICAgd2lkdGg6MjEwcHg7XHJcbiAgICBoZWlnaHQ6NTdweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI3cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWxpZ2h0Z3JlZW4pOyAgXHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOndoaXRlO1xyXG59XHJcbi5lbmRUaW1lQnRue1xyXG4gICAgd2lkdGg6MjEwcHg7XHJcbiAgICBoZWlnaHQ6NTdweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI3cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1kYW5nZXIpOyBcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi50aW1lQnRue1xyXG4gICAgd2lkdGg6MjEwcHg7XHJcbiAgICBoZWlnaHQ6NTdweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI3cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbmlvbi1pdGVte1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAzMHB4O1xyXG4gICAgbGVmdDogLTI1cHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHQuYWRkQnRuIHtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG4gICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgICAgICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICB0b3A6IC05cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgICAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6MTFweDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDo0MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpb24taWNvbntcclxuICAgICAgICAgICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgICAgICAgICB3aWR0aDozMHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6MzBweDtcclxuICAgICAgICAgICAgc3Ryb2tlOiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1iYWRnZXtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5pb24tbGFiZWx7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA0N3B4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE5cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6LTMwMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxufVxyXG4uaW9zIC50aGVMaXN0e1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAyMDBweDtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLmlvcyBpb24tdGl0bGV7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOi0yODBweDtcclxuICAgICAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgfVxyXG4gICAgLmlvcyAudGhlTGlzdHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiA3MHB4O1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/routedetails/routedetails.page.ts":
/*!***************************************************!*\
  !*** ./src/app/routedetails/routedetails.page.ts ***!
  \***************************************************/
/*! exports provided: RoutedetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutedetailsPage", function() { return RoutedetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");








let RoutedetailsPage = class RoutedetailsPage {
    constructor(http, platform, loading, nativeHttp, activatedRoute, router) {
        this.http = http;
        this.platform = platform;
        this.loading = loading;
        this.nativeHttp = nativeHttp;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.pickUps = localStorage.getItem('routeDetails');
        this.theRoutingPathSelection = [];
        this.toMyNewArray = [];
        //greg 
        this.dataFromRouteStartedDriverIdJSON = this.activatedRoute.snapshot.paramMap.get('driverid');
        console.log('%c Driver Id', 'color:orange;');
        console.log(this.dataFromRouteStartedDriverIdJSON);
        // this.dataFromRouteStartedDriverIdJSON = JSON.parse(this.dataFromRouteStartedDriverId);
        // console.log(this.dataFromRouteStartedDriverIdJSON);
        this.dataFromRouteStartedDromologioJSON = this.activatedRoute.snapshot.paramMap.get('dromologio');
        console.log('%c Dromologio', 'color:orange;');
        console.log(this.dataFromRouteStartedDromologioJSON);
        // this.dataFromRouteStartedDromologioJSON = JSON.parse(this.dataFromRouteStartedDromologio);
        // console.log(this.dataFromRouteStartedDromologioJSON);
        // this.dataFromRouteStartedPickupsJSON = this.activatedRoute.snapshot.paramMap.get('gogogopick')
        // console.log('%c Pickups', 'color:orange;');
        // console.log(this.dataFromRouteStartedPickupsJSON);
        // this.dataFromRouteStartedPickupsJSON = JSON.parse(this.dataFromRouteStartedPickups);
        // console.log(this.dataFromRouteStartedPickupsJSON);
        console.log(this.toMyNewArray);
        //  console.log('%c JSON','color:yellow;');
        // console.log(this.dataFromRouteStartedDriverIdJSON); // in this line i will take the arrival and departure 
        // this.theRoutingPathSelection = this.dataFromRouteStartedDriverIdJSON.routePath;
        console.log(this.pickUps);
        this.tests = [];
        this.tests = ["Athens", "Komotini", "Alexpoli"];
    }
    ionViewWillEnter() {
        this.platform.is('cordova') ? this.getDetails() : this.getDetailsHTTPclient();
    }
    getDetailsHTTPclient() {
        let url = '';
        this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=' + this.dataFromRouteStartedDriverIdJSON + '&userid=dmta').subscribe(data => {
            let parsed = data;
            console.log('in');
            this.newCustomPickupRoutes = parsed;
            console.log(this.newCustomPickupRoutes);
            this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutes.CUSTPICKUPS;
            console.log(this.newCustomPickupRoutesJSONtoArray);
            this.startingPoint = this.newCustomPickupRoutesJSONtoArray[0].PICKUP_ADDRESS;
            console.log(this.startingPoint);
            for (var i = 1; i < this.newCustomPickupRoutesJSONtoArray.length - 1; i++) {
                this.thisIsMyStartingPoint = this.newCustomPickupRoutesJSONtoArray[0].PICKUP_ADDRESS;
                console.log(this.thisIsMyStartingPoint);
                this.toMyNewArray.push(this.newCustomPickupRoutesJSONtoArray[i]);
                this.thisIsmyLastPoint = this.newCustomPickupRoutesJSONtoArray[i + 1].PICKUP_ADDRESS;
            }
        }, err => {
            console.log("native error", err);
        });
    }
    getDetails() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let loader = yield this.loading.create();
            yield loader.present();
            //let url = 'http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=2&userid=dmta';
            let url = '';
            let myNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?route_id=' + this.dataFromRouteStartedDriverIdJSON + '&userid=dmta', {}, {
                'Content-Type': 'application/json'
            });
            Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["from"])(myNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(() => loader.dismiss()))
                .subscribe(data => {
                let parsed = JSON.parse(data.data);
                console.log('in');
                this.newCustomPickupRoutes = parsed;
                console.log(this.newCustomPickupRoutes);
                this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutes.CUSTPICKUPS;
                console.log(this.newCustomPickupRoutesJSONtoArray);
                this.startingPoint = this.newCustomPickupRoutesJSONtoArray[0].PICKUP_ADDRESS;
                console.log(this.startingPoint);
                for (var i = 1; i < this.newCustomPickupRoutesJSONtoArray.length - 1; i++) {
                    this.thisIsMyStartingPoint = this.newCustomPickupRoutesJSONtoArray[0].PICKUP_ADDRESS;
                    console.log(this.thisIsMyStartingPoint);
                    this.toMyNewArray.push(this.newCustomPickupRoutesJSONtoArray[i]);
                    this.thisIsmyLastPoint = this.newCustomPickupRoutesJSONtoArray[i + 1].PICKUP_ADDRESS;
                }
            }, err => {
                console.log("native error", err);
            });
        });
    }
    ngOnInit() {
    }
    navigateToMyroutePage() {
        this.router.navigate(['routestarted/' + this.dataFromRouteStartedDromologioJSON + '/' + this.dataFromRouteStartedDriverIdJSON]);
    }
};
RoutedetailsPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__["HTTP"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
RoutedetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-routedetails',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./routedetails.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/routedetails/routedetails.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./routedetails.page.scss */ "./src/app/routedetails/routedetails.page.scss")).default]
    })
], RoutedetailsPage);



/***/ })

}]);
//# sourceMappingURL=routedetails-routedetails-module-es2015.js.map