(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSettingsSettingsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home/feed\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon><ion-badge></ion-badge></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'routelist-settings.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n<ion-content>\n  <div class=\"centerItems styleItems ion-padding\">\n    <ion-label class=\"labelIon\">{{'settings-distance.title' | translate}}</ion-label>\n    <ion-item class=\"itemIon\" lines=\"none\">\n      <ion-select [(ngModel)]=\"selectedValueUnit\"  interface=\"action-sheet\">\n        <ion-select-option  *ngFor=\"let item of dataUnit\" [value]=\"item.id\">\n                    {{item.distanceUnit}}\n        </ion-select-option>\n      </ion-select>\n    </ion-item >\n    </div>\n    <div class=\"centerItems styleItems ion-padding\">\n    <ion-label class=\"labelIon\">{{'settings-currency.title' | translate}}</ion-label>\n    <ion-item class=\"itemIon\"  lines=\"none\">\n      <ion-select [(ngModel)]=\"selectedValueCurrency\"  interface=\"action-sheet\">\n        <ion-select-option  *ngFor=\"let item of dataCoin\" [value]=\"item.id\">\n                    {{item.currency}}\n        </ion-select-option>\n      </ion-select>\n    </ion-item>\n  </div>\n  <div class=\"centerItems styleItems ion-padding\">\n    <ion-label class=\"labelIon\">{{'settings-language.title' | translate}}</ion-label>\n    <ion-item class=\"itemIon\" lines=\"none\">\n      <ion-select [(ngModel)]=\"selectedValueLanguage\"  (ionChange)=\"check()\" interface=\"action-sheet\">\n        <ion-select-option *ngFor=\"let language of languages\"  [value]=\"language\">\n                    {{language}}\n        </ion-select-option>\n      </ion-select>\n    </ion-item>\n  </div>\n  \n  <ion-buttons (click)=\"navigateToMaintenancePage()\" >\n   <button class=\"maintBtn\">{{'settings-maintenance.title' | translate}}</button>\n  </ion-buttons>\n\n</ion-content>\n<div class=\"myiontab\">\n\n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n    <ion-buttons class=\"addBtn\" (click)=\"navigateToStartFeedPage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button (click)=\"navigateToStartFeedPage()\" class=\"adBtn2\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-tab-button tab=\"routelist\" (click)=\"navigateToRouteListPage()\">\n      <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"navigateToWalletPage()\">\n      <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n    </ion-tab-button>\n\n    <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n\n    <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"navigateToRouteHistoryPage()\">\n      <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n      <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n    </ion-tab-button>\n  </ion-tab-bar>\n\n</div>\n\n";
      /***/
    },

    /***/
    "./src/app/settings/settings-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/settings/settings-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: SettingsPageRoutingModule */

    /***/
    function srcAppSettingsSettingsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SettingsPageRoutingModule", function () {
        return SettingsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _settings_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./settings.page */
      "./src/app/settings/settings.page.ts");

      var routes = [{
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_3__["SettingsPage"]
      }];

      var SettingsPageRoutingModule = function SettingsPageRoutingModule() {
        _classCallCheck(this, SettingsPageRoutingModule);
      };

      SettingsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SettingsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/settings/settings.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/settings/settings.module.ts ***!
      \*********************************************/

    /*! exports provided: SettingsPageModule */

    /***/
    function srcAppSettingsSettingsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function () {
        return SettingsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _settings_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./settings-routing.module */
      "./src/app/settings/settings-routing.module.ts");
      /* harmony import */


      var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./settings.page */
      "./src/app/settings/settings.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var SettingsPageModule = function SettingsPageModule() {
        _classCallCheck(this, SettingsPageModule);
      };

      SettingsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _settings_routing_module__WEBPACK_IMPORTED_MODULE_5__["SettingsPageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]],
        declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
      })], SettingsPageModule);
      /***/
    },

    /***/
    "./src/app/settings/settings.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/settings/settings.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppSettingsSettingsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\nion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n  position: fixed;\n}\n\nion-fab ion-fab-button {\n  --box-shadow: none;\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.myiontab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\n\n.myiontab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 70px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 51px;\n  height: 51px;\n  position: absolute;\n  left: 181px;\n  top: -32px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.myiontab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.myiontab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.myiontab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.myiontab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.myiontab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.maintBtn {\n  height: 70px;\n  border-radius: 23px;\n  background-color: var(--ion-color-ibus-darkblue);\n  font-family: monteRegular;\n  font-size: 18px;\n  padding: 21px;\n  position: absolute;\n  width: 90%;\n  left: 5%;\n  color: white;\n  top: 20px;\n}\n\n.centerItems {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n}\n\nion-select {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding: 3px 1px 1px 34px;\n}\n\n.itemIon {\n  width: 100%;\n  margin-top: 10%;\n}\n\n.labelIon {\n  position: absolute;\n  font-size: 20px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.action-sheet-button {\n  color: red !important;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNENBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtBQUVKOztBQUFBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBR0o7O0FBREE7RUFDSSwwQ0FBQTtFQUE0QyxpQkFBQTtFQUl4QyxlQUFBO0FBRVI7O0FBTEU7RUFDQyxrQkFBQTtBQU9IOztBQUhBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBTUo7O0FBSkE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBT0o7O0FBTEE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxxQ0FBQTtBQVFKOztBQU5BO0VBQ0kscUNBQUE7QUFTSjs7QUFQQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBVUo7O0FBUkE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7QUFXSjs7QUFUQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQVlKOztBQVZBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBYUo7O0FBWEE7RUFDSSxtQkFBQTtBQWNKOztBQVpBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBZUo7O0FBYkE7RUFDSSxvQkFBQTtBQWdCSjs7QUFaQztFQUNDLDBDQUFBO0VBQTRDLGlCQUFBO0FBZ0I5Qzs7QUFmRTtFQUNDLGtCQUFBO0VBQ0EsVUFBQTtBQWlCSDs7QUFkQztFQTZCQyxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQVEsUUFBQTtFQUNGLFdBQUE7RUFDQSxZQUFBO0FBWFI7O0FBdkJRO0VBQ0ksdUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQ0FBQTtFQUE0QyxpQkFBQTtBQTBCeEQ7O0FBekJZO0VBQ0ksa0JBQUE7RUFDQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQTJCaEI7O0FBekJZO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQTJCaEI7O0FBekJZO0VBQ0EseUJBQUE7QUEyQlo7O0FBakJFO0VBQ0MsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBbUJIOztBQWpCRTtFQUNDLG9DQUFBO0FBbUJIOztBQWpCRTtFQUNDLGlCQUFBO0VBQ0EsNkJBQUE7QUFtQkg7O0FBakJFO0VBQ0MsZ0JBQUE7RUFDQSw0QkFBQTtBQW1CSDs7QUFqQkU7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7QUFtQkg7O0FBbEJHO0VBQ0MsNEJBQUE7QUFvQko7O0FBc0JBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0RBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FBbkJKOztBQXFCQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7QUFsQko7O0FBb0JBO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FBakJKOztBQW1CQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FBaEJKOztBQWtCQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFmSjs7QUFrQkE7RUFDSSxxQkFBQTtBQWZKOztBQWtCQTtFQUNJO0lBQ0kscUJBQUE7SUFDQSxhQUFBO0VBZk47QUFDRjs7QUFpQkU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQWZOO0FBQ0Y7O0FBa0JFO0VBQ0U7SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUFoQk47O0VBa0JFO0lBQ0ksa0JBQUE7SUFDQSxVQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VBZk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG59XHJcbi5pb3MgaW9uLXRvb2xiYXJ7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICBoZWlnaHQ6IDEyN3B4O1xyXG59XHJcbi5tZCBpb24tdG9vbGJhcntcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogODdweDtcclxufVxyXG5pb24tZmFie1xyXG4gICAgbWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwb3NpdGlvbjpmaXhlZDtcclxufVxyXG4ubm90QnRue1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgd2lkdGg6IDQ3cHg7XHJcbiAgICBoZWlnaHQ6IDYycHg7XHJcbn1cclxuLm1lbnVCdG57XHJcbiAgICBmb250LXNpemU6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLm1lbnVUeHR7XHJcbiAgICBmb250LXNpemU6MThweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlUmVndWxhcjtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uaWNvblN0eWxpbmd7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLm1lbnVUb29sYmFye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogODBweDtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxufVxyXG4uaW9zICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5tZCAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxufVxyXG5cclxuLm15aW9udGFie1xyXG5cdGlvbi1mYWIge1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG5cdFx0XHR6LWluZGV4OiAxO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRpb24tdGFiLWJhciB7XHJcbiAgICAgICAgLmFkZEJ0biB7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTsgLyogZml4IG5vdGNoIGlvcyovXHJcbiAgICAgICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNTFweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IDE4MXB4O1xyXG4gICAgICAgICAgICAgICAgdG9wOiAtMzJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6bW9udGVTQm9sZDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDozMHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OjMwcHg7XHJcbiAgICAgICAgICAgICAgICBzdHJva2U6IDVweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpb24tYmFkZ2V7XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cdFx0LS1ib3JkZXI6IDA7XHJcblx0XHQtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Ym90dG9tOiAwO1xyXG5cdFx0bGVmdDowOyByaWdodDogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6NzBweDtcclxuXHRcdCY6YWZ0ZXJ7XHJcblx0XHRcdGNvbnRlbnQ6IFwiIFwiO1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHRoZWlnaHQ6IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5jb21tZW50cyB7XHJcblx0XHRcdG1hcmdpbi1yaWdodDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMThweDtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uLm5vdGlmcyB7XHJcblx0XHRcdG1hcmdpbi1sZWZ0OiAwcHg7XHJcblx0XHRcdGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRzdmcgeyAgICBcclxuXHRcdFx0d2lkdGg6IDcycHg7XHJcblx0XHRcdG1hcmdpbi10b3A6IDE5cHg7XHJcblx0XHRcdHBhdGh7XHJcblx0XHRcdFx0ZmlsbDogIHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHRcdH1cdFx0XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbi8vIC5ib3R0b21CYXJ7XHJcblx0XHJcbi8vIFx0aW9uLXRhYi1iYXIge1xyXG4vLyBcdFx0LS1ib3JkZXI6IDA7XHJcbi8vIFx0XHQtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4vLyBcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG4vLyBcdFx0Ym90dG9tOiAwO1xyXG4vLyBcdFx0bGVmdDowOyByaWdodDogMDtcclxuLy8gXHRcdHdpZHRoOiAxMDAlO1xyXG4vLyBcdFx0JjphZnRlcntcclxuLy8gXHRcdFx0Y29udGVudDogXCIgXCI7XHJcbi8vIFx0XHRcdHdpZHRoOiAxMDAlO1xyXG4vLyBcdFx0XHRib3R0b206IDA7XHJcbi8vIFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbi8vIFx0XHRcdGhlaWdodDogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pO1xyXG4vLyBcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vIFx0XHR9XHJcbi8vIFx0XHRpb24tdGFiLWJ1dHRvbiB7XHJcbi8vIFx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuLy8gXHRcdH1cclxuLy8gXHRcdGlvbi10YWItYnV0dG9uLmNvbW1lbnRzIHtcclxuLy8gXHRcdFx0bWFyZ2luLXJpZ2h0OiAwcHg7XHJcbi8vIFx0XHRcdGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxOHB4O1xyXG4vLyBcdFx0fVxyXG4vLyBcdFx0aW9uLXRhYi1idXR0b24ubm90aWZzIHtcclxuLy8gXHRcdFx0bWFyZ2luLWxlZnQ6IDBweDtcclxuLy8gXHRcdFx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMThweDtcclxuLy8gXHRcdH1cclxuLy8gXHRcdHN2ZyB7ICAgIFxyXG4vLyBcdFx0XHR3aWR0aDogNzJweDtcclxuLy8gXHRcdFx0bWFyZ2luLXRvcDogMTlweDtcclxuLy8gXHRcdFx0cGF0aHtcclxuLy8gXHRcdFx0XHRmaWxsOiAgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuLy8gXHRcdFx0fVx0XHRcclxuLy8gXHRcdH1cclxuLy8gXHR9XHJcbi8vIH1cclxuLm1haW50QnRue1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVJlZ3VsYXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBwYWRkaW5nOiAyMXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGxlZnQ6IDUlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgdG9wOiAyMHB4O1xyXG59XHJcbi5jZW50ZXJJdGVtc3tcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5pb24tc2VsZWN0e1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgcGFkZGluZzozcHggMXB4IDFweCAzNHB4O1xyXG59XHJcbi5pdGVtSW9ue1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIG1hcmdpbi10b3A6MTAlO1xyXG59XHJcbi5sYWJlbElvbntcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgZm9udC1zaXplOjIwcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuXHJcbi5hY3Rpb24tc2hlZXQtYnV0dG9ue1xyXG4gICAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQxNHB4KSB7XHJcbiAgICBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiAxMjdweDtcclxuICAgIH1cclxuICB9XHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gICAgaW9uLXRvb2xiYXJ7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgICAgIGhlaWdodDogNzBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzc1cHgpIHtcclxuICAgIC5pb3MgaW9uLXRvb2xiYXJ7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgICAgIGhlaWdodDogOTVweDtcclxuICAgIH1cclxuICAgIC5pb3MgaW9uLXRpdGxle1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IC0xNXB4O1xyXG4gICAgICAgIGxlZnQ6IC0xNXB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDIxcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgXHJcblxyXG4gIH0iXX0= */";
      /***/
    },

    /***/
    "./src/app/settings/settings.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/settings/settings.page.ts ***!
      \*******************************************/

    /*! exports provided: SettingsPage */

    /***/
    function srcAppSettingsSettingsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SettingsPage", function () {
        return SettingsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_language_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/language.service */
      "./src/app/services/language.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var SettingsPage = /*#__PURE__*/function () {
        function SettingsPage(languageService, router) {
          _classCallCheck(this, SettingsPage);

          this.languageService = languageService;
          this.router = router;
          this.languages = [];
          this.languages = ["English", "Ελληνικά"];
        }

        _createClass(SettingsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "check",
          value: function check() {
            if (this.selectedValueLanguage == "English") {
              this.languageService.setLanguage("en");
            }

            if (this.selectedValueLanguage == "Ελληνικά") {
              this.languageService.setLanguage("gr");
            }
          }
        }, {
          key: "navigateToStartFeedPage",
          value: function navigateToStartFeedPage() {
            this.router.navigate(['routelist']);
          }
        }, {
          key: "navigateToMaintenancePage",
          value: function navigateToMaintenancePage() {
            this.router.navigate(['maintenance']);
          }
        }, {
          key: "navigateToWalletPage",
          value: function navigateToWalletPage() {
            this.router.navigate(['wallet']);
          }
        }, {
          key: "navigateToRouteHistoryPage",
          value: function navigateToRouteHistoryPage() {
            this.router.navigate(['routehistory']);
          }
        }, {
          key: "navigateToTechHistoryPage",
          value: function navigateToTechHistoryPage() {
            this.router.navigate(['techhistory']);
          }
        }, {
          key: "navigateToProfilePage",
          value: function navigateToProfilePage() {
            this.router.navigate(['profile']);
          }
        }, {
          key: "navigateToSettingsPage",
          value: function navigateToSettingsPage() {
            this.router.navigate(['settings']);
          }
        }, {
          key: "navigateToNotificationsPage",
          value: function navigateToNotificationsPage() {
            this.router.navigate(['notifications']);
          }
        }]);

        return SettingsPage;
      }();

      SettingsPage.ctorParameters = function () {
        return [{
          type: _services_language_service__WEBPACK_IMPORTED_MODULE_2__["LanguageService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      SettingsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-settings',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./settings.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./settings.page.scss */
        "./src/app/settings/settings.page.scss"))["default"]]
      })], SettingsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=settings-settings-module-es5.js.map