(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["passrec-passrec-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/passrec/passrec.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/passrec/passrec.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content>\n  <div class=\"registration-page\">\n  <ion-toolbar color=\"blue\">\n    <ion-title class=\"toolbar-title center\">{{'passrec-recovery.title' | translate}}</ion-title>\n    <ion-buttons>\n    <ion-button routerLink=\"/forgotpass\" class=\"back-btn\"><ion-icon class=\"arrow-back\" slot=\"start\" name=\"arrow-back\"></ion-icon></ion-button>\n  </ion-buttons>\n  </ion-toolbar>\n\n\n  <div class=\"registration-completed-desc\">\n    <a class=\"reg-title1\">{{'forgot-title.title' | translate}}</a>\n    <a class=\"reg-title2\">{{'passrec-success.title' | translate}}</a>\n    <img src=\"assets/images/pass_recover_check.svg\" alt=\"Registration is Completed\" />\n  </div>\n\n\n\n  <div class=\"email-verification-desc\">\n    <a>{{'passrec-emailsend.title' | translate}}</a>\n    <a>{{'passrec-emailsend2.title' | translate}}</a>\n    <a>{{'passrec-verify.title' | translate}}</a>\n    <a>{{'passrec-trylogin.title' | translate}}</a>\n  </div>\n  <form id=\"form\">\n  <ion-button class=\"btn-send\" expand=\"block\" routerLink=\"/login\" shape=\"round\">{{'passrec-login.title' | translate}}</ion-button>\n</form>\n</div>\n</ion-content>");

/***/ }),

/***/ "./src/app/passrec/passrec-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/passrec/passrec-routing.module.ts ***!
  \***************************************************/
/*! exports provided: PassrecPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassrecPageRoutingModule", function() { return PassrecPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _passrec_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./passrec.page */ "./src/app/passrec/passrec.page.ts");




const routes = [
    {
        path: '',
        component: _passrec_page__WEBPACK_IMPORTED_MODULE_3__["PassrecPage"]
    }
];
let PassrecPageRoutingModule = class PassrecPageRoutingModule {
};
PassrecPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PassrecPageRoutingModule);



/***/ }),

/***/ "./src/app/passrec/passrec.module.ts":
/*!*******************************************!*\
  !*** ./src/app/passrec/passrec.module.ts ***!
  \*******************************************/
/*! exports provided: PassrecPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassrecPageModule", function() { return PassrecPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _passrec_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./passrec-routing.module */ "./src/app/passrec/passrec-routing.module.ts");
/* harmony import */ var _passrec_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./passrec.page */ "./src/app/passrec/passrec.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let PassrecPageModule = class PassrecPageModule {
};
PassrecPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _passrec_routing_module__WEBPACK_IMPORTED_MODULE_5__["PassrecPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_passrec_page__WEBPACK_IMPORTED_MODULE_6__["PassrecPage"]]
    })
], PassrecPageModule);



/***/ }),

/***/ "./src/app/passrec/passrec.page.scss":
/*!*******************************************!*\
  !*** ./src/app/passrec/passrec.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".registration-page {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -6px;\n}\n\n.toolbar-title {\n  font-size: 18px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 2px;\n}\n\nimg {\n  position: relative;\n  bottom: 80px;\n}\n\n.registration-completed-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n.email-verification-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteRegular;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 70px;\n}\n\n.reg-title1 {\n  font-family: monteBold;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 155px;\n}\n\n.reg-title2 {\n  font-family: monteBold;\n  font-size: 28px;\n  color: var(--ion-color-ibus-lightgreen);\n  position: relative;\n  top: 155px;\n}\n\n.steps-desc {\n  position: relative;\n  top: 10px;\n}\n\n.step-1 {\n  position: relative;\n  left: 30px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-2 {\n  position: relative;\n  left: 70px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-3 {\n  position: relative;\n  left: 120px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  position: relative;\n  top: 110px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFzc3JlYy9wYXNzcmVjLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFHSjs7QUFEQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtBQUlKOztBQUZBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFLSjs7QUFIQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUFNSjs7QUFKQTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBT0o7O0FBTEE7RUFDSSxzQkFBQTtFQUNBLGVBQUE7RUFDQSx1Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQVFKOztBQU5BO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0FBU0o7O0FBUEE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQVVKOztBQVJBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFXSjs7QUFUQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBWUo7O0FBVkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFhSjs7QUFWQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBYUoiLCJmaWxlIjoic3JjL2FwcC9wYXNzcmVjL3Bhc3NyZWMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlZ2lzdHJhdGlvbi1wYWdle1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICB0b3A6NTVweDtcclxuICAgIHotaW5kZXg6MjAwO1xyXG59XHJcblxyXG4uYmFjay1idG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDMwcHg7XHJcbiAgICBsZWZ0OjIwcHg7XHJcbiAgICB3aWR0aDo1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbi5hcnJvdy1iYWNre1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMTBweDtcclxuICAgIHRvcDogLTZweDtcclxufVxyXG4udG9vbGJhci10aXRsZXtcclxuICAgIGZvbnQtc2l6ZToxOHB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7ICAgXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDJweDtcclxufVxyXG5pbWd7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3R0b206IDgwcHg7XHJcbn1cclxuLnJlZ2lzdHJhdGlvbi1jb21wbGV0ZWQtZGVzY3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uZW1haWwtdmVyaWZpY2F0aW9uLWRlc2N7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTptb250ZVJlZ3VsYXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogNzBweDtcclxufVxyXG4ucmVnLXRpdGxlMXtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAxNTVweDtcclxufVxyXG4ucmVnLXRpdGxlMntcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6IDI4cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtbGlnaHRncmVlbik7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDE1NXB4O1xyXG59XHJcbi5zdGVwcy1kZXNje1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICB0b3A6MTBweDtcclxufVxyXG4uc3RlcC0xe1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjMwcHg7XHJcbiAgICBmb250LXNpemU6MTFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uc3RlcC0ye1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjcwcHg7XHJcbiAgICBmb250LXNpemU6MTFweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uc3RlcC0ze1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OjEyMHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLmJ0bi1zZW5ke1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgaGVpZ2h0OjE4O1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6MTNweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjNEU5OEZGIDkwJSwjNEU5OEZGIDEwJSk7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHRvcDoxMTBweDtcclxufVxyXG5cclxuI2Zvcm0ge1xyXG4gICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6MzBweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6MzBweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/passrec/passrec.page.ts":
/*!*****************************************!*\
  !*** ./src/app/passrec/passrec.page.ts ***!
  \*****************************************/
/*! exports provided: PassrecPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassrecPage", function() { return PassrecPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let PassrecPage = class PassrecPage {
    constructor() { }
    ngOnInit() {
    }
};
PassrecPage.ctorParameters = () => [];
PassrecPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-passrec',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./passrec.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/passrec/passrec.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./passrec.page.scss */ "./src/app/passrec/passrec.page.scss")).default]
    })
], PassrecPage);



/***/ })

}]);
//# sourceMappingURL=passrec-passrec-module-es2015.js.map