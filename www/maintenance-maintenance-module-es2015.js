(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["maintenance-maintenance-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/maintenance/maintenance.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/maintenance/maintenance.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"popUp\" *ngIf=\"showInformation\">\n  <div id=\"hidenItem\">\n    <ion-buttons class=\"closeBtn\" (click)=\"saveTodos()\">\n      <ion-icon side=\"end\" class=\"closeBtnIcon\" name=\"exit\" ></ion-icon>\n    </ion-buttons>\n    <ion-label class=\"pDetails\">\n      <div class=\"centerItems pTitle\">{{'maintenance-details.title' | translate}}</div>\n    </ion-label>\n    <div class=\" pDetails ion-padding\">\n      <ion-label class=\"centerItems classLabel\">{{'maintenance-daysOut.title' | translate}}</ion-label>\n      <ion-input name=\"text\" class=\"centerItems\" placeholder=\"Insert Value\"></ion-input>\n    </div>\n    <div class=\"pDetails ion-padding\">\n      <ion-label  class=\"centerItems classLabel\">{{'maintenance-daysIn.title' | translate}}</ion-label>\n      <ion-input name=\"text\" class=\"centerItems\" placeholder=\"Insert value\"></ion-input>\n    </div>\n  </div>\n\n  <div  (click)=\"navigateToMaintenancePage()\" class=\"center\">\n    <button (click)=\"saveTodos()\" ion-button class=\"sendBtn\">{{'maintenance-send.title' | translate}}</button>\n </div>\n\n</div>\n<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home/feed\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/home/notifications\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon><ion-badge></ion-badge></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'settings-maintenance.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n<ion-content>\n  <div class=\"center\">\n     <button (click)=\"callService()\" ion-button class=\"maintBtn\">{{'maintenance-service.title' | translate}}</button>\n     <button class=\"maintBtn\" (click)=\"callWash()\">{{'maintenance-carWash.title' | translate}}</button>\n     <button class=\"maintBtn\" (click)=\"callAcciddent()\">{{'maintenance-accident.title' | translate}}</button>\n     <button class=\"maintBtn\" (click)=\"callCompany()\">{{'maintenance-company.title' | translate}}</button>\n  </div> \n</ion-content>\n\n<div  class=\"bottomBar\">\n  <ion-buttons class=\"addBtn\" (click)=\"navigateToSettingsPage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <button (click)=\"navigateToSettingsPage()\" class=\"adBtn2\">\n      <ion-icon name=\"arrow-back\"></ion-icon>\n    </button>\n  </ion-buttons>\n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n        <ion-tab-button (click)=\"navigateToroutelist()\" tab=\"routelist\">\n          <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button (click)=\"navigateToWalletPage()\" tab=\"wallet\" class=\"comments\">\n          <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n        </ion-tab-button>\n    \n        <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n    \n        <ion-tab-button (click)=\"navigateToRouteHistoryPage()\" tab=\"route-history\" class=\"notifs\">\n          <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button (click)=\"navigateToSettingsPage()\" tab=\"settings\">\n          <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n        </ion-tab-button>\n      </ion-tab-bar>\n    </div>\n</div>");

/***/ }),

/***/ "./src/app/maintenance/maintenance-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/maintenance/maintenance-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: MaintenancePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenancePageRoutingModule", function() { return MaintenancePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _maintenance_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./maintenance.page */ "./src/app/maintenance/maintenance.page.ts");




const routes = [
    {
        path: '',
        component: _maintenance_page__WEBPACK_IMPORTED_MODULE_3__["MaintenancePage"]
    }
];
let MaintenancePageRoutingModule = class MaintenancePageRoutingModule {
};
MaintenancePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MaintenancePageRoutingModule);



/***/ }),

/***/ "./src/app/maintenance/maintenance.module.ts":
/*!***************************************************!*\
  !*** ./src/app/maintenance/maintenance.module.ts ***!
  \***************************************************/
/*! exports provided: MaintenancePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenancePageModule", function() { return MaintenancePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _maintenance_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./maintenance-routing.module */ "./src/app/maintenance/maintenance-routing.module.ts");
/* harmony import */ var _maintenance_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./maintenance.page */ "./src/app/maintenance/maintenance.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let MaintenancePageModule = class MaintenancePageModule {
};
MaintenancePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _maintenance_routing_module__WEBPACK_IMPORTED_MODULE_5__["MaintenancePageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_maintenance_page__WEBPACK_IMPORTED_MODULE_6__["MaintenancePage"]]
    })
], MaintenancePageModule);



/***/ }),

/***/ "./src/app/maintenance/maintenance.page.scss":
/*!***************************************************!*\
  !*** ./src/app/maintenance/maintenance.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\n.bottomBar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.bottomBar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 60px;\n  height: 60px;\n  position: relative;\n  top: -9px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.bottomBar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.bottomBar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.bottomBar ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\n\n.bottomBar ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.bottomBar ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.bottomBar ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.bottomBar ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.bottomBar ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.bottomBar ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.maintBtn {\n  width: 300px;\n  height: 70px;\n  border-radius: 23px;\n  background-color: var(--ion-color-ibus-theblue);\n  font-family: monteSBold;\n  font-size: 18px;\n  color: white;\n  padding: 21px;\n  width: 90%;\n  left: 5%;\n  color: white;\n  margin-top: 10%;\n}\n\n.sendBtn {\n  width: 300px;\n  height: 70px;\n  border-radius: 23px;\n  background-color: white;\n  font-family: monteSBold;\n  font-size: 18px;\n  color: var(--ion-color-ibus-darkblue);\n  padding: 21px;\n  width: 90%;\n  left: 5%;\n  margin-top: 10%;\n  position: absolute;\n  top: 470px;\n}\n\n.returnBtn {\n  height: 70px;\n  border-radius: 63px;\n  background-color: var(--ion-color-ibus-darkblue);\n  font-family: monteSBold;\n  font-size: 18px;\n  color: white;\n  padding: 21px;\n  width: 70px;\n  left: 41%;\n  margin-top: 10%;\n  position: absolute;\n  top: 550px;\n}\n\n.closeBtn {\n  position: relative;\n  top: -90px;\n  left: 360px;\n}\n\n.popUp {\n  background-color: var(--ion-color-ibus-theblue);\n  height: 100vh;\n  width: 100vw;\n  position: relative;\n  color: white;\n  font-size: 25px;\n  z-index: 9999;\n}\n\nion-input {\n  background-color: white;\n  color: var(--ion-color-ibus-darkblue);\n  border-radius: 21px;\n  width: 80%;\n  margin-top: 5%;\n  text-align: center;\n}\n\n.classLabel {\n  font-size: 20px;\n  font-family: monteRegular;\n}\n\n.centerItems {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n}\n\n.pTitle {\n  position: relative;\n  top: -30px;\n  font-family: monteSBold;\n}\n\n#hidenItem {\n  margin-top: 30%;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbnRlbmFuY2UvbWFpbnRlbmFuY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNENBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtBQUVKOztBQUFBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBR0o7O0FBQ0M7RUFDTyx1QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLDBDQUFBO0VBQTRDLGlCQUFBO0FBR3BEOztBQUZRO0VBQ0ksa0JBQUE7RUFDQSwrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBSVo7O0FBRlE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBSVo7O0FBRlE7RUFDQSx5QkFBQTtBQUlSOztBQURDO0VBQ0MsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUFRLFFBQUE7RUFDUixXQUFBO0FBSUY7O0FBSEU7RUFDQyxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esa0JBQUE7QUFLSDs7QUFIRTtFQUNDLG9DQUFBO0FBS0g7O0FBSEU7RUFDQyxpQkFBQTtFQUNBLDZCQUFBO0FBS0g7O0FBSEU7RUFDQyxnQkFBQTtFQUNBLDRCQUFBO0FBS0g7O0FBSEU7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7QUFLSDs7QUFKRztFQUNDLDRCQUFBO0FBTUo7O0FBREE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7QUFJSjs7QUFGQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUtKOztBQUhBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBTUo7O0FBSkE7RUFDSSxtQkFBQTtBQU9KOztBQUxBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBUUo7O0FBTkE7RUFDSSxvQkFBQTtBQVNKOztBQVBBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBVUo7O0FBUkE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBV0o7O0FBVEE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxxQ0FBQTtBQVlKOztBQVZBO0VBQ0kscUNBQUE7QUFhSjs7QUFYQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBY0o7O0FBWEE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsK0NBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFjSjs7QUFaQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLHFDQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQWVKOztBQWJBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0RBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQWdCSjs7QUFkQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFpQko7O0FBZkE7RUFDSSwrQ0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7QUFrQko7O0FBaEJBO0VBQ0ksdUJBQUE7RUFDQSxxQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQW1CSjs7QUFqQkE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUFvQko7O0FBbEJBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtBQXFCSjs7QUFuQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSx1QkFBQTtBQXNCSjs7QUFuQkE7RUFDSSxlQUFBO0FBc0JKOztBQWxCQTtFQUNJO0lBQ0kscUJBQUE7SUFDQSxhQUFBO0VBcUJOO0FBQ0Y7O0FBbkJFO0VBQ0U7SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUFxQk47QUFDRjs7QUFsQkU7RUFDRTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQW9CTjs7RUFsQkU7SUFDSSxrQkFBQTtJQUNBLFVBQUE7SUFDQSxXQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7RUFxQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL21haW50ZW5hbmNlL21haW50ZW5hbmNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG59XHJcbi5pb3MgaW9uLXRvb2xiYXJ7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICBoZWlnaHQ6IDEyN3B4O1xyXG59XHJcbi5tZCBpb24tdG9vbGJhcntcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogODdweDtcclxufVxyXG5cclxuLmJvdHRvbUJhcntcclxuXHQuYWRkQnRuIHtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG4gICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgdG9wOiAtOXB4O1xyXG4gICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6bW9udGVTQm9sZDtcclxuICAgICAgICAgICAgZm9udC1zaXplOjExcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgICAgICAgd2lkdGg6MzBweDtcclxuICAgICAgICAgICAgaGVpZ2h0OjMwcHg7XHJcbiAgICAgICAgICAgIHN0cm9rZTogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpb24tYmFkZ2V7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgfVxyXG4gICAgfVxyXG5cdGlvbi10YWItYmFyIHtcclxuXHRcdC0tYm9yZGVyOiAwO1xyXG5cdFx0LS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdGJvdHRvbTogMDtcclxuXHRcdGxlZnQ6MDsgcmlnaHQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdCY6YWZ0ZXJ7XHJcblx0XHRcdGNvbnRlbnQ6IFwiIFwiO1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHRoZWlnaHQ6IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5jb21tZW50cyB7XHJcblx0XHRcdG1hcmdpbi1yaWdodDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMThweDtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uLm5vdGlmcyB7XHJcblx0XHRcdG1hcmdpbi1sZWZ0OiAwcHg7XHJcblx0XHRcdGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRzdmcgeyAgICBcclxuXHRcdFx0d2lkdGg6IDcycHg7XHJcblx0XHRcdG1hcmdpbi10b3A6IDE5cHg7XHJcblx0XHRcdHBhdGh7XHJcblx0XHRcdFx0ZmlsbDogIHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHRcdH1cdFx0XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbi5pb3MgI3Byb2ZpbGV7XHJcbiAgICB3aWR0aDogNjBweDtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMjdweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDMxcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9wcm9maWxlLmpwZyk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDYwcHggODBweDtcclxufVxyXG4uaW9zIGlvbi10aXRsZXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogLTE1cHg7XHJcbiAgICBsZWZ0OiAtMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxufVxyXG4ubWQgI3Byb2ZpbGV7XHJcbiAgICB3aWR0aDogNjBweDtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMjdweDtcclxuICAgIG1hcmdpbi10b3A6IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2ltYWdlcy9wcm9maWxlLmpwZyk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDYwcHggODBweDtcclxufVxyXG4uaW9zIC5uYXYtYnV0dG9uc3tcclxuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuLm1kIGlvbi10aXRsZXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMzBweDtcclxuICAgIGxlZnQ6IDgwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kIC5uYXYtYnV0dG9uc3tcclxuICAgIG1hcmdpbi1ib3R0b206IC0xNXB4O1xyXG59XHJcbi5ub3RCdG57XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogNDdweDtcclxuICAgIGhlaWdodDogNjJweDtcclxufVxyXG4ubWVudUJ0bntcclxuICAgIGZvbnQtc2l6ZTogMTAwcHg7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ubWVudVR4dHtcclxuICAgIGZvbnQtc2l6ZToxOHB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVSZWd1bGFyO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5pY29uU3R5bGluZ3tcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4ubWVudVRvb2xiYXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA4MHB4O1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG59XHJcblxyXG4ubWFpbnRCdG57XHJcbiAgICB3aWR0aDozMDBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6MjNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgZm9udC1zaXplOjE4cHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiAyMXB4O1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGxlZnQ6IDUlO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMCU7XHJcbn1cclxuLnNlbmRCdG57XHJcbiAgICB3aWR0aDozMDBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6MjNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XHJcbiAgICBmb250LWZhbWlseTptb250ZVNCb2xkO1xyXG4gICAgZm9udC1zaXplOjE4cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcGFkZGluZzogMjFweDtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBsZWZ0OiA1JTtcclxuICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNDcwcHg7XHJcbn1cclxuLnJldHVybkJ0bntcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDYzcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDIxcHg7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIGxlZnQ6IDQxJTtcclxuICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTUwcHg7XHJcbn1cclxuLmNsb3NlQnRue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtOTBweDtcclxuICAgIGxlZnQ6IDM2MHB4O1xyXG59XHJcbi5wb3BVcHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIHotaW5kZXg6IDk5OTk7XHJcbn1cclxuaW9uLWlucHV0e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBtYXJnaW4tdG9wOjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5jbGFzc0xhYmVse1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlUmVndWxhcjtcclxufVxyXG4uY2VudGVySXRlbXN7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLnBUaXRsZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTMwcHg7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcclxufVxyXG5cclxuI2hpZGVuSXRlbXtcclxuICAgIG1hcmdpbi10b3A6MzAlO1xyXG59XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MTRweCkge1xyXG4gICAgaW9uLXRvb2xiYXJ7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xyXG4gICAgICAgIGhlaWdodDogMTI3cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAgIGlvbi10b29sYmFye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDM3NXB4KSB7XHJcbiAgICAuaW9zIGlvbi10b29sYmFye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDk1cHg7XHJcbiAgICB9XHJcbiAgICAuaW9zIGlvbi10aXRsZXtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAtMTVweDtcclxuICAgICAgICBsZWZ0OiAtMTVweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgIFxyXG5cclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/maintenance/maintenance.page.ts":
/*!*************************************************!*\
  !*** ./src/app/maintenance/maintenance.page.ts ***!
  \*************************************************/
/*! exports provided: MaintenancePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenancePage", function() { return MaintenancePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let MaintenancePage = class MaintenancePage {
    constructor(router) {
        this.router = router;
        this.number = 123;
    }
    ngOnInit() {
    }
    callWash() {
        window.open(`tel:12345`, '_system');
        // window.plugins.CallNumber.callNumber(function(){
        //  //success logic goes here
        // }, function(){
        //  //error logic goes here
        // }, number) 
    }
    callAccident() {
        window.open(`tel:123456`, '_system');
    }
    callService() {
        window.open(`tel:1234567`, '_system');
    }
    callCompany() {
        window.open(`tel:12345678`, '_system');
    }
    navigateToroutelist() {
        this.router.navigate(["home2/routelist"]);
    }
    navigateToWalletPage() {
        this.router.navigate(["home2/wallet"]);
    }
    navigateToSettingsPage() {
        this.router.navigate(["home2/settigns"]);
    }
};
MaintenancePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
MaintenancePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-maintenance',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./maintenance.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/maintenance/maintenance.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./maintenance.page.scss */ "./src/app/maintenance/maintenance.page.scss")).default]
    })
], MaintenancePage);



/***/ })

}]);
//# sourceMappingURL=maintenance-maintenance-module-es2015.js.map