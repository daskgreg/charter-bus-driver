(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-map-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html":
    /*!*************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html ***!
      \*************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppMapMapPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"background\">\n <div #map id=\"map\"></div>\n<!--   <div >\n        <ion-button class=\"btn-primary\" ion-button (click)=\"startWithoutApi()\" style=\"background-color:green; display: inline-block;width:32%;border-right:0;border-left:0\">\n            Start\n          </ion-button>\n        <ion-button class=\"btn-primary\" ion-button (click)=\"navigation()\" style=\"display: inline-block;width:32%;\" >\n            Navigate\n         </ion-button>\n      \n      \n     \n      <ion-button class=\"btn-primary\" ion-button (click)=\"endWithoutApi()\" style=\"background-color:red;border-right:0;width:32%; display: inline-block;border-left:0\">\n         End\n      </ion-button>\n         \n    \n</div> -->\n <!-- <ion-button class=\"btn-primary\" ion-button (click)=\"inspect()\" style=\"background-color:red;border-right:0;width:32%; display: inline-block;border-left:0\">\n         Tech Inspect\n      </ion-button> -->\n      <!-- <ion-fab vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button (click)=\"navigateToRouteOnGoPage()\">\n      <ion-icon name=arrow-back></ion-icon>\n    </ion-fab-button>\n     \n  </ion-fab> -->\n</ion-content>\n\n\n\n<!--  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n      <ion-tab-button tab=\"routelist\" (click)=\"startWithoutApi()\">\n        Start\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"endWithoutApi()\">\n        End\n      </ion-tab-button>\n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"inspect()\">\n        Tech Inspect\n      </ion-tab-button>\n    </ion-tab-bar> -->\n\n\n \n<div class=\"maptab\">\n  <ion-fab *ngIf=\"hideBackBtn\" class=\"backBtn\"  vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button class=\"backBtnStyle\" (click)=\"navigateToRouteOnGoPage()\">\n      <ion-icon name=arrow-back></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n  <ion-fab *ngIf=\"hideEndBtn\" class=\"endBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button class=\"endBtnStyle\" (click)=\"endWithoutApi()\">\n      <button class=\"endBtnFont\" >{{'map-end.title' | translate}}</button>\n    </ion-fab-button>\n  </ion-fab>\n  <ion-fab *ngIf=\"hideCustomBtn\" class=\"customBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button class=\"customBtnStyle\" (click)=\"startWithoutApi()\">\n      <button class=\"customBtnFont\" >{{'map-start.title' | translate}}</button>\n    </ion-fab-button>\n  </ion-fab>\n  <ion-fab vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button class=\"menuBtn\" (click)=\"showBtn()\" >\n      <ion-icon name=\"apps\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</div>\n\n\n\n<!-- <ion-content padding class=\"background\">\n INSERT MAP HERE\n <div #map id=\"map\"></div>\n\n</ion-content>\n<ion-tabs>  \n\n  <ion-fab vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <ion-fab-button (click)=\"navigateToRouteOnGoPage()\">\n      <ion-icon name=arrow-back></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-tabs> -->\n";
      /***/
    },

    /***/
    "./src/app/map/map-routing.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/map/map-routing.module.ts ***!
      \*******************************************/

    /*! exports provided: MapPageRoutingModule */

    /***/
    function srcAppMapMapRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MapPageRoutingModule", function () {
        return MapPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _map_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./map.page */
      "./src/app/map/map.page.ts");

      var routes = [{
        path: '',
        component: _map_page__WEBPACK_IMPORTED_MODULE_3__["MapPage"]
      }];

      var MapPageRoutingModule = function MapPageRoutingModule() {
        _classCallCheck(this, MapPageRoutingModule);
      };

      MapPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MapPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/map/map.module.ts":
    /*!***********************************!*\
      !*** ./src/app/map/map.module.ts ***!
      \***********************************/

    /*! exports provided: MapPageModule */

    /***/
    function srcAppMapMapModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MapPageModule", function () {
        return MapPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _map_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./map-routing.module */
      "./src/app/map/map-routing.module.ts");
      /* harmony import */


      var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./map.page */
      "./src/app/map/map.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var MapPageModule = function MapPageModule() {
        _classCallCheck(this, MapPageModule);
      };

      MapPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _map_routing_module__WEBPACK_IMPORTED_MODULE_5__["MapPageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]],
        declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]]
      })], MapPageModule);
      /***/
    },

    /***/
    "./src/app/map/map.page.scss":
    /*!***********************************!*\
      !*** ./src/app/map/map.page.scss ***!
      \***********************************/

    /*! exports provided: default */

    /***/
    function srcAppMapMapPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content.background {\n  --ion-background-color: var( --ion-color-ibus-theblue);\n}\n\n#map {\n  width: 100%;\n  height: 87%;\n  padding-left: 12px;\n  padding-top: 22px;\n  padding-right: 12px;\n  border-top-right-radius: 60px;\n  border-top-left-radius: 60px;\n  border-bottom-left-radius: 60px;\n  border-bottom-right-radius: 60px;\n}\n\n.maptab {\n  text-align: center;\n  justify-content: center;\n  width: 100%;\n  display: flex;\n}\n\n.maptab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.maptab ion-fab ion-fab-button {\n  --box-shadow: none;\n}\n\n.maptab ion-fab-button {\n  width: 70px;\n  height: 70px;\n}\n\n.maptab ion-icon {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.maptab button {\n  background-color: transparent;\n}\n\n.maptab .menuBtn {\n  --background:white;\n}\n\n.maptab .endBtn {\n  position: absolute;\n  bottom: 1.2%;\n  margin: auto;\n  left: 69%;\n}\n\n.maptab .endBtn .endBtnStyle {\n  border: 2px solid var(--ion-color-ibus-red);\n  border-radius: 40px;\n  --background: white;\n}\n\n.maptab .endBtn .endBtnFont {\n  font-size: 15px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-red);\n}\n\n.maptab .backBtn {\n  position: absolute;\n  bottom: 1.2%;\n  margin: auto;\n  left: 17%;\n}\n\n.maptab .backBtn .backBtnStyle {\n  border: 2px solid var(--ion-color-ibus-darkblue);\n  border-radius: 40px;\n  --background:white;\n}\n\n.maptab .customBtn {\n  position: absolute;\n  bottom: 13%;\n  margin: auto;\n  left: 43%;\n}\n\n.maptab .customBtn .customBtnStyle {\n  border: 2px solid var(--ion-color-ibus-classicgreen);\n  border-radius: 40px;\n  --background: white;\n}\n\n.maptab .customBtn .customBtnFont {\n  font-size: 15px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-classicgreen);\n}\n\n.maptab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\n\n.maptab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.maptab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.maptab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.maptab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.maptab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.maptab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL21hcC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzREFBQTtBQUNKOztBQUNBO0VBVUcsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsK0JBQUE7RUFDQSxnQ0FBQTtBQVBIOztBQVNBO0VBQ0Msa0JBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FBTkQ7O0FBT0M7RUFDQywwQ0FBQTtFQUE0QyxpQkFBQTtBQUo5Qzs7QUFLRTtFQUNDLGtCQUFBO0FBSEg7O0FBTUk7RUFDRixXQUFBO0VBQ0EsWUFBQTtBQUpGOztBQU1JO0VBQ0kscUNBQUE7QUFKUjs7QUFNQztFQUNDLDZCQUFBO0FBSkY7O0FBTUM7RUFDQyxrQkFBQTtBQUpGOztBQU1DO0VBQ0Msa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7QUFKRjs7QUFLRTtFQUNDLDJDQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQUhIOztBQUtFO0VBQ0MsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7QUFISDs7QUFNQztFQUNDLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FBSkY7O0FBS0U7RUFDQyxnREFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFISDs7QUFNQztFQUNDLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FBSkY7O0FBS0U7RUFDQyxvREFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFISDs7QUFLRTtFQUNDLGVBQUE7RUFDQSx1QkFBQTtFQUNBLHlDQUFBO0FBSEg7O0FBT0M7RUFDQyxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQVEsUUFBQTtFQUNSLFdBQUE7QUFKRjs7QUFLRTtFQUNDLFlBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtBQUhIOztBQUtFO0VBQ0Msb0NBQUE7QUFISDs7QUFLRTtFQUNDLGlCQUFBO0VBQ0EsNkJBQUE7QUFISDs7QUFLRTtFQUNDLGdCQUFBO0VBQ0EsNEJBQUE7QUFISDs7QUFLRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQUhIOztBQUlHO0VBQ0MsNEJBQUE7QUFGSjs7QUFRQTtFQUNDLHFDQUFBO0FBTEQiLCJmaWxlIjoic3JjL2FwcC9tYXAvbWFwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50LmJhY2tncm91bmR7XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoIC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbn1cclxuI21hcHtcclxuIFxyXG5cdFx0XHQvLyB3aWR0aDogMTAwJTtcclxuXHRcdFx0Ly8gIGhlaWdodDogODUlO1xyXG5cdFx0XHQvLyAgb3BhY2l0eTogMDtcclxuXHRcdFx0Ly8gIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcclxuXHRcdFx0Ly8gIGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0XHQvLyAgJi5zaG93LW1hcHtcclxuXHRcdFx0Ly8gICAgb3BhY2l0eTogMTtcclxuXHRcdFx0Ly8gIH1cclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdGhlaWdodDogODclO1xyXG5cdFx0XHRwYWRkaW5nLWxlZnQ6IDEycHg7XHJcblx0XHRcdHBhZGRpbmctdG9wOiAyMnB4O1xyXG5cdFx0XHRwYWRkaW5nLXJpZ2h0OiAxMnB4O1xyXG5cdFx0XHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNjBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNjBweDtcclxuXHRcdFx0Ym9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNjBweDtcclxuXHRcdFx0Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDYwcHg7XHJcbiB9XHJcbi5tYXB0YWJ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0aW9uLWZhYiB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xyXG5cdFx0aW9uLWZhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJveC1zaGFkb3c6IG5vbmU7XHJcblx0XHR9XHJcbiAgICB9XHJcbiAgICBpb24tZmFiLWJ1dHRvbntcclxuXHRcdHdpZHRoOiA3MHB4O1xyXG5cdFx0aGVpZ2h0OiA3MHB4O1xyXG5cdH1cclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuXHR9XHJcblx0YnV0dG9ue1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtcclxuXHR9XHJcblx0Lm1lbnVCdG57XHJcblx0XHQtLWJhY2tncm91bmQ6d2hpdGU7XHJcblx0fVxyXG5cdC5lbmRCdG57XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRib3R0b206IDEuMiU7XHJcblx0XHRtYXJnaW46IGF1dG87XHJcblx0XHRsZWZ0OiA2OSU7XHJcblx0XHQuZW5kQnRuU3R5bGV7XHJcblx0XHRcdGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1pYnVzLXJlZCk7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDQwcHg7XHJcblx0XHRcdC0tYmFja2dyb3VuZDogd2hpdGU7XHJcblx0XHR9XHJcblx0XHQuZW5kQnRuRm9udHtcclxuXHRcdFx0Zm9udC1zaXplOjE1cHg7XHJcblx0XHRcdGZvbnQtZmFtaWx5Om1vbnRlU0JvbGQ7XHJcblx0XHRcdGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLXJlZCk7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5iYWNrQnRue1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Ym90dG9tOiAxLjIlO1xyXG5cdFx0bWFyZ2luOiBhdXRvO1xyXG5cdFx0bGVmdDogMTclO1xyXG5cdFx0LmJhY2tCdG5TdHlsZXtcclxuXHRcdFx0Ym9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG5cdFx0XHRib3JkZXItcmFkaXVzOiA0MHB4O1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6d2hpdGU7XHJcblx0XHR9XHJcblx0fVxyXG5cdC5jdXN0b21CdG57XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRib3R0b206IDEzJTtcclxuXHRcdG1hcmdpbjogYXV0bztcclxuXHRcdGxlZnQ6IDQzJTtcclxuXHRcdC5jdXN0b21CdG5TdHlsZXtcclxuXHRcdFx0Ym9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLWlidXMtY2xhc3NpY2dyZWVuKTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogNDBweDtcclxuXHRcdFx0LS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxuXHRcdH1cclxuXHRcdC5jdXN0b21CdG5Gb250e1xyXG5cdFx0XHRmb250LXNpemU6MTVweDtcclxuXHRcdFx0Zm9udC1mYW1pbHk6bW9udGVTQm9sZDtcclxuXHRcdFx0Y29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtY2xhc3NpY2dyZWVuKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdH1cclxuXHRpb24tdGFiLWJhciB7XHJcblx0XHQtLWJvcmRlcjogMDtcclxuXHRcdC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHRsZWZ0OjA7IHJpZ2h0OiAwO1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHQmOmFmdGVye1xyXG5cdFx0XHRjb250ZW50OiBcIiBcIjtcclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdGJvdHRvbTogMDtcclxuXHRcdFx0YmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuXHRcdFx0aGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uIHtcclxuXHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24uY29tbWVudHMge1xyXG5cdFx0XHRtYXJnaW4tcmlnaHQ6IDBweDtcclxuXHRcdFx0Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5ub3RpZnMge1xyXG5cdFx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xyXG5cdFx0fVxyXG5cdFx0c3ZnIHsgICAgXHJcblx0XHRcdHdpZHRoOiA3MnB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxOXB4O1xyXG5cdFx0XHRwYXRoe1xyXG5cdFx0XHRcdGZpbGw6ICB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHR9XHRcdFxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLmljb25TdHlsaW5ne1xyXG5cdGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG5cclxuXHJcbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/map/map.page.ts":
    /*!*********************************!*\
      !*** ./src/app/map/map.page.ts ***!
      \*********************************/

    /*! exports provided: MapPage */

    /***/
    function srcAppMapMapPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MapPage", function () {
        return MapPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_native_background_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/background-geolocation/ngx */
      "./node_modules/@ionic-native/background-geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/http/ngx */
      "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var Geolocation = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"].Geolocation;

      var MapPage = /*#__PURE__*/function () {
        function MapPage(platform, loading, nativeHttp, activatedRoute, http, backgroundGeolocation, alertCtrl, loadingCtrl, router) {
          var _this = this;

          _classCallCheck(this, MapPage);

          this.platform = platform;
          this.loading = loading;
          this.nativeHttp = nativeHttp;
          this.activatedRoute = activatedRoute;
          this.http = http;
          this.backgroundGeolocation = backgroundGeolocation;
          this.alertCtrl = alertCtrl;
          this.loadingCtrl = loadingCtrl;
          this.router = router;
          this.markers = [];
          this.directionsService = null;
          this.directionsDisplay = null;
          this.bounds = null;
          this.waypoints = [];
          this.k = 0;
          this.i = 0;
          this.loader = null; //3/11/2017

          this.destLat = [];
          this.navCounter = 0;
          this.chrcode = localStorage.getItem('chrcode');
          this.lats = [];
          this.longs = [];
          this.telos = 0;
          this.apostasi = [];
          this.xronos = [];
          this.hideBackBtn = false;
          this.hideEndBtn = false;
          this.hideCustomBtn = false;
          this.dataFromPickups = [];
          this.dataFromPickupsFromJSONtoArray = [];
          this.custom = localStorage.getItem("custom");
          this.routeid = 2;
          this.custroutePickups_json = [{
            "route_id": "1",
            "pickup_id": "1",
            "order_id": "1",
            "latitude": "37.865044000000000",
            "longitude": "23.755045000000000",
            "pickup_address": "Glyfada, Attica, Greece",
            "pickup_stop": "1"
          }, {
            "route_id": "1",
            "pickup_id": "2",
            "order_id": "2",
            "latitude": "37.955894000000000",
            "longitude": "23.702099000000000",
            "pickup_address": "Kallithea, Attica, Greece",
            "pickup_stop": "1"
          }, {
            "route_id": "1",
            "pickup_id": "3",
            "order_id": "3",
            "latitude": "37.983810000000000",
            "longitude": "23.727539000000000",
            "pickup_address": "Athens, Greece",
            "pickup_stop": "1"
          }, {
            "route_id": "1",
            "pickup_id": "4",
            "order_id": "4",
            "latitude": "40.736851000000000",
            "longitude": "22.920227000000000",
            "pickup_address": "Thessaloniki, Greece",
            "pickup_stop": "1"
          }, {
            "route_id": "1",
            "pickup_id": "5",
            "order_id": "5",
            "latitude": "41.122440000000000",
            "longitude": "25.406557000000000",
            "pickup_address": "Komotini, East Macedonia and Thrace, Greece",
            "pickup_stop": "1"
          }];
          this.chrbusroutepup_json = [{
            "chrbus_code": "panos",
            "zone_id": "4",
            "pup_code": "chrPup",
            "duration": "1900-01-01 00:00:00.000",
            "time": "1900-01-01 01:00:00.000",
            "vhc_type": "",
            "order": "1",
            "chrbus_name": "panos",
            "zone_name": "panos_chr",
            "pup_desc": "chrPup",
            "cty_code": "1",
            "pup_address": "chrPup 12",
            "latitude": "37.983810",
            "longitude": "23.727539",
            "cty_name": "Athens"
          }, {
            "chrbus_code": "panos",
            "zone_id": "5",
            "pup_code": "chrPup2",
            "duration": "1900-01-01 00:00:00.000",
            "time": "1900-01-01 02:00:00.000",
            "vhc_type": "",
            "order": "2",
            "chrbus_name": "panos",
            "zone_name": "panos_chr2",
            "pup_desc": "chrPup2",
            "cty_code": "3",
            "pup_address": "chrPup 2",
            "latitude": "40.630777",
            "longitude": "22.943101",
            "cty_name": "Thessaloniki"
          }, {
            "chrbus_code": "panos",
            "zone_id": "6",
            "pup_code": "chrPup4",
            "duration": "1900-01-01 00:00:00.000",
            "time": "1900-01-01 03:00:00.000",
            "vhc_type": "",
            "order": "3",
            "chrbus_name": "panos",
            "zone_name": "panos_chr3",
            "pup_desc": "chrPup4",
            "cty_code": "1",
            "pup_address": "chrPup 4",
            "latitude": "38.011600",
            "longitude": "23.717808",
            "cty_name": "Athens"
          }];
          this.dataFromThePickups = [];
          this.dataFromTheRoutes = [];
          this.dataFromTheDriverId = [];
          console.log('%cIN MAP', 'color:red;');
          this.pickUpJsonFromApi = [];
          this.dataFromPickupsFromJSONtoArray = [];
          this.dataFromTheRoutes = this.activatedRoute.snapshot.paramMap.get('rptroutes');
          this.dataFromTheRoutesJSON = JSON.parse(this.dataFromTheRoutes);
          console.log('%c RPT ROUTES', 'color:yellow;');
          console.log(this.dataFromTheRoutesJSON);
          this.dataFromTheDriverId = this.activatedRoute.snapshot.paramMap.get('theidofdriver');
          this.dataFromTheDriverIdJSON = JSON.parse(this.dataFromTheDriverId);
          console.log('%c ID OF DRIVER', 'color:yellow;');
          console.log(this.dataFromTheDriverIdJSON);

          if (platform.is('cordova')) {
            if (this.dataFromTheRoutesJSON.TYPE == 'FIX') {
              var fixNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_route_pup.cfm?' + 'chrbus_code=' + this.dataFromTheRoutesJSON.SERVICECODE + '&userid=dmta', {}, {
                'Content-Type': 'application/json'
              });
              Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["from"])(fixNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () {
                return console.log('fix route');
              })).subscribe(function (data) {
                console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                console.log(data);
                _this.newCustomPickupRoutes = JSON.parse(data.data);
                console.log(_this.newCustomPickupRoutes);
                alert(_this.newCustomPickupRoutes);
                _this.newCustomPickupRoutesJSON = _this.newCustomPickupRoutes;
                console.log(_this.newCustomPickupRoutesJSON);
                _this.newCustomPickupRoutesJSONtoArray = _this.newCustomPickupRoutesJSON;
                console.log(_this.newCustomPickupRoutesJSONtoArray);
                _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = _this.newCustomPickupRoutesJSONtoArray.FIXEDPICKUPS;
                alert(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                console.log('auto thelw');
                console.log(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                console.log(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME);
                _this.startingPoint = _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME;

                for (var i = 0; i < _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS.length; i++) {
                  console.log('%c Data From CUSTOM PICKUP', 'color:red;', _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i]); //  this.lats[i]=parseFloat(this.custroutePickups_json[i].latitude);
                  //  this.longs[i]=parseFloat(this.custroutePickups_json[i].longitude);

                  _this.longs[i] = parseFloat(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LONGITUDE);
                  _this.lats[i] = parseFloat(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LATITUDE);
                  console.log('if = 2');
                  console.log(_this.longs);
                }

                var j;
                _this.i = 0;
                _this.waypoints.length = 0;
                _this.destLat = _this.lats[_this.i];
                console.log(_this.destLat);
                console.log('shit happens');
                _this.destLng = _this.longs[_this.i];

                for (var _j = 0; _j < _this.lats.length; _j++) {
                  var stations = {
                    location: {
                      lat: _this.lats[_j],
                      lng: _this.longs[_j]
                    },
                    stopover: true
                  };

                  _this.waypoints.push(stations);

                  console.log(stations);
                  console.log(_this.waypoints);
                }

                _this.getPosition(); //this.lats=[];
                //this.longs=[];


                console.log(_this.lats, _this.longs);
                _this.fire = "";
                _this.directionsService = new google.maps.DirectionsService();
                _this.directionsDisplay = new google.maps.DirectionsRenderer({
                  suppressMarkers: true,
                  suppressPolylines: true
                });
                _this.bounds = new google.maps.LatLngBounds();
              });
            } else {
              var custNativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?' + 'route_id=' + this.dataFromTheRoutesJSON.SERVICECODE + '&userid=dmta', {}, {
                'Content-Type': 'application/json'
              });
              Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["from"])(custNativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () {
                return console.log('custom route');
              })).subscribe(function (data) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                  var i, j, _j2, stations;

                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                          console.log(data);
                          this.newCustomPickupRoutes = JSON.parse(data.data);
                          console.log(this.newCustomPickupRoutes);
                          this.newCustomPickupRoutesJSON = this.newCustomPickupRoutes;
                          console.log(this.newCustomPickupRoutesJSON);
                          this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutesJSON;
                          console.log(this.newCustomPickupRoutesJSONtoArray);
                          this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = this.newCustomPickupRoutesJSONtoArray.CUSTPICKUPS;
                          console.log('NEWWWW CUSTOOOOOOM PIIIIICKUPPPPP', this.newCustomPickupRoutesJSONtoArray);
                          console.log('NEWWWW CUSTOOOOOOM PIIIIICKUPPPPP', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);

                          for (i = 0; i < this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS.length; i++) {
                            console.log('%c Data From CUSTOM PICKUP', 'color:red;', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i]); //  this.lats[i]=parseFloat(this.custroutePickups_json[i].latitude);
                            //  this.longs[i]=parseFloat(this.custroutePickups_json[i].longitude);

                            this.longs[i] = parseFloat(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LONGITUDE);
                            this.lats[i] = parseFloat(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LATITUDE);
                            console.log('if = 2');
                            console.log('Longitude', this.longs);
                            console.log('Latitude', this.lats);
                          }

                          this.i = 0;
                          this.waypoints.length = 0;
                          this.destLat = this.lats[this.i];
                          console.log(this.destLat);
                          console.log('shit happens');
                          this.destLng = this.longs[this.i];

                          for (_j2 = 0; _j2 < this.lats.length; _j2++) {
                            stations = {
                              location: {
                                lat: this.lats[_j2],
                                lng: this.longs[_j2]
                              },
                              stopover: true
                            };
                            this.waypoints.push(stations);
                            console.log(stations);
                            console.log(this.waypoints);
                          }

                          this.getPosition(); //this.lats=[];
                          //this.longs=[];

                          console.log(this.lats, this.longs);
                          this.fire = "";
                          this.directionsService = new google.maps.DirectionsService();
                          this.directionsDisplay = new google.maps.DirectionsRenderer({
                            suppressMarkers: true,
                            suppressPolylines: true
                          });
                          this.bounds = new google.maps.LatLngBounds();

                        case 25:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, this);
                }));
              });
            }
          } else {
            if (this.dataFromTheRoutesJSON.TYPE == 'FIX') {
              this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_route_pup.cfm?' + 'chrbus_code=' + this.dataFromTheRoutesJSON.SERVICECODE + '&userid=dmta').subscribe(function (data) {
                console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                console.log(data);
                _this.newCustomPickupRoutes = data;
                console.log(_this.newCustomPickupRoutes);
                _this.newCustomPickupRoutesJSON = _this.newCustomPickupRoutes;
                console.log(_this.newCustomPickupRoutesJSON);
                _this.newCustomPickupRoutesJSONtoArray = _this.newCustomPickupRoutesJSON;
                console.log(_this.newCustomPickupRoutesJSONtoArray);
                _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = _this.newCustomPickupRoutesJSONtoArray.FIXEDPICKUPS;
                console.log('auto thelw');
                console.log(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                console.log(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME);
                _this.startingPoint = _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].CTY_NAME;

                for (var i = 0; i < _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS.length; i++) {
                  console.log('%c Data From CUSTOM PICKUP', 'color:red;', _this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i]); //  this.lats[i]=parseFloat(this.custroutePickups_json[i].latitude);
                  //  this.longs[i]=parseFloat(this.custroutePickups_json[i].longitude);

                  _this.longs[i] = parseFloat(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LONGITUDE);
                  _this.lats[i] = parseFloat(_this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LATITUDE);
                  console.log('if = 2');
                  console.log(_this.longs);
                }

                var j;
                _this.i = 0;
                _this.waypoints.length = 0;
                _this.destLat = _this.lats[_this.i];
                console.log(_this.destLat);
                console.log('shit happens');
                _this.destLng = _this.longs[_this.i];

                for (var _j3 = 0; _j3 < _this.lats.length; _j3++) {
                  var stations = {
                    location: {
                      lat: _this.lats[_j3],
                      lng: _this.longs[_j3]
                    },
                    stopover: true
                  };

                  _this.waypoints.push(stations);

                  console.log(stations);
                  console.log(_this.waypoints);
                }

                _this.getPosition(); //this.lats=[];
                //this.longs=[];


                console.log(_this.lats, _this.longs);
                _this.fire = "";
                _this.directionsService = new google.maps.DirectionsService();
                _this.directionsDisplay = new google.maps.DirectionsRenderer({
                  suppressMarkers: true,
                  suppressPolylines: true
                });
                _this.bounds = new google.maps.LatLngBounds();
              });
            } else {
              this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_cust_route_pickups.cfm?' + 'route_id=' + this.dataFromTheRoutesJSON.SERVICECODE + '&userid=dmta').subscribe(function (data) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                  var i, j, _j4, stations;

                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          console.log('%c DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 'color:red;');
                          console.log(data);
                          this.newCustomPickupRoutes = data;
                          console.log(this.newCustomPickupRoutes);
                          this.newCustomPickupRoutesJSON = this.newCustomPickupRoutes;
                          console.log(this.newCustomPickupRoutesJSON);
                          this.newCustomPickupRoutesJSONtoArray = this.newCustomPickupRoutesJSON;
                          console.log(this.newCustomPickupRoutesJSONtoArray);
                          this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS = this.newCustomPickupRoutesJSONtoArray.CUSTPICKUPS;
                          console.log('auto thelw');
                          console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS);
                          console.log(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].PICKUP_ADDRESS);
                          this.startingPoint = this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[0].PICKUP_ADDRESS;

                          for (i = 0; i < this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS.length; i++) {
                            console.log('%c Data From CUSTOM PICKUP', 'color:red;', this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i]); //  this.lats[i]=parseFloat(this.custroutePickups_json[i].latitude);
                            //  this.longs[i]=parseFloat(this.custroutePickups_json[i].longitude);

                            this.longs[i] = parseFloat(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LONGITUDE);
                            this.lats[i] = parseFloat(this.newCustomPickupRoutesJSONtoArrayCUSTOMPICKUPS[i].LATITUDE);
                            console.log('if = 2');
                            console.log(this.longs);
                          }

                          this.i = 0;
                          this.waypoints.length = 0;
                          this.destLat = this.lats[this.i];
                          console.log(this.destLat);
                          console.log('shit happens');
                          this.destLng = this.longs[this.i];

                          for (_j4 = 0; _j4 < this.lats.length; _j4++) {
                            stations = {
                              location: {
                                lat: this.lats[_j4],
                                lng: this.longs[_j4]
                              },
                              stopover: true
                            };
                            this.waypoints.push(stations);
                            console.log(stations);
                            console.log(this.waypoints);
                          }

                          this.getPosition(); //this.lats=[];
                          //this.longs=[];

                          console.log(this.lats, this.longs);
                          this.fire = "";
                          this.directionsService = new google.maps.DirectionsService();
                          this.directionsDisplay = new google.maps.DirectionsRenderer({
                            suppressMarkers: true,
                            suppressPolylines: true
                          });
                          this.bounds = new google.maps.LatLngBounds();

                        case 27:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2, this);
                }));
              });
            }
          } // if (this.dataFromTheRoutesJSON.TYPE == 'CUST') {
          //   console.log('%c Data From CUSTOM PICKUP', 'color:red;', this.dataFromTheRoutesJSON);
          //   console.log('%c Data From CUSTOM PICKUP', 'color:red;', this.dataFromTheRoutesJSON.TYPE);
          // } else {
          //   console.log('TIN PANAGIA DEN THA TIN KSANA ')
          //   for (i = 0; i < this.newCustomPickupRoutesJSON.length; i++) {
          //     console.log(this.newCustomPickupRoutesJSON);
          //     console.log(this.newCustomPickupRoutesJSON[i].LATITUDE);
          //     this.lats[i] = parseFloat(this.newCustomPickupRoutesJSON[i].LATITUDE);
          //     this.longs[i] = parseFloat(this.newCustomPickupRoutesJSON[i].LONGITUDE);
          //     console.log(this.longs);
          //     console.log(this.lats);
          //   }
          // }

        }

        _createClass(MapPage, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {}
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3);
            }));
          }
        }, {
          key: "getPosition",
          value: function getPosition() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var response;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      console.log('before get position');
                      console.log('after get position');
                      _context4.next = 4;
                      return Geolocation.getCurrentPosition({
                        enableHighAccuracy: true
                      }).then(function (res) {
                        console.log(res);

                        _this2.loadMap(res);
                      })["catch"](function (error) {
                        console.log('error could not ');
                        console.log(error);
                      });

                    case 4:
                      response = _context4.sent;

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4);
            }));
          } // ngOnInit() {
          // }

        }, {
          key: "loadMap",
          value: function loadMap(res) {
            var _this3 = this;

            console.log('%c LOAD MAP', 'color:red;');
            var latLng = new google.maps.LatLng(41.1214145, 25.3878458);
            console.log('%c LOAD MAP', 'color:red;'); //this.latitude = res.coords.latitude;
            //this.longitude = res.coords.longitude;

            console.log('%c LOAD MAP', 'color:red;');
            this.latitude = 41.1214145;
            this.longitude = 25.3878458;
            console.log('%c LOAD MAP', 'color:red;');
            var mapEle = document.getElementById('map');
            var panelEle = document.getElementById('panel');
            this.myLatLng = {
              lat: this.latitude,
              lng: this.longitude
            };
            console.log('%c LOAD MAP', 'color:red;');
            var mapOptions = {
              center: this.myLatLng,
              zoom: 5,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            this.infowindow = new google.maps.InfoWindow();
            this.directionsDisplay.setMap(this.map); // this.directionsDisplay.setPanel(panelEle);

            google.maps.event.addListenerOnce(this.map, 'idle', function () {
              mapEle.classList.add('show-map');

              _this3.calculateRoute();
            });
          }
        }, {
          key: "calculateRoute",
          value: function calculateRoute() {
            var _this4 = this;

            var stepDisplay = new google.maps.InfoWindow();
            this.bounds.extend(this.myLatLng);
            this.waypoints.forEach(function (waypoint) {
              var point = new google.maps.LatLng(waypoint.location.lat, waypoint.location.lng);

              _this4.bounds.extend(point);
            });
            this.map.fitBounds(this.bounds);
            this.directionsService.route({
              origin: new google.maps.LatLng(this.myLatLng.lat, this.myLatLng.lng),
              destination: new google.maps.LatLng(this.destLat, this.destLng),
              waypoints: this.waypoints,
              optimizeWaypoints: true,
              //suppressMarkers: true,
              travelMode: google.maps.TravelMode.DRIVING,
              avoidTolls: true
            }, function (response, status) {
              if (status === google.maps.DirectionsStatus.OK) {
                console.log(response);

                _this4.directionsDisplay.setDirections(response);

                _this4.directionsDisplay.setMap(_this4.map);

                var polylineOptions = {
                  strokeColor: '#C83939',
                  strokeOpacity: 1,
                  strokeWeight: 4
                };
                var polylines = [];

                for (var i = 0; i < polylines.length; i++) {
                  polylines[i].setMap(null);
                }

                _this4.startLocation = new Object();
                _this4.endLocation = new Object();
                _this4.waypointLocations = []; // Display start and end markers for the route.

                var legs = response.routes[0].legs;

                for (i = 0; i < legs.length; i++) {
                  var steps = legs[i].steps;

                  for (_this4.r1 = 0; _this4.r1 < steps.length; _this4.r1++) {
                    var nextSegment = steps[_this4.r1].path;
                    var stepPolyline = new google.maps.Polyline(polylineOptions);

                    for (_this4.r2 = 0; _this4.r2 < nextSegment.length; _this4.r2++) {
                      stepPolyline.getPath().push(nextSegment[_this4.r2]);

                      _this4.bounds.extend(nextSegment[_this4.r2]);
                    }

                    google.maps.event.addListener(stepPolyline, 'mouseover', function (evt) {
                      console.log(evt);
                      console.log("route mouse over event @" + evt.latLng.toUrlValue(6));
                      console.log(legs[i].distance, legs[i].duration);
                    });
                    polylines.push(stepPolyline);
                    stepPolyline.setMap(_this4.map); // route click listeners, different one on each step

                    google.maps.event.addListener(stepPolyline, 'click', function (evt) {
                      console.log(legs[i].distance, legs[i].duration);
                      var apostasi = google.maps.geometry.spherical.computeLength(stepPolyline.getPath().getArray());
                      console.log(apostasi);
                      stepDisplay.setContent("you clicked on the route<br>" + evt.latLng.toUrlValue(6));
                      stepDisplay.setPosition(evt.latLng);
                      stepDisplay.open(this.map);
                    });
                  }

                  if (i == 0) {
                    _this4.startLocation.latlng = legs[i].start_location;
                    _this4.startLocation.address = legs[i].start_address;
                    var startDistance = legs[i].distance;
                    var startDuracion = legs[i].duration; // createMarker(legs[i].start_location, "start", legs[i].start_address, "green");
                  }

                  if (i != 0 && i != legs.length - 1) {
                    _this4.waypoint = {};
                    _this4.waypoint.latlng = legs[i].start_location;
                    _this4.waypoint.address = legs[i].start_address;

                    _this4.waypointLocations.push(_this4.waypoint);
                  }

                  if (i == legs.length - 1) {
                    _this4.endLocation.latlng = legs[i].end_location;
                    _this4.endLocation.address = legs[i].end_address;
                    var endDistance = legs[i].distance;
                    var endDuration = legs[i].duration;
                  } // var steps = legs[i].steps;

                }

                var legs2 = response.routes[0].legs;

                _this4.createMarker(_this4.endLocation.latlng, "end", "special text for end marker", "http://www.google.com/mapfiles/markerB.png", endDistance, endDuration);

                _this4.createMarker(_this4.startLocation.latlng, "start", "special text for start marker", "http://maps.gstatic.com/mapfiles/markers2/marker_greenA.png", startDistance, startDuracion);

                for (var i = 0; i < _this4.waypointLocations.length; i++) {
                  _this4.apostasi[i] = legs[i + 1].distance;
                  _this4.xronos[i] = legs[i + 1].duration;

                  _this4.createMarker(_this4.waypointLocations[i].latlng, "waypoint " + i, "special text for waypoint marker " + i, "http://www.google.com/mapfiles/marker_yellow.png", _this4.apostasi[i], _this4.xronos[i]);
                }
              } else {
                alert('Could not display directions due to: ' + status);
              }
            });
          }
        }, {
          key: "createMarker",
          value: function createMarker(latlng, label, html, url, distance, duration) {
            console.log(distance);

            if (distance == undefined) {
              distance = "";
            }

            if (duration == undefined) {
              distance = "";
            }

            var dis = distance.text;
            var dur = duration.text;
            var sint = latlng.lat() + " , " + latlng.lng();
            var contentString = '<b>' + label + '</b><br>' + "<p>" + sint + "<p>" + "Επόμενος σταθμός:" + dis + "/" + dur;
            var infowindow2 = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
              position: latlng,
              map: this.map,
              icon: url,
              title: label,
              subtitle: sint,
              zIndex: Math.round(latlng.lat() * -100000) << 5
            });
            google.maps.event.addListener(marker, 'click', function () {
              infowindow2.setContent(contentString);
              infowindow2.open(this.map, marker);
            });
          }
        }, {
          key: "startWithoutApi",
          value: function startWithoutApi() {
            // 	this.backgroundGeolocation.start();
            var startDate = new Date().toLocaleTimeString();
            console.log(startDate);
            this.board = 1;
            this.startTr = 1;

            if (this.startTr == 1) {
              this.onTimeOut();
            }

            alert("Αποδοχή δρομολογίου από τη θέση " + this.latitude + "," + this.longitude + " και ώρα " + startDate + ".");
          }
        }, {
          key: "endWithoutApi",
          value: function endWithoutApi() {
            //	this.backgroundGeolocation.stop();
            var endDate = new Date().toLocaleTimeString();
            console.log(endDate);
            this.startTr = 0;
            alert("Τέλος δρομολογίου από τη θέση " + this.latitude + "," + this.longitude + " και ώρα " + endDate + ".");
            this.telos = 1;

            if (this.telos == 1) {
              console.log(this.dataFromTheRoutes);
              console.log(this.dataFromTheDriverId);
              this.router.navigate(['techinspect-finished-route/' + this.dataFromTheRoutes + '/' + this.dataFromTheDriverId]);
            } else {
              alert("Το δρομολόγιο δεν έχει τελειώσει");
            }
          }
        }, {
          key: "inspect",
          value: function inspect() {
            if (this.telos == 1) {
              this.router.navigate(['techinspect']);
            } else {
              alert("Απαιτείται ολοκλήρωση του δρομολογίου.");
            }
          }
        }, {
          key: "navigateToRouteOnGoPage",
          value: function navigateToRouteOnGoPage() {
            this.router.navigate(['routestarted']);
          }
        }, {
          key: "onTimeOut",
          value: function onTimeOut() {
            var _this5 = this;

            if (this.startTr == 1) {
              Geolocation.getCurrentPosition().then(function (response) {
                console.log(response.coords.latitude, response.coords.longitude);

                var nativeCall = _this5.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_drv_geo.cfm?' + 'driver_id=' + _this5.dataFromTheRoutesJSON.DRIVER_ID + '&srv_type=' + _this5.dataFromTheRoutesJSON.SERVICE + '&srv_code=' + _this5.dataFromTheRoutesJSON.SERVICECODE + '&sp_id=' + -1 + '&sp_code=' + -1 + '&fromd=' + _this5.dataFromTheRoutesJSON.ASSIGNMENT_FROM_DATE + '&tod=' + _this5.dataFromTheRoutesJSON.ASSIGNMENT_TO_DATE + '&vehicle_map_id=' + _this5.dataFromTheRoutesJSON.VEHICLE_MAP_ID + '&vhc_id=' + 1 + '&vhc_plates=' + _this5.dataFromTheRoutesJSON.VHC_PLATES + '&version_id=' + 1 + '&VechicleTypeID=' + 1 + '&virtualversion_id=' + 1 + '&latitude=' + response.coords.latitude + '&longitude=' + response.coords.longitude + '&userid=dmta', {}, {
                  'Content-Type': 'application/json'
                });

                Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["finalize"])(function () {
                  return console.log('');
                })).subscribe(function (data) {
                  console.log(data);
                });
              })["catch"](function (error) {
                console.log('error could not ');
                console.log(error);
              });
              setTimeout(function () {
                _this5.onTimeOut();
              }, 100000);
            }
          }
        }, {
          key: "showBtn",
          value: function showBtn() {
            console.log('here');

            if (this.hideBackBtn === false && this.hideEndBtn === false && this.hideCustomBtn === false) {
              this.hideBackBtn = true;
              this.hideEndBtn = true;
              this.hideCustomBtn = true;
              document.getElementById('map').style.height = "75%";
            } else {
              this.hideBackBtn = false;
              this.hideEndBtn = false;
              this.hideCustomBtn = false;
              document.getElementById('map').style.height = "87%";
            }
          }
        }]);

        return MapPage;
      }();

      MapPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__["HTTP"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]
        }, {
          type: _ionic_native_background_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["BackgroundGeolocation"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      MapPage.propDecorators = {
        mapElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['map']
        }]
      };
      MapPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./map.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./map.page.scss */
        "./src/app/map/map.page.scss"))["default"]]
      })], MapPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=map-map-module-es5.js.map