(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["welcome2-welcome2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppWelcome2Welcome2PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>welcome2</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/welcome2/welcome2-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/welcome2/welcome2-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: Welcome2PageRoutingModule */

    /***/
    function srcAppWelcome2Welcome2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Welcome2PageRoutingModule", function () {
        return Welcome2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _welcome2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./welcome2.page */
      "./src/app/welcome2/welcome2.page.ts");

      var routes = [{
        path: '',
        component: _welcome2_page__WEBPACK_IMPORTED_MODULE_3__["Welcome2Page"]
      }];

      var Welcome2PageRoutingModule = function Welcome2PageRoutingModule() {
        _classCallCheck(this, Welcome2PageRoutingModule);
      };

      Welcome2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Welcome2PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/welcome2/welcome2.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/welcome2/welcome2.module.ts ***!
      \*********************************************/

    /*! exports provided: Welcome2PageModule */

    /***/
    function srcAppWelcome2Welcome2ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Welcome2PageModule", function () {
        return Welcome2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _welcome2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./welcome2-routing.module */
      "./src/app/welcome2/welcome2-routing.module.ts");
      /* harmony import */


      var _welcome2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./welcome2.page */
      "./src/app/welcome2/welcome2.page.ts");

      var Welcome2PageModule = function Welcome2PageModule() {
        _classCallCheck(this, Welcome2PageModule);
      };

      Welcome2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _welcome2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Welcome2PageRoutingModule"]],
        declarations: [_welcome2_page__WEBPACK_IMPORTED_MODULE_6__["Welcome2Page"]]
      })], Welcome2PageModule);
      /***/
    },

    /***/
    "./src/app/welcome2/welcome2.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/welcome2/welcome2.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppWelcome2Welcome2PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUyL3dlbGNvbWUyLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/welcome2/welcome2.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/welcome2/welcome2.page.ts ***!
      \*******************************************/

    /*! exports provided: Welcome2Page */

    /***/
    function srcAppWelcome2Welcome2PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Welcome2Page", function () {
        return Welcome2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var Welcome2Page = /*#__PURE__*/function () {
        function Welcome2Page() {
          _classCallCheck(this, Welcome2Page);
        }

        _createClass(Welcome2Page, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return Welcome2Page;
      }();

      Welcome2Page.ctorParameters = function () {
        return [];
      };

      Welcome2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-welcome2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./welcome2.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome2/welcome2.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./welcome2.page.scss */
        "./src/app/welcome2/welcome2.page.scss"))["default"]]
      })], Welcome2Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=welcome2-welcome2-module-es5.js.map