(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registercomplete-registercomplete-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/registercomplete/registercomplete.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registercomplete/registercomplete.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRegistercompleteRegistercompletePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"registration-page\">\n  <ion-toolbar color=\"blue\">\n    <ion-title class=\"toolbar-title center\">{{'registercom-emailverify.title' | translate}}</ion-title>\n    <ion-buttons>\n    <ion-button class=\"back-btn\" (click)=\"navigateBack()\"><ion-icon class=\"arrow-back\" slot=\"start\" name=\"arrow-back\"></ion-icon></ion-button>\n  </ion-buttons>\n  </ion-toolbar>\n\n\n  <div class=\"registration-completed-desc\">\n    <a class=\"reg-title1\">{{'registercom-regcomplete.title' | translate}}</a>\n    <a class=\"reg-title2\">{{'registercom-success.title' | translate}}</a>\n  </div>\n  <div class=\"center\">\n    <img src=\"assets/images/registration_steps_three.svg\" alt=\"Registration is Completed\" />\n  </div>\n  <div class=\"steps-desc\">\n    <a class=\"step-1\">{{'register-info.title' | translate}}</a>\n    <a class=\"step-2\">{{'register-addressinfo.title' | translate}}</a>\n    <a class=\"step-3\">{{'register-driverinfo.title' | translate}}</a>\n  </div>\n\n\n  <div class=\"email-verification-desc\">\n    <a>{{'passrec-emailsend.title' | translate}}</a>\n    <a>{{'passrec-emailsend2.title' | translate}}</a>\n    <a>{{'passrec-verify.title' | translate}}</a>\n    <a>{{'passrec-trylogin.title' | translate}}</a>\n  </div>\n  <form id=\"form\">\n  <ion-button *ngIf=\"!hasVerifiedEmail\" class=\"btn-send\" expand=\"block\" shape=\"round\" (click)=\"navigateLogin()\">{{'registercom-return.title' | translate}}</ion-button>\n</form>\n</div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/registercomplete/registercomplete-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/registercomplete/registercomplete-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: RegistercompletePageRoutingModule */

    /***/
    function srcAppRegistercompleteRegistercompleteRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistercompletePageRoutingModule", function () {
        return RegistercompletePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _registercomplete_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./registercomplete.page */
      "./src/app/registercomplete/registercomplete.page.ts");

      var routes = [{
        path: '',
        component: _registercomplete_page__WEBPACK_IMPORTED_MODULE_3__["RegistercompletePage"]
      }];

      var RegistercompletePageRoutingModule = function RegistercompletePageRoutingModule() {
        _classCallCheck(this, RegistercompletePageRoutingModule);
      };

      RegistercompletePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegistercompletePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/registercomplete/registercomplete.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/registercomplete/registercomplete.module.ts ***!
      \*************************************************************/

    /*! exports provided: RegistercompletePageModule */

    /***/
    function srcAppRegistercompleteRegistercompleteModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistercompletePageModule", function () {
        return RegistercompletePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _registercomplete_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./registercomplete-routing.module */
      "./src/app/registercomplete/registercomplete-routing.module.ts");
      /* harmony import */


      var _registercomplete_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./registercomplete.page */
      "./src/app/registercomplete/registercomplete.page.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

      var RegistercompletePageModule = function RegistercompletePageModule() {
        _classCallCheck(this, RegistercompletePageModule);
      };

      RegistercompletePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _registercomplete_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistercompletePageRoutingModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]],
        declarations: [_registercomplete_page__WEBPACK_IMPORTED_MODULE_6__["RegistercompletePage"]]
      })], RegistercompletePageModule);
      /***/
    },

    /***/
    "./src/app/registercomplete/registercomplete.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/registercomplete/registercomplete.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppRegistercompleteRegistercompletePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".registration-page {\n  position: relative;\n  top: 55px;\n  z-index: 200;\n}\n\n.back-btn {\n  position: relative;\n  bottom: 30px;\n  left: 20px;\n  width: 50px;\n  height: 50px;\n}\n\n.arrow-back {\n  position: relative;\n  left: 10px;\n  top: -6px;\n}\n\n.toolbar-title {\n  font-size: 16px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-blue);\n  position: relative;\n  bottom: 2px;\n}\n\n.registration-completed-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  position: relative;\n  top: -57px;\n  margin-bottom: 72px;\n}\n\n.email-verification-desc {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  font-family: monteRegular;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  margin-top: 60px;\n  top: -5px;\n  position: relative;\n  margin-bottom: 49px;\n}\n\n.reg-title1 {\n  font-family: monteBold;\n  font-size: 26px;\n  color: var(--ion-color-ibus-darkblue);\n  position: relative;\n  top: 65px;\n}\n\n.reg-title2 {\n  font-family: monteBold;\n  font-size: 28px;\n  color: var(--ion-color-ibus-lightgreen);\n  position: relative;\n  top: 65px;\n}\n\n.steps-desc {\n  position: relative;\n  top: 55px;\n}\n\n.step-1 {\n  position: relative;\n  left: 30px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-2 {\n  position: relative;\n  left: 70px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.step-3 {\n  position: relative;\n  left: 120px;\n  font-size: 11px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.btn-send {\n  height: 60px;\n  height: 18;\n  text-align: center;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  margin-top: 7%;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n}\n\n@media only screen and (max-width: 428px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 7px;\n  }\n}\n\n@media only screen and (max-width: 414px) {\n  .steps-desc {\n    position: relative;\n    top: 10px;\n    left: 0px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 14px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -2px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .steps-desc {\n    display: none;\n  }\n\n  .toolbar-title {\n    font-size: 12px;\n    font-family: monteBold;\n    color: var(--ion-color-ibus-blue);\n    position: relative;\n    top: -5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXJjb21wbGV0ZS9yZWdpc3RlcmNvbXBsZXRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFHSjs7QUFLQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUFGSjs7QUFJQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBREo7O0FBR0E7RUFDSSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQUFKOztBQUVBO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0VBQ0EsdUNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUFDSjs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUFBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7QUFHSjs7QUFEQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBSUo7O0FBRkE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQ0FBQTtBQUtKOztBQUhBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG1FQUFBO0VBQ0EsY0FBQTtBQU1KOztBQUhBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFNSjs7QUFIQTtFQUVJO0lBQ0ksa0JBQUE7SUFDQSxTQUFBO0lBQ0EsU0FBQTtFQUtOO0FBQ0Y7O0FBSEE7RUFFSTtJQUNJLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFNBQUE7RUFJTjtBQUNGOztBQUZBO0VBQ0k7SUFDSSxhQUFBO0VBSU47O0VBRkU7SUFDSSxlQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQ0FBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtFQUtOO0FBQ0Y7O0FBSEE7RUFDSTtJQUNJLGFBQUE7RUFLTjs7RUFIRTtJQUNJLGVBQUE7SUFDQSxzQkFBQTtJQUNBLGlDQUFBO0lBQ0Esa0JBQUE7SUFDQSxTQUFBO0VBTU47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyY29tcGxldGUvcmVnaXN0ZXJjb21wbGV0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmVnaXN0cmF0aW9uLXBhZ2V7XHJcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgIHRvcDo1NXB4O1xyXG4gICAgei1pbmRleDoyMDA7XHJcbn1cclxuXHJcbi5iYWNrLWJ0bntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvdHRvbTogMzBweDtcclxuICAgIGxlZnQ6MjBweDtcclxuICAgIHdpZHRoOjUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuLmFycm93LWJhY2t7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAxMHB4O1xyXG4gICAgdG9wOiAtNnB4O1xyXG59XHJcbi50b29sYmFyLXRpdGxle1xyXG4gICAgZm9udC1zaXplOjE2cHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTsgICBcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgYm90dG9tOjJweDtcclxufVxyXG4vLyBpbWd7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICBsZWZ0OiAzNXB4O1xyXG4vLyAgICAgbWFyZ2luLXRvcDogNzFweDtcclxuLy8gICAgIHRvcDogNTZweDtcclxuLy8gfVxyXG4ucmVnaXN0cmF0aW9uLWNvbXBsZXRlZC1kZXNje1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtNTdweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDcycHg7XHJcbn1cclxuLmVtYWlsLXZlcmlmaWNhdGlvbi1kZXNje1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVSZWd1bGFyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xyXG4gICAgdG9wOiAtNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDlweDtcclxufVxyXG4ucmVnLXRpdGxlMXtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiA2NXB4O1xyXG59XHJcbi5yZWctdGl0bGUye1xyXG4gICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogNjVweDtcclxufVxyXG4uc3RlcHMtZGVzY3tcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgdG9wOjU1cHg7XHJcbn1cclxuLnN0ZXAtMXtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDozMHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLnN0ZXAtMntcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDo3MHB4O1xyXG4gICAgZm9udC1zaXplOjExcHg7XHJcbiAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLnN0ZXAtM3tcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgbGVmdDoxMjBweDtcclxuICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xyXG4gICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xyXG59XHJcbi5idG4tc2VuZHtcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGhlaWdodDoxODtcclxuICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgIzRFOThGRiA5MCUsIzRFOThGRiAxMCUpO1xyXG4gICAgbWFyZ2luLXRvcDo3JTtcclxufVxyXG5cclxuI2Zvcm0ge1xyXG4gICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6MzBweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6MzBweDtcclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MjhweCkge1xyXG4gXHJcbiAgICAuc3RlcHMtZGVzY3tcclxuICAgICAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcclxuICAgICAgICB0b3A6MTBweDtcclxuICAgICAgICBsZWZ0OjdweDtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQxNHB4KSB7XHJcbiBcclxuICAgIC5zdGVwcy1kZXNje1xyXG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDoxMHB4O1xyXG4gICAgICAgIGxlZnQ6MHB4O1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzc1cHgpIHtcclxuICAgIC5zdGVwcy1kZXNje1xyXG4gICAgICAgIGRpc3BsYXk6bm9uZTtcclxuICAgIH1cclxuICAgIC50b29sYmFyLXRpdGxle1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1ibHVlKTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOi0ycHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gICAgLnN0ZXBzLWRlc2N7XHJcbiAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgfVxyXG4gICAgLnRvb2xiYXItdGl0bGV7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6LTVweDtcclxuICAgIH1cclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/registercomplete/registercomplete.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/registercomplete/registercomplete.page.ts ***!
      \***********************************************************/

    /*! exports provided: RegistercompletePage */

    /***/
    function srcAppRegistercompleteRegistercompletePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistercompletePage", function () {
        return RegistercompletePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var RegistercompletePage = /*#__PURE__*/function () {
        function RegistercompletePage(router) {
          _classCallCheck(this, RegistercompletePage);

          this.router = router;
        }

        _createClass(RegistercompletePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "navigateLogin",
          value: function navigateLogin() {
            this.router.navigate(['login']);
          }
        }, {
          key: "navigateBack",
          value: function navigateBack() {
            this.router.navigate(['register3']);
          }
        }]);

        return RegistercompletePage;
      }();

      RegistercompletePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      RegistercompletePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registercomplete',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./registercomplete.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/registercomplete/registercomplete.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./registercomplete.page.scss */
        "./src/app/registercomplete/registercomplete.page.scss"))["default"]]
      })], RegistercompletePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=registercomplete-registercomplete-module-es5.js.map