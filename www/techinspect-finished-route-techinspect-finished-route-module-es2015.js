(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["techinspect-finished-route-techinspect-finished-route-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect-finished-route/techinspect-finished-route.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect-finished-route/techinspect-finished-route.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home2/routelist\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-techHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToLoginPage()\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n\n\n<div class=\"ion-page\" id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'vehicleCheck-check.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n  <ion-content>\n    <ion-list>\n    <ion-label class=\"ion-text-center\">\n      <p class=\"titleStyle\">{{'vehicleCheck-pleaseCheck.title' | translate}}</p>\n    </ion-label>\n  <!--   <ion-list class=\"ionListClass\">\n      <ion-button [ngClass]=\"{'btnStyleChanged' : isChanged }\" (click)=\"color()\"  class=\"btnStyle\">TIRES</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button   class=\"btnStyle\">FUEL</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button class=\"btnStyle\">ENGINE</ion-button>\n    </ion-list>\n    <ion-list class=\"ionListClass\">\n      <ion-button  class=\"btnStyle\">BUS</ion-button>\n    </ion-list> -->\n    <form [formGroup]=\"commentForm\" (ngSubmit)=\"submitWithEveryHttp()\">\n\n     <div *ngIf=\"eng\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'vehicleCheck-checkArea.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select interface=\"action-sheet\" formControlName=\"measurementData\" [(ngModel)]=\"checkpointen\"  >\n          <ion-select-option  class=\"custom-options\" *ngFor=\"let enlang of englishLanguageJSON; let i = index\" [value]=\"enlang\">\n                      {{enlang.CHECKPOINT}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n    </div>\n\n     <div *ngIf=\"el\" class=\"centerItems styleItems ion-padding\">\n      <ion-label class=\"labelIon\">{{'vehicleCheck-checkArea.title' | translate}}</ion-label>\n      <ion-item class=\"itemIon\" lines=\"none\">\n        <ion-select [(ngModel)]=\"checkpointel\"  interface=\"action-sheet\">\n          <ion-select-option  *ngFor=\"let ellang of ellangs\" [value]=\"ellang\">\n                      {{ellang}}\n          </ion-select-option>\n        </ion-select>\n      </ion-item >\n\n\n    </div>\n\n    <!-- <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button (click)=\"wannaBeData()\" class=\"adBtn2\">\n        {{'vehicleCheck-start.title' | translate}}\n      </button>\n    </ion-buttons> -->\n  \n    <ion-list class=\"ionListClass\">  \n      <textarea formControlName=\"comment\" type=\"text\" class=\"txtArea\" rows=\"4\" cols=\"50\" [(ngModel)]=\"amount\"  (change)=check(amount)>\n        {{'vehicleCheck-comments.title' | translate}}\n      </textarea>\n      <div *ngFor=\"let error of errorMessages.comment\">\n        <ng-container *ngIf=\"comment.hasError(error.type) && (comment.dirty || comment.touched)\">\n          <small class=\"error-message\">{{error.message}}</small>\n        </ng-container>\n      </div>\n    </ion-list>\n    <div class=\"myiontab\">\n\n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n        <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n          <button class=\"adBtn2\">\n           END\n          </button>\n        </ion-buttons>\n       \n      </ion-tab-bar>\n    \n    </div>\n  \n  </form>\n\n    </ion-list>\n  </ion-content>\n  \n</div>\n\n\n\n\n\n <!-- <div class=\"myiontab\">\n\n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n    <ion-buttons type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button class=\"adBtn2\">\n        {{'vehicleCheck-start.title' | translate}}\n      </button>\n    </ion-buttons>\n  </ion-tab-bar>\n\n</div>\n<div class=\"myiontab2\">\n  <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n  <ion-buttons  (click)=\"navigateToroutelist()\" type=\"submit\" class=\"hide-on-keyboard-open addBtn\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n    <button  (click)=\"navigateToroutelist()\" class=\"adBtn2\">\n      CANCEL\n    </button>\n  </ion-buttons>      \n</ion-tab-bar>\n</div> -->\n");

/***/ }),

/***/ "./src/app/techinspect-finished-route/techinspect-finished-route-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/techinspect-finished-route/techinspect-finished-route-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: TechinspectFinishedRoutePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectFinishedRoutePageRoutingModule", function() { return TechinspectFinishedRoutePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _techinspect_finished_route_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./techinspect-finished-route.page */ "./src/app/techinspect-finished-route/techinspect-finished-route.page.ts");




const routes = [
    {
        path: '',
        component: _techinspect_finished_route_page__WEBPACK_IMPORTED_MODULE_3__["TechinspectFinishedRoutePage"]
    }
];
let TechinspectFinishedRoutePageRoutingModule = class TechinspectFinishedRoutePageRoutingModule {
};
TechinspectFinishedRoutePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TechinspectFinishedRoutePageRoutingModule);



/***/ }),

/***/ "./src/app/techinspect-finished-route/techinspect-finished-route.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/techinspect-finished-route/techinspect-finished-route.module.ts ***!
  \*********************************************************************************/
/*! exports provided: TechinspectFinishedRoutePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectFinishedRoutePageModule", function() { return TechinspectFinishedRoutePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _techinspect_finished_route_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./techinspect-finished-route-routing.module */ "./src/app/techinspect-finished-route/techinspect-finished-route-routing.module.ts");
/* harmony import */ var _techinspect_finished_route_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./techinspect-finished-route.page */ "./src/app/techinspect-finished-route/techinspect-finished-route.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let TechinspectFinishedRoutePageModule = class TechinspectFinishedRoutePageModule {
};
TechinspectFinishedRoutePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _techinspect_finished_route_routing_module__WEBPACK_IMPORTED_MODULE_5__["TechinspectFinishedRoutePageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_techinspect_finished_route_page__WEBPACK_IMPORTED_MODULE_6__["TechinspectFinishedRoutePage"]]
    })
], TechinspectFinishedRoutePageModule);



/***/ }),

/***/ "./src/app/techinspect-finished-route/techinspect-finished-route.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/techinspect-finished-route/techinspect-finished-route.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background: #4E98FF;\n  height: 87px;\n}\n\n.error-message {\n  position: absolute;\n  top: 120px;\n  left: 1px;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-red);\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.my-custom-interface .select-interface-option {\n  color: red;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.labelIon {\n  position: absolute;\n  font-size: 20px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.itemIon {\n  width: 100%;\n  margin-top: 10%;\n}\n\nion-select {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding: 4px 5px 5px 29px;\n  text-align: center;\n}\n\n.centerItems {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n}\n\n.titleStyle {\n  position: relative;\n  top: -18px;\n  left: 0px;\n  color: var(--ion-color-ibus-darkblue);\n  font-size: 21px;\n  z-index: 9999;\n  margin-top: 40px;\n  font-family: monteBold;\n}\n\n.titleStyle {\n  font-size: 25px;\n  font-family: monteSBold;\n  color: var(--ion-color-ibus-darkblue);\n  padding: 37px;\n}\n\n.ionListClass {\n  justify-content: center;\n  display: flex !important;\n  align-items: center !important;\n}\n\n.ionListClass .btnStyle {\n  width: 88%;\n  height: 50px;\n  border-radius: 12px;\n  --background: var(--ion-color-ibus-theblue);\n  font-family: monteBold;\n  font-size: 16px;\n  --background-activated: red;\n}\n\n.ionListClass .btnStyleChanged {\n  --background: var(--ion-color-ibus-lightgreen);\n}\n\n.ionListClass .txtArea {\n  width: 91%;\n  height: 150px;\n  border-radius: 8px;\n  border: none;\n  background-color: var(--ion-color-ibus-theblue);\n  color: white;\n  font-family: monteSBold;\n  font-size: 16px;\n  padding: 56px;\n  margin-top: 1%;\n}\n\nionic-selectable {\n  font-size: 21px;\n  font-family: monteSBold;\n  color: white;\n  background-color: var(--ion-color-ibus-theblue);\n  width: 100%;\n  height: 70px;\n  border-radius: 8px;\n  padding-top: 8px;\n  opacity: 1 !important;\n  text-align: center;\n}\n\n.myiontab ion-tab-bar {\n  justify-content: center;\n  display: flex;\n  width: 100%;\n  height: 90px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-red);\n  border-radius: 70px;\n  width: 80px;\n  height: 80px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab2 ion-tab-bar {\n  justify-content: center;\n  display: flex;\n  width: 100%;\n  height: 90px;\n}\n\n.myiontab2 ion-tab-bar .addBtn {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab2 ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-red);\n  border-radius: 70px;\n  width: 80px;\n  height: 80px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab2 ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab2 ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.bottomBar .ionTabBar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n}\n\n.bottomBar .ionTabBar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.bottomBar .ionTabBar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.bottomBar .ionTabBar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.bottomBar .ionTabBar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.bottomBar .ionTabBar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.bottomBar .ionTabBar svg path {\n  fill: var(--ion-color-light);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.centerTitle {\n  justify-content: center;\n  width: 100%;\n  display: flex;\n  text-align: center;\n  margin-top: 6%;\n}\n\nion-select::part(icon) {\n  display: none !important;\n}\n\n.srchBtn {\n  width: 100%;\n  margin-top: 6%;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 13px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 10px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  .ionListClass {\n    justify-content: center;\n    display: flex !important;\n    align-items: center !important;\n  }\n  .ionListClass .btnStyle {\n    width: 88%;\n    height: 50px;\n    border-radius: 12px;\n    --background: var(--ion-color-ibus-theblue);\n    font-family: monteBold;\n    font-size: 16px;\n    --background-activated: red;\n  }\n  .ionListClass .btnStyleChanged {\n    --background: var(--ion-color-ibus-lightgreen);\n  }\n  .ionListClass .txtArea {\n    width: 91%;\n    height: 150px;\n    border-radius: 8px;\n    border: none;\n    background-color: var(--ion-color-ibus-theblue);\n    color: white;\n    font-family: monteSBold;\n    font-size: 8px;\n    padding: 56px;\n    margin-top: 1%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVjaGluc3BlY3QtZmluaXNoZWQtcm91dGUvdGVjaGluc3BlY3QtZmluaXNoZWQtcm91dGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNENBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSx5QkFBQTtFQUNBLHFDQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFDQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxxQ0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EscUNBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0EsOEJBQUE7QUFDSjs7QUFDSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0FBQ1I7O0FBRUk7RUFDSSw4Q0FBQTtBQUFSOztBQUdJO0VBQ0ksVUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSwrQ0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQURSOztBQU1BO0VBQ0ksZUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLCtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQUhKOztBQVFJO0VBQ0ksdUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFMUjs7QUFPUTtFQUVJLDBDQUFBO0VBRUEsaUJBQUE7QUFQWjs7QUFRWTtFQUNJLGtCQUFBO0VBQ0EsMkNBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQU5oQjs7QUFTWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFQaEI7O0FBVVk7RUFDSSx5QkFBQTtBQVJoQjs7QUFpQkk7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQWRSOztBQWdCUTtFQUVJLDBDQUFBO0VBRUEsaUJBQUE7QUFoQlo7O0FBaUJZO0VBQ0ksa0JBQUE7RUFDQSwyQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBZmhCOztBQWtCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFoQmhCOztBQW1CWTtFQUNJLHlCQUFBO0FBakJoQjs7QUFzRUk7RUFDSSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7QUFuRVI7O0FBcUVRO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBbkVaOztBQXNFUTtFQUNJLG9DQUFBO0FBcEVaOztBQXVFUTtFQUNJLGlCQUFBO0VBQ0EsNkJBQUE7QUFyRVo7O0FBd0VRO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtBQXRFWjs7QUF5RVE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7QUF2RVo7O0FBeUVZO0VBQ0ksNEJBQUE7QUF2RWhCOztBQThFQTtFQUNJLHFDQUFBO0FBM0VKOztBQThFQTtFQUNJLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUEzRUo7O0FBOEVBO0VBQ0ksd0JBQUE7QUEzRUo7O0FBOEVBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7QUEzRUo7O0FBOEVBO0VBQ0k7SUFDSSxxQkFBQTtJQUNBLGFBQUE7RUEzRU47O0VBOEVFO0lBQ0ksdUJBQUE7SUFDQSx3QkFBQTtJQUNBLDhCQUFBO0VBM0VOO0VBNkVNO0lBQ0ksVUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLDJDQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0lBQ0EsMkJBQUE7RUEzRVY7RUE4RU07SUFDSSw4Q0FBQTtFQTVFVjtFQStFTTtJQUNJLFVBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0lBQ0EsK0NBQUE7SUFDQSxZQUFBO0lBQ0EsdUJBQUE7SUFDQSxlQUFBO0lBQ0EsYUFBQTtJQUNBLGNBQUE7RUE3RVY7QUFDRjs7QUFrRkE7RUFDSTtJQUNJLHFCQUFBO0lBQ0EsWUFBQTtFQWhGTjtBQUNGOztBQW1GQTtFQUNJO0lBQ0kscUJBQUE7SUFDQSxZQUFBO0VBakZOOztFQW9GRTtJQUNJLGtCQUFBO0lBQ0EsVUFBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtFQWpGTjs7RUFvRkU7SUFDSSx1QkFBQTtJQUNBLHdCQUFBO0lBQ0EsOEJBQUE7RUFqRk47RUFtRk07SUFDSSxVQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsMkNBQUE7SUFDQSxzQkFBQTtJQUNBLGVBQUE7SUFDQSwyQkFBQTtFQWpGVjtFQW9GTTtJQUNJLDhDQUFBO0VBbEZWO0VBcUZNO0lBQ0ksVUFBQTtJQUNBLGFBQUE7SUFDQSxrQkFBQTtJQUNBLFlBQUE7SUFDQSwrQ0FBQTtJQUNBLFlBQUE7SUFDQSx1QkFBQTtJQUNBLGVBQUE7SUFDQSxhQUFBO0lBQ0EsY0FBQTtFQW5GVjtBQUNGOztBQXdGQTtFQUNJO0lBQ0ksdUJBQUE7SUFDQSx3QkFBQTtJQUNBLDhCQUFBO0VBdEZOO0VBd0ZNO0lBQ0ksVUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLDJDQUFBO0lBQ0Esc0JBQUE7SUFDQSxlQUFBO0lBQ0EsMkJBQUE7RUF0RlY7RUF5Rk07SUFDSSw4Q0FBQTtFQXZGVjtFQTBGTTtJQUNJLFVBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxZQUFBO0lBQ0EsK0NBQUE7SUFDQSxZQUFBO0lBQ0EsdUJBQUE7SUFDQSxjQUFBO0lBQ0EsYUFBQTtJQUNBLGNBQUE7RUF4RlY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3RlY2hpbnNwZWN0LWZpbmlzaGVkLXJvdXRlL3RlY2hpbnNwZWN0LWZpbmlzaGVkLXJvdXRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWJsdWUpO1xufVxuXG4uaW9zIGlvbi10b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XG4gICAgaGVpZ2h0OiAxMjdweDtcbn1cblxuLm1kIGlvbi10b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XG4gICAgaGVpZ2h0OiA4N3B4O1xufVxuXG4uZXJyb3ItbWVzc2FnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTIwcHg7XG4gICAgbGVmdDogMXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZVJlZ3VsYXI7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXJlZCk7XG59XG5cbi5ub3RCdG4ge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogNDdweDtcbiAgICBoZWlnaHQ6IDYycHg7XG59XG5cbi5tZW51QnRuIHtcbiAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIHdpZHRoOiA3MHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm15LWN1c3RvbS1pbnRlcmZhY2UgLnNlbGVjdC1pbnRlcmZhY2Utb3B0aW9uIHtcbiAgICBjb2xvcjogcmVkO1xufVxuXG4ubWVudVR4dCB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZVJlZ3VsYXI7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcbn1cblxuLmljb25TdHlsaW5nIHtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xufVxuXG4ubWVudVRvb2xiYXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tdG9wOiA4MHB4O1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmlvcyAjcHJvZmlsZSB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMjdweDtcbiAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL3Byb2ZpbGUuanBnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDYwcHggODBweDtcbn1cblxuLmlvcyBpb24tdGl0bGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC0xNXB4O1xuICAgIGxlZnQ6IC0zMHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDIxcHg7XG59XG5cbi5tZCAjcHJvZmlsZSB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMjdweDtcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvaW1hZ2VzL3Byb2ZpbGUuanBnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDYwcHggODBweDtcbn1cblxuLmlvcyAubmF2LWJ1dHRvbnMge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5tZCBpb24tdGl0bGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDMwcHg7XG4gICAgbGVmdDogODBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAyMXB4O1xufVxuXG4ubWQgLm5hdi1idXR0b25zIHtcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcbn1cblxuLmxhYmVsSW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcbn1cblxuLml0ZW1Jb24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbn1cblxuaW9uLXNlbGVjdCB7XG4gICAgZm9udC1zaXplOiAyMXB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHBhZGRpbmc6IDRweCA1cHggNXB4IDI5cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2VudGVySXRlbXMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi50aXRsZVN0eWxlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtMThweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcbiAgICBmb250LXNpemU6IDIxcHg7XG4gICAgei1pbmRleDogOTk5OTtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XG59XG5cbi50aXRsZVN0eWxlIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcbiAgICBwYWRkaW5nOiAzN3B4O1xufVxuXG4uaW9uTGlzdENsYXNzIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xuXG4gICAgLmJ0blN0eWxlIHtcbiAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xuICAgICAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHJlZDtcbiAgICB9XG5cbiAgICAuYnRuU3R5bGVDaGFuZ2VkIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcbiAgICB9XG5cbiAgICAudHh0QXJlYSB7XG4gICAgICAgIHdpZHRoOiA5MSU7XG4gICAgICAgIGhlaWdodDogMTUwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBwYWRkaW5nOiA1NnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxJTtcbiAgICB9XG5cbn1cblxuaW9uaWMtc2VsZWN0YWJsZSB7XG4gICAgZm9udC1zaXplOiAyMXB4O1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHBhZGRpbmctdG9wOiA4cHg7XG4gICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm15aW9udGFiIHtcblxuICAgIGlvbi10YWItYmFyIHtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDkwcHg7XG5cbiAgICAgICAgLmFkZEJ0biB7XG5cbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcblxuICAgICAgICAgICAgLyogZml4IG5vdGNoIGlvcyovXG4gICAgICAgICAgICBidXR0b24ge1xuICAgICAgICAgICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1yZWQpO1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDgwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA4MHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDExcHg7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgIHN0cm9rZTogNXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tYmFkZ2Uge1xuICAgICAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH1cbn1cblxuLm15aW9udGFiMiB7XG5cbiAgICBpb24tdGFiLWJhciB7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiA5MHB4O1xuXG4gICAgICAgIC5hZGRCdG4ge1xuXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XG5cbiAgICAgICAgICAgIC8qIGZpeCBub3RjaCBpb3MqL1xuICAgICAgICAgICAgYnV0dG9uIHtcbiAgICAgICAgICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtcmVkKTtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiA4MHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogODBweDtcbiAgICAgICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICBzdHJva2U6IDVweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLWJhZGdlIHtcbiAgICAgICAgICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9XG59XG5cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLSAhISEhISEgLS0tLS0tLS0tLS0tLS0tLSAvLyBcbi8vIEJvdHRvbUJhciBDU1MgXG5cblxuXG4vLyAuc3RhcnRCdXR0b25DbGFzc3tcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4vLyAgICAgdG9wOiAtMTBweDtcbi8vICAgICBoZWlnaHQ6IDYwcHg7XG4vLyAgICAgYm9yZGVyOiBub25lO1xuLy8gICAgIC5hZGRCdG4ge1xuLy8gICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbi8vICAgICAgICAgZGlzcGxheTogZmxleDtcbi8vICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gICAgICAgICBtYXJnaW4tYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7IC8qIGZpeCBub3RjaCBpb3MqL1xuLy8gICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgICAgICAgIHRvcDogOXB4O1xuLy8gICAgICAgICBidXR0b24ge1xuLy8gICAgICAgICAgICAgLS1ib3gtc2hhZG93OiBub25lO1xuLy8gICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtY2xhc3NpY2dyZWVuKTtcbi8vICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XG4vLyAgICAgICAgICAgICB3aWR0aDogNjBweDtcbi8vICAgICAgICAgICAgIGhlaWdodDogNjBweDtcbi8vICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbi8vICAgICAgICAgICAgIHRvcDogLTlweDtcbi8vICAgICAgICAgICAgIGNvbG9yOndoaXRlO1xuLy8gICAgICAgICAgICAgZm9udC1mYW1pbHk6bW9udGVTQm9sZDtcbi8vICAgICAgICAgICAgIGZvbnQtc2l6ZToxMXB4O1xuLy8gICAgICAgICAgICAgYm90dG9tOiAwcHg7XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgaW9uLWljb257XG4vLyAgICAgICAgICAgICBjb2xvcjp3aGl0ZTtcbi8vICAgICAgICAgICAgIHdpZHRoOjMwcHg7XG4vLyAgICAgICAgICAgICBoZWlnaHQ6MzBweDtcbi8vICAgICAgICAgICAgIHN0cm9rZTogNXB4O1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIGlvbi1iYWRnZXtcbi8vICAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbi8vICAgICAgICAgIH1cbi8vICAgICB9XG4vLyB9XG4uYm90dG9tQmFyIHtcblxuICAgIC8vICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLy8gaGVpZ2h0OiA2MHB4O1xuICAgIC5pb25UYWJCYXIge1xuICAgICAgICAtLWJvcmRlcjogMDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBib3R0b206IDA7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICB3aWR0aDogMTAwJTtcblxuICAgICAgICAmOmFmdGVyIHtcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBib3R0b206IDA7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICAgICAgICAgICAgaGVpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tdGFiLWJ1dHRvbiB7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tdGFiLWJ1dHRvbi5jb21tZW50cyB7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxOHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLXRhYi1idXR0b24ubm90aWZzIHtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxOHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgc3ZnIHtcbiAgICAgICAgICAgIHdpZHRoOiA3MnB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTlweDtcblxuICAgICAgICAgICAgcGF0aCB7XG4gICAgICAgICAgICAgICAgZmlsbDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tICEhISEhISAtLS0tLS0tLS0tLS0tLS0tIC8vIFxuLmljb25TdHlsaW5nIHtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtZGFya2JsdWUpO1xufVxuXG4uY2VudGVyVGl0bGUge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDYlO1xufVxuXG5pb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5zcmNoQnRuIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiA2JTtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MTRweCkge1xuICAgIGlvbi10b29sYmFyIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xuICAgICAgICBoZWlnaHQ6IDEyN3B4O1xuICAgIH1cblxuICAgIC5pb25MaXN0Q2xhc3Mge1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG5cbiAgICAgICAgLmJ0blN0eWxlIHtcbiAgICAgICAgICAgIHdpZHRoOiA4OCU7XG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiByZWQ7XG4gICAgICAgIH1cblxuICAgICAgICAuYnRuU3R5bGVDaGFuZ2VkIHtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWlidXMtbGlnaHRncmVlbik7XG4gICAgICAgIH1cblxuICAgICAgICAudHh0QXJlYSB7XG4gICAgICAgICAgICB3aWR0aDogOTElO1xuICAgICAgICAgICAgaGVpZ2h0OiAxNTBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlU0JvbGQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgICAgICBwYWRkaW5nOiA1NnB4O1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMSU7XG4gICAgICAgIH1cblxuICAgIH1cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xuICAgIGlvbi10b29sYmFyIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjNEU5OEZGO1xuICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgfVxufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDM3NXB4KSB7XG4gICAgLmlvcyBpb24tdG9vbGJhciB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xuICAgIH1cblxuICAgIC5pb3MgaW9uLXRpdGxlIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IC0xNXB4O1xuICAgICAgICBsZWZ0OiAtMTVweDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDIxcHg7XG4gICAgfVxuXG4gICAgLmlvbkxpc3RDbGFzcyB7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcblxuICAgICAgICAuYnRuU3R5bGUge1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHJlZDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5idG5TdHlsZUNoYW5nZWQge1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC50eHRBcmVhIHtcbiAgICAgICAgICAgIHdpZHRoOiA5MSU7XG4gICAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDU2cHg7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxJTtcbiAgICAgICAgfVxuXG4gICAgfVxufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XG4gICAgLmlvbkxpc3RDbGFzcyB7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcblxuICAgICAgICAuYnRuU3R5bGUge1xuICAgICAgICAgICAgd2lkdGg6IDg4JTtcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1pYnVzLXRoZWJsdWUpO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IG1vbnRlQm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHJlZDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5idG5TdHlsZUNoYW5nZWQge1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1saWdodGdyZWVuKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC50eHRBcmVhIHtcbiAgICAgICAgICAgIHdpZHRoOiA5MSU7XG4gICAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBmb250LWZhbWlseTogbW9udGVTQm9sZDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogOHB4O1xuICAgICAgICAgICAgcGFkZGluZzogNTZweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDElO1xuICAgICAgICB9XG5cbiAgICB9XG5cbn0iXX0= */");

/***/ }),

/***/ "./src/app/techinspect-finished-route/techinspect-finished-route.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/techinspect-finished-route/techinspect-finished-route.page.ts ***!
  \*******************************************************************************/
/*! exports provided: TechinspectFinishedRoutePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechinspectFinishedRoutePage", function() { return TechinspectFinishedRoutePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");









let TechinspectFinishedRoutePage = class TechinspectFinishedRoutePage {
    constructor(platform, nativeHttp, activatedRoute, loadingCtrl, http, router, formBuilder) {
        this.platform = platform;
        this.nativeHttp = nativeHttp;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.router = router;
        this.formBuilder = formBuilder;
        this.techchk_json = [{ "checkpoint_id": "33", "checkpoint_gre": "Τα στοιχεία του οχήματος (πινακίδες κυκλοφορίας, αρ. πλαισίου, αρ. κινητήρα σύμφωνα με την άδεια. κυκλοφορίας).", "checkpoint_eng": "Vehicle details (registration plates, chassis number, engine number according to the license. Registration)." },
            { "checkpoint_id": "34", "checkpoint_gre": "Το όχημα εσωτερικά- εξωτερικά (οπτικώς).", "checkpoint_eng": "The vehicle internally-externally (visually)." },
            { "checkpoint_id": "35", "checkpoint_gre": "Ο εξοπλισμός του οχήματος (τρίγωνο, πυροσβεστήρας, φαρμακείο).", "checkpoint_eng": "The equipment of the vehicle (triangle, fire extinguisher, pharmacy)." },
            { "checkpoint_id": "36", "checkpoint_gre": "Η πίεση των ελαστικών- η ύπαρξη ρεζέρβας.", "checkpoint_eng": "The tire pressure - the existence of a reserve." },
            { "checkpoint_id": "37", "checkpoint_gre": "Η ποιότητα και η σύνθεση των καυσαερίων.", "checkpoint_eng": "The quality and composition of the exhaust gases." },
            { "checkpoint_id": "38", "checkpoint_gre": "Η σύγκλιση- απόκλιση των τροχών.", "checkpoint_eng": "The convergence-deviation of the wheels." },
            { "checkpoint_id": "39", "checkpoint_gre": "Η απόδοση της ανάρτησης.", "checkpoint_eng": "Post performance." },
            { "checkpoint_id": "40", "checkpoint_gre": "Η απόδοση των φρένων.", "checkpoint_eng": "The performance of the brakes." },
            { "checkpoint_id": "41", "checkpoint_gre": "Τα φώτα ως προς την ένταση και την κλίση τους- γενική λειτουργία.", "checkpoint_eng": "Lights in terms of intensity and inclination - general function." },
            { "checkpoint_id": "42", "checkpoint_gre": "Το σύστημα διεύθυνσης.", "checkpoint_eng": "The steering system." },
            { "checkpoint_id": "43", "checkpoint_gre": "Το σύστημα μετάδοσης κίνησης.", "checkpoint_eng": "The drive system." },
            { "checkpoint_id": "44", "checkpoint_gre": "Οι άξονες – οι τροχοί- τα ελαστικά – η ανάρτηση.", "checkpoint_eng": "The axles - the wheels - the tires - the suspension." },
            { "checkpoint_id": "45", "checkpoint_gre": "Οι δίσκοι πέδησης.", "checkpoint_eng": "The brake discs." },
            { "checkpoint_id": "46", "checkpoint_gre": "Οι εύκαμπτες – άκαμπτες σωληνώσεις του συστήματος πέδησης.", "checkpoint_eng": "The flexible - rigid piping of the braking system." },
            { "checkpoint_id": "47", "checkpoint_gre": "Το κάτω μέρος του αμαξώματος (έλεγχος οξειδώσεων).", "checkpoint_eng": "The lower part of the body (oxidation control)." },
            { "checkpoint_id": "48", "checkpoint_gre": "Η εξάτμιση.", "checkpoint_eng": "The exhaust." }];
        this.langs = ["Ελληνικά", "English"];
        this.language = localStorage.getItem('lang');
        this.eng = false;
        this.el = false;
        this.com = false;
        this.imageChosen = 0;
        this.greekLanguage = [];
        this.englishLanguage = [];
        this.englishLanguageJSON = [];
        this.myCommentForm = [];
        this.checkThePoint = [];
        this.checkThePointComment = [];
        this.errorMessages = {
            comment: [
                { type: 'required', message: 'Comment is Required' },
                { type: 'maxlength', message: 'This field should be no longer than 200 characters' }
            ],
            measurementData: [
                {
                    type: 'reuired', message: 'Fill Data'
                }
            ]
        };
        this.commentForm = this.formBuilder.group({
            comment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(20)]],
            measurementData: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]]
        });
        this.serviceRegistration = "";
        this.dataFromService = "";
        const selects = document.querySelectorAll('.custom-options');
        //greg 
        this.dataFromStartedRouteWhileGoingBack = this.activatedRoute.snapshot.paramMap.get('custompickupsdata');
        this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverId = this.activatedRoute.snapshot.paramMap.get('thelogindriverid');
        this.dataFromStartedRouteWhileGoingBackJSON = JSON.parse(this.dataFromStartedRouteWhileGoingBack);
        this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON = JSON.parse(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverId);
        console.log('%c DATA FROM ROUTELIST JSON', 'color:orange;');
        console.log(this.dataFromStartedRouteWhileGoingBackJSON);
        this.personId = this.dataFromStartedRouteWhileGoingBackJSON.PERSON_ID;
        console.log('%c DATA FROM ROUTELIST LOGIN JSON', 'color:yellow;');
        console.log(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON);
        this.Id = this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON;
        this.enlangs = [];
        this.ellangs = [];
        this.ids = [];
        var k = 0;
        console.log(this.language);
        if (this.language == "en") {
            if (this.platform.is('cordova')) {
                let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=eng&userid=dmta', {}, {
                    'Content-Type': 'application/json'
                });
                Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => console.log('')))
                    .subscribe((data) => {
                    console.log(data);
                    let parsed = JSON.parse(data.data).CHECKS;
                    this.englishLanguageJSON = parsed;
                    this.test1 = this.englishLanguage;
                    console.log('%c English Language', 'color:orange;');
                    console.log(this.englishLanguageJSON);
                });
                this.el = false;
                this.eng = true;
            }
            else {
                this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=eng&userid=dmta').subscribe((data) => {
                    console.log(data);
                    this.englishLanguage = data;
                    console.log('%c English Language', 'color:orange;');
                    this.englishLanguageJSON = this.englishLanguage.CHECKS;
                    console.log(this.englishLanguageJSON);
                });
                this.el = false;
                this.eng = true;
            }
        }
        else if (this.language == "gr") {
            this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_checkpoints.cfm?lang=gre&userid=dmta').subscribe((data) => {
                this.greekLanguage = data;
                console.log('%c Greek Language', 'color:orange;');
                this.greekLanguageJSON = this.greekLanguage.DATA;
                console.log(this.greekLanguageJSON);
            });
            this.el = true;
            this.eng = false;
        }
    }
    //greg
    get measurementData() {
        return this.commentForm.get('measurementData');
    }
    get comment() {
        return this.commentForm.get('comment');
    }
    ngOnInit() {
    }
    submitWithEveryHttp() {
        this.platform.is('cordova') ? this.submitNativeClient() : this.submitHttpClient();
    }
    submitHttpClient() {
        console.log('%c MY ENGLISH JSON', 'color:orange;');
        console.log(this.commentForm.value);
        this.myCommentForm = this.commentForm.value;
        console.log(this.myCommentForm.measurementData);
        this.checkThePointComment = this.myCommentForm.comment;
        console.log(this.checkThePointComment);
        this.checkThePoint = this.myCommentForm.measurementData;
        console.log(this.checkThePoint.CHECKPOINT);
        console.log();
        //
        this.http.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_inspect_add.cfm?'
            + 'vhc_plates=' + this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES
            + '&chrbus_code=' + this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE
            + '&chrbus_sp_id=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.CHRBUS_SP_ID
            + '&sp_code=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.SP_CODE
            + '&fromd=' + this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE
            + '&tod=' + this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE
            + '&driver_id=' + this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID
            + '&checkpoint_id=' + this.checkThePoint.CHECKPOINT_ID
            + '&checkpoint_txt=' + this.checkThePoint.CHECKPOINT
            + '&checkpoint_status=' + this.checkThePoint.FLAG
            + '&comment=' + this.checkThePointComment
            + '&userid=dmta').subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(data);
            let loader = yield this.loadingCtrl.create({
                message: "Tech Inspect SuccessFully Done"
            });
            loader.present();
            setTimeout(() => {
                loader.dismiss();
                this.router.navigate(['routelist/' + this.Id + '/' + this.personId]);
            }, 1000);
        }));
    }
    submitNativeClient() {
        console.log('%c MY ENGLISH JSON', 'color:orange;');
        console.log(this.commentForm.value);
        this.myCommentForm = this.commentForm.value;
        console.log(this.myCommentForm.measurementData);
        this.checkThePointComment = this.myCommentForm.comment;
        console.log(this.checkThePointComment);
        this.checkThePoint = this.myCommentForm.measurementData;
        console.log(this.checkThePoint.CHECKPOINT);
        console.log();
        console.log("tod");
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE);
        console.log(this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID);
        console.log(this.checkThePoint.CHECKPOINT_ID);
        console.log(this.checkThePoint.CHECKPOINT);
        console.log(this.checkThePoint.FLAG);
        console.log(this.checkThePointComment);
        // let fromDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE;
        // let fromDateToGo = fromDate.split(/\s/).join('');
        // let fromDateToGo3 = fromDateToGo.replace('January,','');
        // let fromDateToGo4 = fromDateToGo3.replace(':','')
        // let fromDateToGo5 = fromDateToGo4.replace('0','')
        // console.log("DATE5",fromDateToGo5);
        // let toDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE;
        // let toDateToGo = toDate.split(/\s/).join('');
        // let toDateToGo3 = toDateToGo.replace('January,','');
        // let toDateToGo4 = toDateToGo3.replace(':','')
        // let toDateToGo5 = toDateToGo4.replace('0','')
        let fromDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_FROM_DATE;
        let fromDateToGo555 = fromDate.split(/\s/).join(',');
        console.log("from Date", fromDate);
        let fromDateToGo = new Date(fromDateToGo555);
        console.log("from Date 2 ", fromDateToGo);
        let year = fromDateToGo.getFullYear();
        console.log("year", year);
        let month = fromDateToGo.getMonth() + 1;
        console.log(month);
        let date = fromDateToGo.getDate();
        console.log(date);
        let fulldateFromDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        let toDate = this.dataFromStartedRouteWhileGoingBackJSON.ASSIGNMENT_TO_DATE;
        let toDateToGo555 = toDate.split(/\s/).join(',');
        console.log("from Date", toDate);
        let toDateToGo = new Date(toDateToGo555);
        console.log("from Date 2 ", toDateToGo);
        let year2 = toDateToGo.getFullYear();
        console.log("year", year);
        let month2 = toDateToGo.getMonth() + 1;
        console.log(month2);
        let date2 = toDateToGo.getDate();
        console.log(date2);
        let fulldateToDate = year + '-' + month + '-' + date;
        console.log(fulldateFromDate);
        //     getDate(); 27
        // getFullYear(); 2020
        let checkpoint = this.checkThePoint.CHECKPOINT;
        let checkpoint3 = checkpoint.split(/\s/).join('');
        console.log(checkpoint3);
        let commentTime = this.checkThePointComment;
        let commentToGo = commentTime.split(/\s/).join('');
        let whiteSpace = ' A  B C D ';
        let noWhiteSpace = whiteSpace.trim();
        console.log(noWhiteSpace);
        //
        let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_vehicle_tech_inspect_add.cfm?'
            + 'vhc_plates=' + this.dataFromStartedRouteWhileGoingBackJSON.VHC_PLATES
            + '&chrbus_code=' + this.dataFromStartedRouteWhileGoingBackJSON.SERVICECODE
            + '&chrbus_sp_id=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.CHRBUS_SP_ID
            + '&sp_code=' + 1 //this.dataFromStartedRouteWhileGoingBackJSON.SP_CODE
            + '&fromd=' + fulldateFromDate
            + '&tod=' + fulldateToDate
            + '&driver_id=' + this.dataFromStartedRouteWhileGoingBackJSON.DRIVER_ID
            + '&checkpoint_id=' + this.checkThePoint.CHECKPOINT_ID
            + '&checkpoint_txt=' + checkpoint3
            + '&checkpoint_status=' + this.checkThePoint.FLAG
            + '&comment=' + commentToGo
            + '&userid=dmta', {}, {
            'Content-Type': 'application/x-www-form-urlencoded',
        });
        console.log('after url call');
        Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["finalize"])(() => console.log('Ready to send')))
            .subscribe(data => {
            console.log(data);
            this.router.navigate(['routelist/' + this.Id + '/' + this.personId]);
            console.log('LEITOURGW');
        }, err => {
            console.log('Error of Vehicle check', err);
        });
    }
    map() {
        setTimeout(() => {
            this.router.navigate(['routestarted/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackJSON) + '/' + JSON.stringify(this.dataFromStartedRouteWhileGoingBackDataWasTakenFromLoginPageTheDriverIdJSON)]);
        }, 1000);
    }
    navigateToroutelist() {
        this.router.navigate(['routelist/' + this.personId + '/' + this.Id]);
    }
    navigateToWalletPage() {
        this.router.navigate(['home2/wallet']);
    }
    navigateToSettingsPage() {
        this.router.navigate(['home2/settings']);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(['home2/routehistory']);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(['home2/techhistory']);
    }
    navigateToProfilePage() {
        this.router.navigate(['profile']);
    }
    navigateToLoginPage() {
        this.router.navigate(['login']);
    }
    // check(amount){
    //   if(this.language=="en"){
    //     alert( "Check " + this.checkpointen + " has been added with your comments " + this.amount);
    //   }
    //   else
    //     if(this.language=="gr"){
    //    alert( "Ο Έλεγχος " + this.checkpointen + " έχει προστεθεί μαζί με τα σχόλιά σος " + this.amount);
    //   }
    // }
    navigateToStartRoutePage() {
        this.router.navigate(['routestarted']);
    }
};
TechinspectFinishedRoutePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__["HTTP"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] }
];
TechinspectFinishedRoutePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-techinspect-finished-route',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./techinspect-finished-route.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/techinspect-finished-route/techinspect-finished-route.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./techinspect-finished-route.page.scss */ "./src/app/techinspect-finished-route/techinspect-finished-route.page.scss")).default]
    })
], TechinspectFinishedRoutePage);



/***/ })

}]);
//# sourceMappingURL=techinspect-finished-route-techinspect-finished-route-module-es2015.js.map