(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["techhistory-techhistory-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/techhistory/techhistory.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/techhistory/techhistory.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>techhistory</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t  <ion-button class=\"btn-login\"  (click)=\"backToRoutes()\" expand=\"block\" shape=\"round\" >Go Back</ion-button>\n\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/techhistory/techhistory-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/techhistory/techhistory-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: TechhistoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechhistoryPageRoutingModule", function() { return TechhistoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _techhistory_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./techhistory.page */ "./src/app/techhistory/techhistory.page.ts");




const routes = [
    {
        path: '',
        component: _techhistory_page__WEBPACK_IMPORTED_MODULE_3__["TechhistoryPage"]
    }
];
let TechhistoryPageRoutingModule = class TechhistoryPageRoutingModule {
};
TechhistoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TechhistoryPageRoutingModule);



/***/ }),

/***/ "./src/app/techhistory/techhistory.module.ts":
/*!***************************************************!*\
  !*** ./src/app/techhistory/techhistory.module.ts ***!
  \***************************************************/
/*! exports provided: TechhistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechhistoryPageModule", function() { return TechhistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _techhistory_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./techhistory-routing.module */ "./src/app/techhistory/techhistory-routing.module.ts");
/* harmony import */ var _techhistory_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./techhistory.page */ "./src/app/techhistory/techhistory.page.ts");







let TechhistoryPageModule = class TechhistoryPageModule {
};
TechhistoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _techhistory_routing_module__WEBPACK_IMPORTED_MODULE_5__["TechhistoryPageRoutingModule"]
        ],
        declarations: [_techhistory_page__WEBPACK_IMPORTED_MODULE_6__["TechhistoryPage"]]
    })
], TechhistoryPageModule);



/***/ }),

/***/ "./src/app/techhistory/techhistory.page.scss":
/*!***************************************************!*\
  !*** ./src/app/techhistory/techhistory.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlY2hoaXN0b3J5L3RlY2hoaXN0b3J5LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/techhistory/techhistory.page.ts":
/*!*************************************************!*\
  !*** ./src/app/techhistory/techhistory.page.ts ***!
  \*************************************************/
/*! exports provided: TechhistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TechhistoryPage", function() { return TechhistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let TechhistoryPage = class TechhistoryPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    backToRoutes() {
        this.router.navigate(['home2/routelist']);
    }
};
TechhistoryPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
TechhistoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-techhistory',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./techhistory.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/techhistory/techhistory.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./techhistory.page.scss */ "./src/app/techhistory/techhistory.page.scss")).default]
    })
], TechhistoryPage);



/***/ })

}]);
//# sourceMappingURL=techhistory-techhistory-module-es2015.js.map