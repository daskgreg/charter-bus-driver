(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routepassengers-routepassengers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/routepassengers/routepassengers.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routepassengers/routepassengers.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu side=\"start\" content-id=\"main-content\">\n\n  <ion-header>\n    <ion-toolbar translucent>\n      <ion-title class=\"menuToolbar\" >{{'routelist-menu.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <ion-item routerLink=\"/home2/routelist\" >\n        <ion-icon class=\"iconStyling\" name=\"home\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myRoutes.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-myWallet.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToTechHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"archive\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-routeHistory.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToProfilePage()\">\n        <ion-icon class=\"iconStyling\" name=\"person\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-profile.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\" slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-settings.title' | translate}}</ion-label>\n      </ion-item>\n      <ion-item routerLink=\"/login\">\n        <ion-icon class=\"iconStyling\" name=\"log-out\"  slot=\"start\"></ion-icon>\n        <ion-label class=\"menuTxt\" >{{'routelist-logout.title' | translate}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n\n\n<div class=\"ion-page\" id=\"main-content\">\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons class=\"nav-buttons\"slot=\"end\">\n         <ion-button style=\"width: 50px;/*!\" (click)=\"navigateToNotificationsPage()\" ><ion-icon class=\"notBtn\" name=\"notifications\" ></ion-icon></ion-button>\n        <ion-menu-button class=\"menuBtn\"></ion-menu-button>\n      </ion-buttons>\n      <ion-buttons (click)=\"navigateToProfilePage()\" id=\"profile\"></ion-buttons>\n      <ion-title>{{'passengers-list.title' | translate}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n\n  <ion-content style=\"--offset-top:0px; --offset-bottom:0px;position: relative;top: 21px;\">\n\n    <ion-list (click)=\"saveTodos(m)\" *ngFor=\"let papapa of thepassengerListJSON.PASSENGERS; let m = index\">\n      <ion-item class=\"ion-text-center itemStyle\" lines=\"none\">\n        {{m}} <ion-label> &nbsp; &nbsp; {{papapa.FIRST_NAME}}  &nbsp; &nbsp;   {{papapa.LAST_NAME}}</ion-label>\n      </ion-item>\n    </ion-list>\n\n    <!-- <ion-item  *ngFor=\"let item of pases; let m=index\" class=\"ion-text-center itemStyle\" lines=\"none\" >\n      <ion-fab vertical=\"bottom\" horizontal=\"left\" translucent=\"true\">\n        <ion-fab-button class=\"numberIconStyle\" (click)=\"navigateToRouteListPage()\">\n          {{item.number}}\n        </ion-fab-button>\n      </ion-fab>\n      <button  class=\"nameBtn\"></button>\n    </ion-item> -->\n  </ion-content>\n\n  <div class=\"myiontab\" *ngIf=\"hideTabs\">\n\n    <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n      <ion-buttons class=\"addBtn\" (click)=\"navigateToStartRoutePage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n        <button (click)=\"navigateToStartRoutePage()\" class=\"adBtn2\">\n          <ion-icon name=\"arrow-back\"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-tab-button tab=\"routelist\" (click)=\"navigateToRouteListPage()\">\n        <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"wallet\" class=\"comments\" (click)=\"navigateToWalletPage()\">\n        <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n      </ion-tab-button>\n  \n      <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n  \n      <ion-tab-button tab=\"route-history\" class=\"notifs\" (click)=\"navigateToRouteHistoryPage()\">\n        <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n      </ion-tab-button>\n  \n      <ion-tab-button tab=\"settings\" (click)=\"navigateToSettingsPage()\">\n        <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n      </ion-tab-button>\n    </ion-tab-bar>\n  \n  </div>\n  <!-- <div class=\"bottomBar\" *ngIf=\"hideTabs\">\n    \n    <ion-buttons class=\"addBtn\" (click)=\"navigateToStartRoutePage()\" vertical=\"bottom\" horizontal=\"center\" translucent=\"true\">\n      <button (click)=\"navigateToStartRoutePage()\" class=\"adBtn2\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </button>\n    </ion-buttons>\n      \n      <ion-tab-bar slot=\"bottom\" class=\"ion-no-border\">\n        <ion-tab-button tab=\"feed\" (click)=\"navigateToRoutelist()\">\n          <ion-icon class=\"iconStyling\" name=\"home\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button (click)=\"navigateToWalletPage()\" tab=\"wallet\" class=\"comments\">\n          <ion-icon class=\"iconStyling\" name=\"wallet\"></ion-icon>\n        </ion-tab-button>\n    \n        <svg height=\"50\" viewBox=\"0 0 100 50\" width=\"100\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M100 0v50H0V0c.543 27.153 22.72 49 50 49S99.457 27.153 99.99 0h.01z\" fill=\"red\" fill-rule=\"evenodd\"></path></svg>\n    \n        <ion-tab-button (click)=\"navigateToRouteHistoryPage()\" tab=\"route-history\" class=\"notifs\">\n          <ion-icon class=\"iconStyling\" name=\"file-tray-full\"></ion-icon>\n        </ion-tab-button>\n    \n        <ion-tab-button (click)=\"navigateToSettingsPage()\" tab=\"settings\">\n          <ion-icon class=\"iconStyling\" name=\"settings\"></ion-icon>\n        </ion-tab-button>\n      </ion-tab-bar>\n    </div> -->\n</div>\n\n\n  <div class=\"popUp\" *ngIf=\"showInformation\">\n    <div *ngFor=\"let item of onepass; let e=index \" id=\"hidenItem\">\n      \n      <ion-label class=\"pDetails\">\n        <ion-buttons class=\"closeBtn\" (click)=\"saveTodos()\">\n          <ion-icon side=\"end\" class=\"closeBtnIcon\" name=\"backspace\" ></ion-icon>\n        </ion-buttons>\n        <div class=\"center\">\n          <ion-title class=\"pTitle\">{{'passengers-personal.title' | translate}}</ion-title>\n        </div>\n      </ion-label>\n      <div class=\"pDetails\">\n      <ion-label class=\"ion-text-center\">{{'passengers-name.title' | translate}}: {{item.fname}}</ion-label>\n      <ion-label class=\"ion-text-center\">{{'passengers-lastName.title' | translate}}: {{item.lname}}</ion-label>\n      <ion-label class=\"ion-text-center\">{{'passengers-phone.title' | translate}}: {{item.lname}}:{{item.phone}}</ion-label>\n      <ion-label class=\"ion-text-center\">{{'passengers-email.title' | translate}}: {{item.lname}}:{{item.phone}}: {{item.email}}</ion-label>\n    </div>\n    </div>\n  </div>");

/***/ }),

/***/ "./src/app/routepassengers/routepassengers-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/routepassengers/routepassengers-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: RoutepassengersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutepassengersPageRoutingModule", function() { return RoutepassengersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _routepassengers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routepassengers.page */ "./src/app/routepassengers/routepassengers.page.ts");




const routes = [
    {
        path: '',
        component: _routepassengers_page__WEBPACK_IMPORTED_MODULE_3__["RoutepassengersPage"]
    }
];
let RoutepassengersPageRoutingModule = class RoutepassengersPageRoutingModule {
};
RoutepassengersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RoutepassengersPageRoutingModule);



/***/ }),

/***/ "./src/app/routepassengers/routepassengers.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/routepassengers/routepassengers.module.ts ***!
  \***********************************************************/
/*! exports provided: RoutepassengersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutepassengersPageModule", function() { return RoutepassengersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _routepassengers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routepassengers-routing.module */ "./src/app/routepassengers/routepassengers-routing.module.ts");
/* harmony import */ var _routepassengers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./routepassengers.page */ "./src/app/routepassengers/routepassengers.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");








let RoutepassengersPageModule = class RoutepassengersPageModule {
};
RoutepassengersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _routepassengers_routing_module__WEBPACK_IMPORTED_MODULE_5__["RoutepassengersPageRoutingModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
        ],
        declarations: [_routepassengers_page__WEBPACK_IMPORTED_MODULE_6__["RoutepassengersPage"]]
    })
], RoutepassengersPageModule);



/***/ }),

/***/ "./src/app/routepassengers/routepassengers.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/routepassengers/routepassengers.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  background-color: var(--ion-color-ibus-blue);\n}\n\n.ios ion-toolbar {\n  --background: #4E98FF;\n  height: 127px;\n}\n\n.md ion-toolbar {\n  --background:#4E98FF;\n  height: 87px;\n}\n\n.notBtn {\n  color: white;\n  width: 47px;\n  height: 62px;\n}\n\n.menuBtn {\n  font-size: 100px;\n  width: 70px;\n  color: white;\n}\n\n.menuTxt {\n  font-size: 18px;\n  font-family: monteRegular;\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.iconStyling {\n  color: var(--ion-color-ibus-darkblue);\n}\n\n.menuToolbar {\n  position: relative;\n  margin-top: 80px;\n  font-size: 25px;\n}\n\n.ios #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-bottom: 31px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios ion-title {\n  position: absolute;\n  top: -15px;\n  left: -30px;\n  color: white;\n  font-size: 21px;\n}\n\n.md #profile {\n  width: 60px;\n  height: 60px;\n  background-color: white;\n  border-radius: 50%;\n  position: relative;\n  left: 27px;\n  margin-top: 14px;\n  background-image: url('profile.jpg');\n  background-size: 60px 80px;\n}\n\n.ios .nav-buttons {\n  margin-bottom: 30px;\n}\n\n.md ion-title {\n  position: absolute;\n  top: 30px;\n  left: 80px;\n  color: white;\n  font-size: 21px;\n}\n\n.md .nav-buttons {\n  margin-bottom: -15px;\n}\n\n.myiontab ion-fab {\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-fab ion-fab-button {\n  --box-shadow: none;\n  z-index: 1;\n}\n\n.myiontab ion-tab-bar {\n  --border: 0;\n  --background: transparent;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  height: 70px;\n}\n\n.myiontab ion-tab-bar .addBtn {\n  justify-content: center;\n  display: flex;\n  text-align: center;\n  margin-bottom: env(safe-area-inset-bottom);\n  /* fix notch ios*/\n}\n\n.myiontab ion-tab-bar .addBtn button {\n  --box-shadow: none;\n  background-color: var(--ion-color-ibus-theblue);\n  border-radius: 70px;\n  width: 51px;\n  height: 51px;\n  position: absolute;\n  left: 181px;\n  top: -32px;\n  color: white;\n  font-family: monteSBold;\n  font-size: 11px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-icon {\n  color: white;\n  width: 30px;\n  height: 30px;\n  stroke: 5px;\n}\n\n.myiontab ion-tab-bar .addBtn ion-badge {\n  --background: transparent;\n}\n\n.myiontab ion-tab-bar:after {\n  content: \" \";\n  width: 100%;\n  bottom: 0;\n  background: var(--ion-color-light);\n  height: env(safe-area-inset-bottom);\n  position: absolute;\n}\n\n.myiontab ion-tab-bar ion-tab-button {\n  --background: var(--ion-color-light);\n}\n\n.myiontab ion-tab-bar ion-tab-button.comments {\n  margin-right: 0px;\n  border-top-right-radius: 18px;\n}\n\n.myiontab ion-tab-bar ion-tab-button.notifs {\n  margin-left: 0px;\n  border-top-left-radius: 18px;\n}\n\n.myiontab ion-tab-bar svg {\n  width: 72px;\n  margin-top: 19px;\n}\n\n.myiontab ion-tab-bar svg path {\n  fill: var(--ion-color-light);\n}\n\n.nameBtn {\n  width: 70%;\n  height: 57px;\n  border-radius: 27px;\n  background-color: white;\n  border: 2px solid var(--ion-color-ibus-darkblue);\n  font-size: 15px;\n  font-family: monteBold;\n  color: var(--ion-color-ibus-darkblue);\n  margin-left: 26%;\n}\n\n.itemStyle {\n  height: 80px;\n  margin-right: 2%;\n  margin-bottom: -4%;\n}\n\n.numberIconStyle {\n  top: 9px;\n  left: 29%;\n  position: relative;\n}\n\nion-fab-button {\n  --box-shadow: none !important;\n}\n\n.popUp {\n  background-color: var(--ion-color-ibus-theblue);\n  height: 100vh;\n  width: 100vw;\n  position: relative;\n  color: white;\n  font-size: 25px;\n}\n\n.popUp ion-icon {\n  display: inline-block;\n  font-size: 35px;\n  vertical-align: middle;\n}\n\n.ion-text-center {\n  display: flex;\n  justify-content: center;\n  width: 100vw;\n  font-size: 22px;\n  font-family: monteRegular;\n}\n\n.pDetails {\n  margin-top: 200px;\n}\n\n.closeBtn {\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n  width: 100%;\n  color: yellow;\n  top: 70px;\n  position: relative;\n}\n\n.md .pTitle {\n  position: relative;\n  top: 120px;\n  left: 0px;\n  color: white;\n  font-size: 18px;\n  font-family: monteSBold;\n}\n\n.ios .pTitle {\n  position: relative;\n  top: 120px;\n  left: 0px;\n  color: white;\n  font-size: 18px;\n  font-family: monteSBold;\n}\n\n@media only screen and (max-width: 414px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 127px;\n  }\n}\n\n@media only screen and (max-width: 320px) {\n  ion-toolbar {\n    --background: #4E98FF;\n    height: 70px;\n  }\n}\n\n@media only screen and (max-width: 375px) {\n  .ios ion-toolbar {\n    --background: #4E98FF;\n    height: 95px;\n  }\n\n  .ios ion-title {\n    position: absolute;\n    top: -15px;\n    left: -15px;\n    color: white;\n    font-size: 21px;\n  }\n\n  .ios .closeBtnIcon {\n    width: 40px;\n    height: 40px;\n    color: yellow;\n    position: relative;\n    top: 60px;\n    left: -180px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm91dGVwYXNzZW5nZXJzL3JvdXRlcGFzc2VuZ2Vycy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw0Q0FBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0FBRUo7O0FBQUE7RUFDSSxvQkFBQTtFQUNBLFlBQUE7QUFHSjs7QUFEQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUlKOztBQUZBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUtKOztBQUhBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0VBQ0EscUNBQUE7QUFNSjs7QUFKQTtFQUNJLHFDQUFBO0FBT0o7O0FBTEE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQVFKOztBQU5BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtFQUNBLDBCQUFBO0FBU0o7O0FBUEE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFVSjs7QUFSQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtBQVdKOztBQVRBO0VBQ0ksbUJBQUE7QUFZSjs7QUFWQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQWFKOztBQVhBO0VBQ0ksb0JBQUE7QUFjSjs7QUFWQztFQUNDLDBDQUFBO0VBQTRDLGlCQUFBO0FBYzlDOztBQWJFO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0FBZUg7O0FBWkM7RUE2QkMsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUFRLFFBQUE7RUFDRixXQUFBO0VBQ0EsWUFBQTtBQWJSOztBQXJCUTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsMENBQUE7RUFBNEMsaUJBQUE7QUF3QnhEOztBQXZCWTtFQUNJLGtCQUFBO0VBQ0EsK0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7QUF5QmhCOztBQXZCWTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUF5QmhCOztBQXZCWTtFQUNBLHlCQUFBO0FBeUJaOztBQWZFO0VBQ0MsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGtCQUFBO0FBaUJIOztBQWZFO0VBQ0Msb0NBQUE7QUFpQkg7O0FBZkU7RUFDQyxpQkFBQTtFQUNBLDZCQUFBO0FBaUJIOztBQWZFO0VBQ0MsZ0JBQUE7RUFDQSw0QkFBQTtBQWlCSDs7QUFmRTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtBQWlCSDs7QUFoQkc7RUFDQyw0QkFBQTtBQWtCSjs7QUFaQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdEQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EscUNBQUE7RUFDQSxnQkFBQTtBQWVKOztBQWJBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFnQko7O0FBZEE7RUFDSSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FBaUJKOztBQWZBO0VBQ0ksNkJBQUE7QUFrQko7O0FBZkE7RUFDSSwrQ0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQWtCSjs7QUFqQkk7RUFDSSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtBQW1CUjs7QUFoQkE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQyxZQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FBbUJMOztBQWpCQTtFQUNJLGlCQUFBO0FBb0JKOztBQWxCQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtBQXFCSjs7QUFqQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQW9CSjs7QUFsQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQXFCSjs7QUFsQkE7RUFDSTtJQUNJLHFCQUFBO0lBQ0EsYUFBQTtFQXFCTjtBQUNGOztBQW5CRTtFQUNFO0lBQ0kscUJBQUE7SUFDQSxZQUFBO0VBcUJOO0FBQ0Y7O0FBbEJFO0VBQ0U7SUFDSSxxQkFBQTtJQUNBLFlBQUE7RUFvQk47O0VBbEJFO0lBQ0ksa0JBQUE7SUFDQSxVQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VBcUJOOztFQW5CRTtJQUNJLFdBQUE7SUFDQSxZQUFBO0lBQ0EsYUFBQTtJQUNBLGtCQUFBO0lBQ0EsU0FBQTtJQUNBLFlBQUE7RUFzQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL3JvdXRlcGFzc2VuZ2Vycy9yb3V0ZXBhc3NlbmdlcnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XHJcbn1cclxuLmlvcyBpb24tdG9vbGJhcntcclxuICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgIGhlaWdodDogMTI3cHg7XHJcbn1cclxuLm1kIGlvbi10b29sYmFye1xyXG4gICAgLS1iYWNrZ3JvdW5kOiM0RTk4RkY7XHJcbiAgICBoZWlnaHQ6ODdweDtcclxufVxyXG4ubm90QnRue1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgd2lkdGg6IDQ3cHg7XHJcbiAgICBoZWlnaHQ6IDYycHg7XHJcbn1cclxuLm1lbnVCdG57XHJcbiAgICBmb250LXNpemU6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDcwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLm1lbnVUeHR7XHJcbiAgICBmb250LXNpemU6MThweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlUmVndWxhcjtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxufVxyXG4uaWNvblN0eWxpbmd7XHJcbiAgICBjb2xvcjp2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbn1cclxuLm1lbnVUb29sYmFye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLXRvcDogODBweDtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxufVxyXG4uaW9zICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMXB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgbGVmdDogLTMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDIxcHg7XHJcbn1cclxuLm1kICNwcm9maWxle1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6IDI3cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZS5qcGcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiA2MHB4IDgwcHg7XHJcbn1cclxuLmlvcyAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcbi5tZCBpb24tdGl0bGV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDMwcHg7XHJcbiAgICBsZWZ0OiA4MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5tZCAubmF2LWJ1dHRvbnN7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxufVxyXG5cclxuLm15aW9udGFie1xyXG5cdGlvbi1mYWIge1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pOyAvKiBmaXggbm90Y2ggaW9zKi9cclxuXHRcdGlvbi1mYWItYnV0dG9uIHtcclxuXHRcdFx0LS1ib3gtc2hhZG93OiBub25lO1xyXG5cdFx0XHR6LWluZGV4OiAxO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRpb24tdGFiLWJhciB7XHJcbiAgICAgICAgLmFkZEJ0biB7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTsgLyogZml4IG5vdGNoIGlvcyovXHJcbiAgICAgICAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItaWJ1cy10aGVibHVlKTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNTFweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNTFweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IDE4MXB4O1xyXG4gICAgICAgICAgICAgICAgdG9wOiAtMzJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOndoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6bW9udGVTQm9sZDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZToxMXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgICAgICAgICAgY29sb3I6d2hpdGU7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDozMHB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OjMwcHg7XHJcbiAgICAgICAgICAgICAgICBzdHJva2U6IDVweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpb24tYmFkZ2V7XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cdFx0LS1ib3JkZXI6IDA7XHJcblx0XHQtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Ym90dG9tOiAwO1xyXG5cdFx0bGVmdDowOyByaWdodDogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6NzBweDtcclxuXHRcdCY6YWZ0ZXJ7XHJcblx0XHRcdGNvbnRlbnQ6IFwiIFwiO1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG5cdFx0XHRoZWlnaHQ6IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0fVxyXG5cdFx0aW9uLXRhYi1idXR0b24ge1xyXG5cdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHR9XHJcblx0XHRpb24tdGFiLWJ1dHRvbi5jb21tZW50cyB7XHJcblx0XHRcdG1hcmdpbi1yaWdodDogMHB4O1xyXG5cdFx0XHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMThweDtcclxuXHRcdH1cclxuXHRcdGlvbi10YWItYnV0dG9uLm5vdGlmcyB7XHJcblx0XHRcdG1hcmdpbi1sZWZ0OiAwcHg7XHJcblx0XHRcdGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDE4cHg7XHJcblx0XHR9XHJcblx0XHRzdmcgeyAgICBcclxuXHRcdFx0d2lkdGg6IDcycHg7XHJcblx0XHRcdG1hcmdpbi10b3A6IDE5cHg7XHJcblx0XHRcdHBhdGh7XHJcblx0XHRcdFx0ZmlsbDogIHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblx0XHRcdH1cdFx0XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG4ubmFtZUJ0bntcclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICBoZWlnaHQ6IDU3cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyN3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSk7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlQm9sZDtcclxuICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcclxuICAgIG1hcmdpbi1sZWZ0OjI2JTtcclxufVxyXG4uaXRlbVN0eWxle1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyJTtcclxuICAgIG1hcmdpbi1ib3R0b206IC00JTtcclxufVxyXG4ubnVtYmVySWNvblN0eWxle1xyXG4gICAgdG9wOiA5cHg7XHJcbiAgICBsZWZ0OiAyOSU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuaW9uLWZhYi1idXR0b257XHJcbiAgICAtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnBvcFVwe1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWlidXMtdGhlYmx1ZSk7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgaW9uLWljb257XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzVweDtcclxuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyAgICAgIFxyXG4gICAgfVxyXG59XHJcbi5pb24tdGV4dC1jZW50ZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgIGZvbnQtc2l6ZToyMnB4O1xyXG4gICAgIGZvbnQtZmFtaWx5Om1vbnRlUmVndWxhcjtcclxufVxyXG4ucERldGFpbHN7XHJcbiAgICBtYXJnaW4tdG9wOiAyMDBweDtcclxufVxyXG4uY2xvc2VCdG57XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBjb2xvcjp5ZWxsb3c7XHJcbiAgICB0b3A6IDcwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuICBcclxuXHJcbi5tZCAucFRpdGxle1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAxMjBweDtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGZvbnQtZmFtaWx5OiBtb250ZVNCb2xkO1xyXG59XHJcbi5pb3MgLnBUaXRsZXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMTIwcHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6MThweDtcclxuICAgIGZvbnQtZmFtaWx5Om1vbnRlU0JvbGQ7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDE0cHgpIHtcclxuICAgIGlvbi10b29sYmFye1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzRFOThGRjtcclxuICAgICAgICBoZWlnaHQ6IDEyN3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XHJcbiAgICBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzNzVweCkge1xyXG4gICAgLmlvcyBpb24tdG9vbGJhcntcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICM0RTk4RkY7XHJcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgfVxyXG4gICAgLmlvcyBpb24tdGl0bGV7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogLTE1cHg7XHJcbiAgICAgICAgbGVmdDogLTE1cHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIH1cclxuICAgIC5pb3MgLmNsb3NlQnRuSWNvbntcclxuICAgICAgICB3aWR0aDogNDBweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgY29sb3I6IHllbGxvdztcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiA2MHB4O1xyXG4gICAgICAgIGxlZnQ6IC0xODBweDtcclxuICAgICAgICBcclxuICAgIH1cclxuICB9XHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gICBcclxuXHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/routepassengers/routepassengers.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/routepassengers/routepassengers.page.ts ***!
  \*********************************************************/
/*! exports provided: RoutepassengersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutepassengersPage", function() { return RoutepassengersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let RoutepassengersPage = class RoutepassengersPage {
    constructor(http, activatedRoute, router, nativeHttp) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.nativeHttp = nativeHttp;
        this.vehmapid = localStorage.getItem('vehmapid');
        this.chrbus_passengers_json = [{ "vehicle_map_id": "2245", "pass_id": "1", "first_name": "Christos", "last_name": "Sotidis", "mobile": "123456", "email": "christos@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2245", "pass_id": "2", "first_name": "Christos2", "last_name": "Sotidis2", "mobile": "123456", "email": "christos2@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2246", "pass_id": "3", "first_name": "Christos3", "last_name": "Sotidis3", "mobile": "123456", "email": "christos3@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2246", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2250", "pass_id": "1", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2250", "pass_id": "2", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2251", "pass_id": "1", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2251", "pass_id": "2", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2247", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2248", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" },
            { "vehicle_map_id": "2249", "pass_id": "4", "first_name": "Christos4", "last_name": "Sotidis4", "mobile": "123456", "email": "christos4@test", "vhc_plates": "OPE6009", "service": "CHT", "ServiceCode": "panos", "assignment_from_date": "2020-10-27 00:00:000:000", "assignment_to_date": "2020-10-27 00:00:000:000" }];
        this.showInformation = false;
        this.hideTabs = true;
        this.thepassengerListJSON = [];
        //greg
        this.dataFromRouteStartedDromologio = this.activatedRoute.snapshot.paramMap.get('dromologio');
        this.dataFromRouteStartedJSONDromologio = JSON.parse(this.dataFromRouteStartedDromologio);
        this.dataFromRouteStartedDriverId = this.activatedRoute.snapshot.paramMap.get('driverid');
        this.dataFromRouteStartedJSONDriverId = JSON.parse(this.dataFromRouteStartedDriverId);
        this.dataFromRouteStarted = this.activatedRoute.snapshot.paramMap.get('passengers');
        console.log('%c JSON', 'color:orange;');
        console.log(this.dataFromRouteStarted);
        console.log('%c JSON', 'color:yellow;');
        this.dataFromRouteStartedJSON = JSON.parse(this.dataFromRouteStarted);
        console.log("HERE", this.dataFromRouteStartedJSON.VEHICLE_MAP_ID);
        this.VEHICLE_MAP_ID = this.dataFromRouteStartedJSON.VEHICLE_MAP_ID;
        console.log(this.VEHICLE_MAP_ID);
        this.fnames = [];
        this.lnames = [];
        this.pases = [];
        this.phones = [];
        this.emails = [];
        let nativeCall = this.nativeHttp.get('http://cf11.travelsoft.gr/itourapi/chrbus_passengers_list.cfm?vehicle_map_id=' + 2408 + '&userid=dmta', {}, {
            'Content-Type': 'application/json'
        });
        Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["from"])(nativeCall).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(() => console.log('')))
            .subscribe((data) => {
            let parsed = JSON.parse(data.data);
            console.log(data);
            this.thePassenger = parsed;
            this.thepassengerListJSON = this.thePassenger;
            this.thepassengerListJSONtoArray = this.thepassengerListJSON.PASSENGERS;
            console.log('%c Passengers', 'color:yellow;');
            console.log(this.thepassengerListJSONtoArray);
            var i = 0;
            var j = 0;
            for (i = 0; i < this.thepassengerListJSONtoArray.length; i++) {
                if (this.VEHICLE_MAP_ID == this.thepassengerListJSONtoArray[i].VEHICLE_MAP_ID) {
                    this.fnames[j] = this.thepassengerListJSONtoArray[i].FIRST_NAME;
                    this.lnames[j] = this.thepassengerListJSONtoArray[i].LAST_NAME;
                    this.phones[j] = this.thepassengerListJSONtoArray[i].MOBILE;
                    this.emails[j] = this.thepassengerListJSONtoArray[i].EMAIL;
                    this.pases.push({
                        fname: this.fnames[i],
                        lname: this.lnames[i],
                        fullname: this.lnames[i] + " " + this.fnames[i],
                        phone: this.phones[i],
                        email: this.emails[i],
                        number: i + 1,
                    });
                    j++;
                }
            }
        });
        console.log(this.fnames, this.lnames);
    }
    saveTodos(m) {
        this.onepass = [];
        console.log("here");
        console.log(m);
        if (this.showInformation == false) {
            this.showInformation = true;
            this.hideTabs = false;
            this.onepass.push({
                fname: this.fnames[m],
                lname: this.lnames[m],
                fullname: this.lnames[m] + " " + this.fnames[m],
                phone: this.phones[m],
                email: this.emails[m],
                number: m + 1,
            });
        }
        else {
            this.showInformation = false;
            this.hideTabs = true;
        }
        console.log('wtf');
    }
    saveTodos2() {
        console.log("here2");
        if (this.showInformation == false) {
            this.showInformation = true;
        }
        else {
            this.showInformation = false;
        }
        console.log('wtf');
    }
    navigateToStartRoutePage() {
        this.router.navigate(['routestarted/' + this.dataFromRouteStartedDromologio + '/' + this.dataFromRouteStartedDriverId]);
    }
    ngOnInit() {
    }
    navigateToWalletPage() {
        this.router.navigate(['wallet']);
    }
    navigateToSettingsPage() {
        this.router.navigate(['settings']);
    }
    navigateToRoutelist() {
        this.router.navigate(['routelist']);
    }
    navigateToTechHistoryPage() {
        this.router.navigate(['techhistory']);
    }
    navigateToProfilePage() {
        this.router.navigate(['profile']);
    }
    navigateToRouteHistoryPage() {
        this.router.navigate(['routehistory']);
    }
    navigateToRouteListPage() {
        this.router.navigate(['routelist']);
    }
    navigateToNotificationsPage() {
        this.router.navigate(['notifications']);
    }
};
RoutepassengersPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"] }
];
RoutepassengersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-routepassengers',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./routepassengers.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/routepassengers/routepassengers.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./routepassengers.page.scss */ "./src/app/routepassengers/routepassengers.page.scss")).default]
    })
], RoutepassengersPage);



/***/ })

}]);
//# sourceMappingURL=routepassengers-routepassengers-module-es2015.js.map