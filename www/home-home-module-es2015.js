(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title>\n      Blank\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n <div #map id=\"map\"></div>\n  <ion-button (click)=\"btnclicked()\" >Test</ion-button>\n</ion-content> -->\n\n<ion-content>\n<app-slides>\n  <ion-slides pager=\"true\" style=\"\" [options]=\"slideOpts\">\n    <ion-slide>\n      <app-logo></app-logo>\n      <h1>Work for us</h1>\n      <h2>LEARN NOW</h2>\n      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\n    </ion-slide>\n    <ion-slide>\n      <app-logo></app-logo>\n      <h1>Register as a Driver</h1>\n      <h2>EASY 4-STEPS</h2>\n      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\n    </ion-slide>\n    <ion-slide>\n      <app-logo></app-logo>\n      <h1>COVID 19 NEWS</h1>\n      <h2>Learn about covid19</h2>\n      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>\n    </ion-slide>\n  </ion-slides>\n  </app-slides>\n  <form id=\"form\">\n\n<ion-button class=\"btn-login\" expand=\"block\" shape=\"round\" (click)=\"navigateToLoginPage()\">CONTINUE</ion-button>\n\n</form>\n<!-- <app-start></app-start> -->\n</ion-content>");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\n#map {\n  width: 100%;\n  height: 300px;\n}\n\n.login-btn {\n  height: 60px;\n  height: 18;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  position: relative;\n  bottom: 50px;\n}\n\nion-button {\n  height: 60px;\n  height: 18;\n  color: white;\n  font-family: monteBold;\n  font-size: 13px;\n  border-radius: 21px;\n  background-image: linear-gradient(180deg, #4E98FF 90%, #4E98FF 10%);\n  position: relative;\n  bottom: -80px;\n}\n\n#form {\n  margin-top: 5px;\n  padding-left: 30px;\n  padding-right: 30px;\n}\n\n/* commit */\n\nion-slide {\n  padding: 20px 20px;\n  display: block;\n}\n\nion-slide h1 {\n  font-size: 25px;\n  color: var(--ion-color-ibus-blue);\n  margin-top: 30px;\n  font-family: monteBold;\n}\n\nion-slide h2 {\n  font-size: 19px;\n  color: var(--ion-color-ibus-darkblue);\n  margin-top: 30px;\n  font-family: monteBold;\n}\n\nion-slide p {\n  font-size: 21px;\n  font-family: monteRegular;\n}\n\nion-slides {\n  position: relative;\n  top: 60px;\n  --bullet-background-active: var(--ion-color-ibus-darkblue) !important;\n}\n\n:host ::ng-deep .swiper-pagination {\n  position: relative;\n}\n\n:host ::ng-deep .swiper-pagination .swiper-pagination-bullet {\n  --bullet-background: #000000;\n  width: 20px;\n  height: 20px;\n}\n\n:host ::ng-deep .swiper-pagination .swiper-pagination-bullet-active {\n  width: 20px;\n  height: 20px;\n}\n\n.img {\n  width: 263px;\n  height: 112px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxjQUFBO0VBRUEsU0FBQTtBQUZGOztBQUtBO0VBQ0UscUJBQUE7QUFGRjs7QUFLQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0FBRkY7O0FBSUE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG1FQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBREo7O0FBR0E7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG1FQUFBO0VBQ0Esa0JBQUE7RUFDSixhQUFBO0FBQUE7O0FBRUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBLFdBQUE7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFBSTtFQUNJLGVBQUE7RUFDQSxpQ0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFFUjs7QUFBSTtFQUNJLGVBQUE7RUFDQSxxQ0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFFUjs7QUFBSTtFQUNJLGVBQUE7RUFDQSx5QkFBQTtBQUVSOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EscUVBQUE7QUFFSjs7QUFBQTtFQUVJLGtCQUFBO0FBRUo7O0FBREk7RUFDSSw0QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBR1I7O0FBQUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUVSOztBQUNBO0VBQ1EsWUFBQTtFQUNBLGFBQUE7QUFFUiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcblxuICBjb2xvcjogIzhjOGM4YztcblxuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuI21hcHtcbiAgd2lkdGg6MTAwJTtcbiAgaGVpZ2h0OiAzMDBweDtcbn1cbi5sb2dpbi1idG57XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGhlaWdodDoxODtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LWZhbWlseTogbW9udGVCb2xkO1xuICAgIGZvbnQtc2l6ZToxM3B4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIxcHg7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgIzRFOThGRiA5MCUsIzRFOThGRiAxMCUpO1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIGJvdHRvbTo1MHB4O1xufVxuaW9uLWJ1dHRvbntcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgaGVpZ2h0OjE4O1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiBtb250ZUJvbGQ7XG4gICAgZm9udC1zaXplOjEzcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMjFweDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjNEU5OEZGIDkwJSwjNEU5OEZGIDEwJSk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuYm90dG9tOiAtODBweDtcbn1cbiNmb3JtIHtcbiAgICBtYXJnaW4tdG9wOjVweDtcbiAgICBwYWRkaW5nLWxlZnQ6MzBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OjMwcHg7XG59XG4vKiBjb21taXQgKi9cblxuaW9uLXNsaWRle1xuICAgIHBhZGRpbmc6IDIwcHggMjBweDtcbiAgICBkaXNwbGF5OmJsb2NrO1xuICAgIGgxe1xuICAgICAgICBmb250LXNpemU6MjVweDtcbiAgICAgICAgY29sb3I6dmFyKC0taW9uLWNvbG9yLWlidXMtYmx1ZSk7XG4gICAgICAgIG1hcmdpbi10b3A6MzBweDtcbiAgICAgICAgZm9udC1mYW1pbHk6bW9udGVCb2xkO1xuICAgIH1cbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOjE5cHg7XG4gICAgICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1pYnVzLWRhcmtibHVlKTtcbiAgICAgICAgbWFyZ2luLXRvcDozMHB4O1xuICAgICAgICBmb250LWZhbWlseTptb250ZUJvbGQ7XG4gICAgfVxuICAgIHB7XG4gICAgICAgIGZvbnQtc2l6ZToyMXB4O1xuICAgICAgICBmb250LWZhbWlseTptb250ZVJlZ3VsYXI7XG4gICAgfVxufVxuaW9uLXNsaWRlc3tcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiA2MHB4O1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQtYWN0aXZlOiB2YXIoLS1pb24tY29sb3ItaWJ1cy1kYXJrYmx1ZSkgIWltcG9ydGFudDtcbn1cbjpob3N0IDo6bmctZGVlcCAuc3dpcGVyLXBhZ2luYXRpb257XG5cbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gICAgICAgIC0tYnVsbGV0LWJhY2tncm91bmQ6ICMwMDAwMDA7XG4gICAgICAgIHdpZHRoOiAyMHB4IDtcbiAgICAgICAgaGVpZ2h0OiAyMHB4IDtcbiAgICB9XG5cbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZSB7XG4gICAgICAgIHdpZHRoOiAyMHB4IDtcbiAgICAgICAgaGVpZ2h0OiAyMHB4IDtcbiAgICB9IFxufVxuLmltZ3tcbiAgICAgICAgd2lkdGg6MjYzcHg7XG4gICAgICAgIGhlaWdodDoxMTJweDtcbiAgICB9XG4iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let HomePage = class HomePage {
    constructor(platform, router) {
        this.platform = platform;
        this.router = router;
        this.markers = [];
    }
    // loadMap(){
    // 	let latLng = new google.maps.LatLng(51.9036442, 7.6673267);
    // 	let mapOptions = {
    // 		center:latLng,
    // 		zoom:5,
    // 		mapTypeId: google.maps.MapTypeId.ROADMAP
    // 	};
    // 	this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // }
    ionViewWillEnter() {
        console.log('here2');
        this.platform.ready().then(() => {
            if (this.platform.is('android')) {
                console.log('android');
            }
            else if (this.platform.is('ios')) {
                console.log('ios');
            }
            else {
                //fallback to browser APIs or
                console.log('The platform is not supported');
            }
        });
        // this.loadMap();
    }
    navigateToLoginPage() {
        this.router.navigate(['language']);
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
HomePage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['map',] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map